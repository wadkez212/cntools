(function( $ ) {
	'use strict';
	console.log('Cntools')
	$(document).on('click', '.js-faq-toggle', function (){
		var $self = $(this);
		$self.toggleClass('faq-show');
	});


})( jQuery );



// SLIDER
$ = jQuery;

$(document).ready(function () {
	recopnceElements();

	// SLider
	if ($('div.cntools-slider').length > 0)
	{
		$('.cntools-slider-item').each(function (ix, val){
			var active = ix == 0 ? 'active' : '';
			var dot = '<span class="dot js-dot ' + active + '" data-item="' + ix + '"></span>';
			$('div.cntools-slider .dot-wrapper').append(dot);
		});
	}


	$(document).on('click touch', 'div.cntools-slider .js-dot', function (){
		var $self = $(this), $data = $self.data();
		$('div.cntools-slider .active').removeClass('active');

		$($('.cntools-slider-item')[$data.item]).addClass('active');
		$self.addClass('active');
	})



	// Box slider
	if ($('div.cntools-box-slider').length > 0)
	{
		var boxNewWidth = $('.cntools-box-slider-item').outerWidth()*4;

		 $boxSLiderWrapperW = boxNewWidth+52;

		 if($boxSLiderWrapperW > $(window).width())
		 {
			 $boxSLiderWrapperW = $(window).width();
		 }

		 // $('div.cntools-box-slider').width($boxSLiderWrapperW);

		 var boxItemsLength = $('.cntools-box-slider-item').length;
		 var Pages = Math.ceil(boxItemsLength/4);
			console.log(Pages);

		for (let i = 0; i < Pages; i++) {
			var active = i == 0 ? 'active' : '';
			var dot = '<span class="dot js-dot ' + active + '" data-item="' + i + '"></span>';
			$('div.cntools-box-slider').parent().find('.dot-wrapper').append(dot);
		}
	}


	$(document).on('click touch', 'div.cntools-box-slider-wrapper .js-dot', function (){
		var $self = $(this), $data = $self.data();
		$('div.cntools-box-slider .active').removeClass('active');
		// $('div.cntools-box-slider').scrollLeft(320);

		$('div.cntools-box-slider').animate({
			scrollLeft: 340
		}, 500);
		$self.addClass('active');
	})


	$(window).resize(function (){
		recopnceElements()
	});



	function recopnceElements()
	{
		// $('div.cntools-box-slider').width($(document).width());
		//

		if($(this).width()<985) {

			$('div.cntools-box-slider').attr('style', ' ');

			if($('.cloned-main-search').length==0)
			{
				var $mainSearch = $('.main-search').clone();
				$mainSearch.addClass('cloned-main-search')
				$('.adaptive > .inner-first').html($mainSearch);
			}


			if($('.clone-compare-and-favorite').length==0)
			{
				var $mainCompFav = $('.compare-and-favorite').clone();
				$mainCompFav.addClass('clone-compare-and-favorite')
				$('.adaptive > .inner-last').html($mainCompFav);
			}

			if($('.clone-cart').length==0)
			{
				var $topCart = $('header nav .cart').clone();
				$topCart.addClass('clone-cart')
				$('.adaptive > .inner-last').prepend($topCart);
			}
		}
		else
		{
			$('.cloned-main-search').remove();
			$('.clone-compare-and-favorite').remove();
			$('.clone-cart').remove();
		}


		// header nav .cart

	}





});

