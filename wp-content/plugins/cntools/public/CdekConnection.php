<?php
namespace Public;


use CdekSDK\Requests;
use CdekSDK\CdekClient;


class CdekConnection
{
     protected  $client;


     function  __construct() {
         $out = get_option('cntools-options-sdek');
         $result = json_decode($out);
         $this->client = new \CdekSDK\CdekClient($result->account, $result->password);
         //$this->client = new CdekClient($result->account, $result->password);

     }

    public function getPvz($city_code = 137) {
        $request = new Requests\PvzListRequest();
        $request->setCityId($city_code);
        $request->setType(Requests\PvzListRequest::TYPE_ALL);
        $request->setCashless(true);
        $request->setCash(true);
        $request->setCodAllowed(true);
        $request->setDressingRoom(true);

        $response = $this->client->sendPvzListRequest($request);

        if ($response->hasErrors()) {
            // обработка ошибок
        }

        return $response->getItems();

        /** @var \CdekSDK\Responses\PvzListResponse $response */
//        foreach ($response as $item) {
//            /** @var \CdekSDK\Common\Pvz $item */
//            // всевозможные параметры соответствуют полям из API СДЭК
//            $item->Code;
//            $item->Name;
//            $item->Address;
//
//
//            dd($item);
//
////            foreach ($item->OfficeImages as $image) {
////                $image->getUrl();
////            }
//
////            foreach ($item->Address as $address) {
////               dd( $address);
////            }
//        }

    }


    public function regions() {
        $request = new Requests\RegionsRequest();
        $request->setPage(0);
        $request->setCountryCode('RU');

        $response = $this->client->sendRegionsRequest($request);

        if ($response->hasErrors()) {
            // обработка ошибок
        }
        $result = [];
        foreach ($response as $region) {
            $result[$region->getName()] = $region->getCode();
        }

        return $result;

//        foreach ($response as $region) {
//            /** @var \CdekSDK\Common\Region $region */
//            $region->getUuid();
//            $region->getName();
//            $region->getPrefix();
//            try {
//                $region->getCode();
//                $region->getCodeExt();
//            } catch (\TypeError $e) {
//                // У региона нет кода
//            }
//            $region->getFiasGuid();
//            $region->getCountryName();
//            $region->getCountryCodeISO();
//            $region->getCountryCodeExt();
//
//
//            echo  $region->getName() . '<br>';
//        }
    }

    /**
     * @param $regionCode
     * @return array
     */
    public function citys($regionCode = 81) {

        $request = new Requests\CitiesRequest();
        $request->setPage(0)->setRegionCode($regionCode);
        $request->setCountryCode('RU');
        $response = $this->client->sendCitiesRequest($request);

        if ($response->hasErrors()) {
            // обработка ошибок
        }

        $result = [];
        foreach ($response as $city) {
            $result[$city->getCityName()] = $city->getCityCode();
        }

        return $result;



//        foreach ($response as $location) {
            /** @var \CdekSDK\Common\Location $location */
            echo $location->getCityCode() . '-' .  $location->getCityName() . ' - ' . $location->getRegion() .  '<br>';
//            echo $location->getCityName() .'<br>';
//            $location->getCityCode();
//            $location->getCityUuid();
//            $location->getCountry();
//            $location->getCountryCodeISO();
//            $location->getRegion();
//            $location->getRegionCode();
//            $location->getRegionCodeExt();
//            $location->getSubRegion();
//            $location->getPaymentLimit();
//            $location->getLatitude();
//            $location->getLongitude();
//            $location->getKladr();
//            $location->getFiasGuid();
//        }
    }

    /**
     * @param $city_name
     * @return false|mixed
     */
    function getRegionCodeByName($city_name = false) {
         if(!$city_name) return false;
         $regions = $this->regions();
         if(!empty($regions[$city_name]))
         {
             return $regions[$city_name];
         }
         else
         {
             return false;
         }
    }

    /**
     * @param $city_name
     * @param $region_code
     * @return false|mixed
     */
    function getCityCode($city_name = false, $region_code = false) {
        if(!$city_name) return false;
        $regions = $this->citys($region_code);
        if(!empty($regions[$city_name]))
        {
            return $regions[$city_name];
        }
        else
        {
            return false;
        }
    }


    public function getTarif($senderPcode = false, $receiverPcode = false, $package = []) {
        $request = new Requests\CalculationWithTariffListAuthorizedRequest();
        $request->setSenderCityPostCode($senderPcode)
            ->setReceiverCityPostCode($receiverPcode)
            ->addTariffToList(136)
            ->addPackage($package);

        $response = $this->client->sendCalculationWithTariffListRequest($request);

        $out = [];

        /** @var \CdekSDK\Responses\CalculationWithTariffListResponse $response */
        if ($response->hasErrors()) {
            $out['errors'][$package['supplier_id']] = $response->getErrors();
        }

        foreach ($response->getResults() as $result) {
            $out['product_id'] = $package['product_id'];
            $out['supplier_id'] = $package['supplier_id'];
            if ($result->hasErrors()) {
//                $out[] = $result->hasErrors();
                continue;
            }
            if (!$result->getStatus()) {
//                $out[] = $result->getStatus();
                continue;
            }
            $out['id'] = $result->getTariffId();
            $out['price'] = $result->getPrice();
            $out['min'] = $result->getDeliveryPeriodMin();
            $out['max'] = $result->getDeliveryPeriodMax();
        }

        return $out;
    }

}