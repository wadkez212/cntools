<?php
$results = $wpdb->get_results( "SELECT ID, post_title, post_content FROM $wpdb->posts  WHERE post_type='faq' AND post_status='publish'");
?>
<!--  popular brands -->
<section class="section useful">
    <h2 class="front-section-title">Это будет полезно</h2>
    <div class="container">
        <div class="flex">
            <div class="flex-inner left-block">
                <h2>Часто задаваемые вопросы</h2>
                <ul class="faq-list">
                    <?php foreach ($results as $item): ?>
                    <li class="js-faq-toggle">
                        <div class="faq-question"><?= $item->post_title?></div>
                        <div class="faq-answer">
                            <?= $item->post_content?>
                        </div>
                    </li>
                    <?php endforeach;?>
                </ul>
            </div>
            <div class="flex-inner right-block">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/man.png">
            </div>
        </div>
    </div>
</section>