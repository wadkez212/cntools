<?php
$sliders = get_posts( array(
    'numberposts' => 5,
//    'category'    => 45,
    'orderby'     => 'date',
    'order'       => 'DESC',
    'include'     => array(),
    'exclude'     => array(),
    'meta_key'    => '',
    'meta_value'  =>'',
    'post_type'   => 'slider',
    'suppress_filters' => true,
) );

?>


<div class="slider-wrapper">
    <div class="cntools-slider">
        <?php $i = 0; ?>
        <?php foreach ($sliders as $item): ?>
        <div class="cntools-slider-item <?= $i == 0 ? 'active' : ''?>">
            <img src="<?= get_the_post_thumbnail_url( $item->ID )?>" alt="<?= $item->post_title?>">
        </div>
        <?php $i++ ?>
        <?php endforeach; ?>
        <div class="dot-wrapper">
<!--            <span class="dot js-dot"></span>-->
<!--            <span class="dot js-dot"></span>-->
        </div>
    </div>
</div>