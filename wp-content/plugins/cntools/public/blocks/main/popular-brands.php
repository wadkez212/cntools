<?php
$categories = get_categories( [
    'taxonomy'     => 'product_cat',
//    'child_of'     => 0,
    'parent'       => '23',
    'orderby'      => 'term_ID',
    'order'        => 'ASC',
    'hide_empty'   => 0,
//    'hierarchical' => 1,
//    'exclude'      => '',
//    'include'      => '',
//    'number'       => 0,
//    'pad_counts'   => false,
] );

?>
<!--  popular brands -->
<section class="section popular-products">
    <h2 class="front-section-title">Популярные Бренды</h2>
    <br>
    <a class="all-popular" href="#">Смотреть все</a>
    <div class="container">
        <div class=cntools-brand-slider>
        <ul class="brands-list-front">
            <?php foreach ($categories as $item): ?>
                <?php $thumbnail_id = get_term_meta( $item->term_id, 'thumbnail_id', true );?>
                <li class="cntools-brand-slider-item">
                    <a href="<?= get_category_link( $item->term_id )?>">
                        <div class="img">
                            <img src="<?= wp_get_attachment_url( $thumbnail_id )?>">
                        </div>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
        </div>
    </div>

    <div class="container">
        <?php
        $results = $wpdb->get_results( "
            SELECT ID, post_parent, post_title FROM $wpdb->posts 
            WHERE (post_type='page' AND ID='52' AND post_status='publish') OR 
                  (post_type='page' AND post_parent='52' AND post_status='publish')
        ");

        $title_key = array_flip(array_column($results, 'post_parent'))[0];

        ?>
            <div class="own-brands">
                <h2><?= $results[$title_key]->post_title?></h2>
                <ul class="own-brands-list">
                    <?php foreach ($results as $item): ?>
                    <?php if($item->post_parent > 0): ?>
                        <li class="list">
                            <a href="<?= get_permalink( $item->ID )?>">
                                <div class="img">
                                    <img src="<?= get_the_post_thumbnail_url( $item->ID )?>">
                                </div>
                            </a>
                        </li>
                    <?php endif;?>
                    <?php endforeach;?>
                </ul>
            </div>


    </div>
</section>