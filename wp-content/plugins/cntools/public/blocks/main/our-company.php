<section class="section our-company">
    <span class="overlay-text">Наша компания</span>
    <div class="container">
        <div class="oc-wrapper">
            <div class="logo-row">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-2.png">
            </div>
            <div class="our-company-row">
                <div class="row-oc">
                    <div>ТОКАРНО-ФРЕЙЗЕРНЫЙ ИНСТРУМЕНТ. ЧПУ СТАНКИ. МАШИНОСТРОЕНИЕ.</div>
                    <div class="style-15">15 Лет вместе</div>
                </div>
                <div class="row-oc">
                    Текст-заполнитель — это текст, который имеет некоторые характеристики реального письменного текста, но является случайным набором слов или сгенерирован иным образом. Его можно использовать для отображения образца шрифтов, создания текста для тестирования или обхода спам-фильтра.
                </div>
            </div>
            <?php
            $out = get_option('cntools-options-out-company');
            $result = !empty($out) ? json_decode($out, true) : false;
            ?>
            <?php if($result): ?>
                <div class="flex our-company-info-blocks">
                    <?php foreach($result as $item):?>
                    <div class="flex-inner">
                        <div class="img-wrapper">
                            <img src="<?=$item['icon']?>" alt="<?=$item['name']?>">
                        </div>
                        <?php if (is_numeric($item['line'])): ?>
                        <h3><?=number_format($item['line'], 0, ' ', ' ')?></h3>
                        <?php else: ?>
                            <h3><?= $item['line']?></h3>
                        <?php endif; ?>
                        <p><?=$item['line2']?></p>
                    </div>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>

<!--        <ul class="oc-info-desc">-->
<!--            <li>-->
<!--                <div></div>-->
<!--                <div></div>-->
<!--            </li>-->
<!--        </ul>-->

    </div>
</section>
