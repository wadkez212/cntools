<?php
namespace Kinaloko;

class Form
{

    public $result = null;
    public $note = null;


    /**
     * Text
     * @param $args
     * @param $value
     * @return false|void
     */
    public function Text($args = [], $value = false, $type = 'static', $item_key = false)
    {
        if (empty($args)) return false;
        ?>
        <?php if($type == 'static'): ?>
        <div class="form-control">
            <label><?=$args['placeholder']?>:</label>
            <input name="form[<?=$args['name']?>]"
                   type="text"
                   id="<?=$args['name']?>"
                   value="<?= !empty($value[$args['name']]) ? $value[$args['name']] : '' ?>"
                   placeholder="<?=$args['placeholder']?>">
            <span class="note">Подсказка: <?= !empty($item['note']) ? $args['note'] : ''?></span>
        </div>
    <?php else: ?>
         <?php $key = !empty($item_key) ? $item_key : '0'?>
        <input name="form[<?= $key?>][<?= $args['name']?>]" type="text" id="<?= $args['name']?>" value="<?= !empty($value[$args['name']]) ? $value[$args['name']] : '' ?>" placeholder="<?= $args['placeholder']?>">
    <?php endif;?>
<?php
    }


    /**
     * Select
     * @param $args
     * @param $value
     * @param $type
     * @param $item_key
     * @return false|void
     */
    public function Select($args = [], $value = false, $type = 'static', $item_key = false)
    {
        if (empty($args)) return false;
        ?>
        <?php if($type == 'static'): ?>
        <div class="form-control">
            <label><?=$args['placeholder']?>:</label>
            <?php $value = !empty($value[$args['name']]) ? $value[$args['name']] : '';?>
            <select name="form[<?= $args['name']?>]">
                <option value="">&mdash; <?= $args['placeholder']?>  &mdash;</option>
                <?php echo $args['func']($value)?>
            </select>
            <span class="note">Подсказка: <?= !empty($item['note']) ? $args['note'] : ''?></span>
        </div>
    <?php else: ?>
         <?php $key = !empty($item_key) ? $item_key : '0'?>
        <select name="form[<?= $key?>][<?= $args['name']?>]">
            <option value="">&mdash; <?= $args['placeholder']?>  &mdash;</option>
            <?php echo $args['func']($value[$args['name']])?>
        </select>
    <?php endif;?>
<?php
    }



    /**
     * Select
     * @param $args
     * @param $value
     * @param $type
     * @param $item_key
     * @return false|void
     */
    public function Checkboxlist($args = [], $value = false, $type = 'static', $item_key = false)
    {
        if (empty($args)) return false;
        ?>
        <?php if($type == 'static'): ?>
        <div class="form-control">
            <label><?=$args['placeholder']?>:</label>
            <?php $value = !empty($value[$args['name']]) ? $value[$args['name']] : '';?>
            <select name="form[<?= $args['name']?>]">
                <option value="">&mdash; <?= $args['placeholder']?>  &mdash;</option>
                <?php echo $args['func']($value)?>
            </select>
            <span class="note">Подсказка: <?= !empty($item['note']) ? $args['note'] : ''?></span>
        </div>
    <?php else: ?>
        <?php $key = !empty($item_key) ? $item_key : '0'?>
        <?php $value = !empty($value[$args['name']]) ? $value[$args['name']] : ''?>
        <div class="checkbox-list">
            <?php echo $args['func']($value, $key, $args['name'])?>
        </div>
    <?php endif;?>
        <?php
    }



    /**
     * Hidden
     * @param $args
     * @param $value
     * @return false|void
     */
   function Hidden($args = [], $value = false, $type = 'static', $item_key = false)
    {
        if (empty($args)) return false;
        ?>
        <?php if(!empty($value[$args['name']])):  ?>
        <div class="key-block" style="display:  block;font-size: 10px; padding: 0 10px; color:  green;">Ключ: <?= $value[$args['name']]?></div>
        <?php endif;?>

        <?php if($type == 'static'): ?>
            <input name="form[<?=$args['name']?>]"
                   type="hidden"
                   id="<?=$args['name']?>"
                   value="<?= !empty($value[$args['name']]) ? $value[$args['name']] : '' ?>">
    <?php else: ?>
        <?php $key = !empty($item_key) ? $item_key : '0'?>
        <input name="form[<?= $key?>][<?= $args['name']?>]" type="hidden" id="<?= $args['name']?>" value="<?= !empty($value[$args['name']]) ? $value[$args['name']] : '' ?>">
    <?php endif;?>
        <?php
    }



    /**
     * Textarea
     * @param $args
     * @param $value
     * @return false|void
     */
    public function Textarea($args = [], $value = false, $type = 'static', $item_key = false)
    {
        if (empty($args)) return false;
        ?>
        <?php if($type == 'static'): ?>
        <div class="form-control">
            <label><?=$args['placeholder']?>:</label>
            <textarea name="form[<?=$args['name']?>]"
                      style="height: 200px;"
                      placeholder="<?= $args['placeholder']?>"><?= !empty($value[$args['name']]) ? $value[$args['name']] : '' ?></textarea>
            <span class="note">Подсказка: <?= !empty($item['note']) ? $args['note'] : ''?></span>
        </div>
        <?php else: ?>
        <?php $key = !empty($item_key) ? $item_key : '0'?>
        <textarea name="form[<?= $key?>][<?=$args['name']?>]"
                  style="height: 120px; width: 100%;"
                  placeholder="<?= $args['placeholder']?>"><?= !empty($value[$args['name']]) ? $value[$args['name']] : '' ?></textarea>
        <?php endif;?>
        <?php
    }


    /**
     * Textarea
     * @param $args
     * @param $value
     * @return false|void
     */
    public function Texteditor($args = [], $value = false, $type = 'static', $item_key = false)
    {
        if (empty($args)) return false;
        ?>
        <?php if($type == 'static'): ?>
        <div class="form-control">
            <?php $content = !empty($value[$args['name']]) ? $value[$args['name']] : '';?>
            <label><?=$args['placeholder']?>:</label>
            <?php wp_editor( $content, $args['name'], [ 'textarea_name' => 'form[' . $args['name'] . ']' ]);?>
            <span class="note">Подсказка: <?= !empty($item['note']) ? $args['note'] : ''?></span>
        </div>
        <?php else: ?>
        <?php $key = !empty($item_key) ? $item_key : '0'?>
            <div>Not allowed!</div>
        <?php endif;?>
        <?php
    }

    /**
     * Image
     * @param $args
     * @param $value
     * @param $type
     * @param $item_key
     * @return false|void
     */
    public function Image($args = [], $value = false, $type = 'static', $item_key = false)
    {
        if (empty($args)) return false;
        ?>
        <?php if($type == 'static'): ?>
        <div class="img-block">
            <div class="img-wrapper" <?= !empty($args['width']) && !empty($args['height']) ? 'style="width: ' . $args['width'] . '; height: ' . $args['height'] . ';"' : '' ?>>
                <?php if(!empty($value[$args['name']])): ?>
                    <img src="<?php echo $value[$args['name']]?>">
                <?php endif;?>
            </div>
            <?php if (!empty($args['width']) && !empty($args['height'])) : ?>
                <span class="img-sizes"><strong><?= $args['placeholder']?></strong> (Размеры: <?= $args['width']?> на <?= $args['height']?>) </span>
            <?php endif; ?>
            <div class="btn-add js-add-icon" data-target=".img-wrapper" title="<?= $args['placeholder']?>">
                <span class="dashicons dashicons-insert"></span>
            </div>
            <input type="hidden" class="icon-0-hidden hidden-input" name="form[<?= $args['name']?>]" value="<?= !empty($value[$args['name']]) ? $value[$args['name']] : '' ?>">
        </div>
        <?php else: ?>
        <div class="img-block">
            <div class="img-wrapper" <?= !empty($args['width']) && !empty($args['height']) ? 'style="width: ' . $args['width'] . '; height: ' . $args['height'] . ';"' : '' ?>>
                <?php if(!empty($value[$args['name']])): ?>
                    <img src="<?= $value[$args['name']]?>">
                <?php endif;?>
            </div>
            <?php if (!empty($args['width']) && !empty($args['height'])) : ?>
                <span class="img-sizes"><strong><?= $args['placeholder']?></strong> (Размеры: <?= $args['width']?> на <?= $args['height']?>) </span>
            <?php endif; ?>
            <div class="btn-add js-add-icon" data-target=".img-wrapper" title="<?= $args['placeholder']?>">
                <span class="dashicons dashicons-insert"></span>
            </div>
            <?php $key = !empty($item_key) ? $item_key : '0'?>
            <input type="hidden" class="icon-0-hidden  hidden-input" name="form[<?= $key?>][<?= $args['name']?>]" value="<?= !empty($value[$args['name']]) ? $value[$args['name']] : '' ?>">
        </div>
    <?php endif;?>
<?php
    }







    /**
     * FormStart
     * @return void
     */
    public function FormStart()
    {
        ?>
        <form action="" method="post" id="foo">
        <input name="deleting" type="hidden" value="">
<?php
    }


    /**
     * FormEnd
     * @return void
     */
    public function FormEnd()
    {
        ?>
        </form>
<?php
    }


    /**
     * DinamicBlockStart
     * @return void
     */
    public function DinamicBlockStart()
    {
        ?>
        <div class="form-control bblock first">
<?php
    }


    /**
     * DinamicBlockEnd
     * @return void
     */
    public function DinamicBlockEnd()
    {
        ?>
        <br>
        <span class="note">Подсказка: <?= !empty($this->note) ? $this->note : ''?><a class="remove js-remove" style="float: right" href="#remove" data-id="0">удалить</a></span>
        </div>
        <?php
    }

    /**
     * DinamicAddButton
     * @return void
     */
    public function DinamicAddButton()
    {
        ?>
        <div class="button button-small button-secondary js-add-form" style="float: right; margin: 0 15px 20px 0">Добавить еще</div>
        <?php
    }


    public function Button()
    {
        ?>
        <div class="form-control">
            <input type="submit" name="update_form" id="submit" class="button button-primary" value="Обновить">
        </div>
<?php
    }


    /**
     * @param $params
     * @return void
     */
    public function Render($params = [], $data = false)
    {
        $this->FormStart();
        foreach ($params as $type => $elements)
        {
            if($type == 'static')
            {
                foreach ($elements as $item) {
                    $method = ucfirst($item['type']);
                    $this->$method($item, $data);
                }
            }

            if($type == 'dynamic')
            {
                $this->note = !empty($elements['note']) ? $elements['note'] : '';
                unset($elements['note']);

                if($data)
                {
                    foreach($data as $item_key => $value)
                    {
                        if(is_array($value))
                        {
                            $this->DinamicBlockStart();
                            foreach ($elements as $item) {
                                $method = ucfirst($item['type']);
                                $this->$method($item, $value, $type, $item_key);
                            }
                            $this->DinamicBlockEnd();
                        }
                    }
                }
                else
                {
                    $this->DinamicBlockStart();
                    foreach ($elements as $item) {
                        $method = ucfirst($item['type']);
                        $this->$method($item, $data, $type);
                    }
                    $this->DinamicBlockEnd();
                }

                $this->DinamicAddButton();
            }
        }

        $this->Button();
        $this->FormEnd();
    }


    /**
     * Handler
     * @param $option_key
     * @return void
     */
    public  function Handler($option_key = false)
    {
        if(!empty($_POST['update_form']))
        {

            $result = $_POST['form'];
            update_option($option_key, json_encode($_POST['form'], JSON_UNESCAPED_UNICODE));
            header('Location: ?page=' . $_GET['page'] . '&action=' . $_GET['action'] . '&ok=1');
        }
        else {
            $out = get_option($option_key);
            $result = !empty($out) ? json_decode($out, true): false;
        }

        $this->result = $result;
    }


    /**
     * RequestResult
     * @return void
     */
    public function RequestResult()
    {
        if (!empty($_GET['ok'])): ?>
        <div class="alert-ok">Запрос успешно выполнен!</div>
    <?php endif;
    }


}