<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://medvedev.studio
 * @since      1.0.0
 *
 * @package    Cntools
 * @subpackage Cntools/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Cntools
 * @subpackage Cntools/includes
 * @author     Medvedev Studio < info@medvedev.studio>
 */
class Cntools_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
