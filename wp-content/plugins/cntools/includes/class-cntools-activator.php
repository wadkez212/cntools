<?php

/**
 * Fired during plugin activation
 *
 * @link       https://medvedev.studio
 * @since      1.0.0
 *
 * @package    Cntools
 * @subpackage Cntools/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Cntools
 * @subpackage Cntools/includes
 * @author     Medvedev Studio < info@medvedev.studio>
 */
class Cntools_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
