<?php
// когда пользователь сам редактирует свой профиль
add_action( 'show_user_profile', 'cnts_custom_profile_fields' );
// когда чей-то профиль редактируется админом например
add_action( 'edit_user_profile', 'cnts_custom_profile_fields' );

function cnts_custom_profile_fields( $user ) {

    $option = get_option('cntools-options-supplier-fields');
    if($option) {
        $fields_arr = json_decode($option, true);
        echo '<div style="padding: 20px; margin-top: 20px; background: #cfcfd39c;">';
        echo '<h3>Поля поставшика</h3>';
        echo '<table class="form-table">';

        foreach ($fields_arr as $filed) {
            $title = $filed['title'];
            $key = $filed['key'];
            $value = get_the_author_meta( $key, $user->ID );
            echo '<tr><th><label for="' . $key . '">' . $title . '</label></th>
 	<td><input type="text" name="' . $key . '" id="' . $key . '" value="' . esc_attr( $value ) . '" class="regular-text" /></td>
	</tr>';
        }

        echo '</table>';
        echo '</div>';
    }
}


// когда пользователь сам редактирует свой профиль
add_action( 'personal_options_update', 'cnts_custom_save_profile_fields' );
// когда чей-то профиль редактируется админом
add_action( 'edit_user_profile_update', 'cnts_custom_save_profile_fields' );

function cnts_custom_save_profile_fields( $user_id ) {
    $option = get_option('cntools-options-supplier-fields');
    if($option) {
        $fields_arr = json_decode($option, true);
        foreach ($fields_arr as $filed) {
            $key = $filed['key'];
            update_user_meta( $user_id, $key, sanitize_text_field( $_POST[$key] ) );
        }
    }


}