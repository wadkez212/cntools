<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://medvedev.studio
 * @since             1.0.0
 * @package           Cntools
 *
 * @wordpress-plugin
 * Plugin Name:       CnTools
 * Plugin URI:        https://medvedev.studio
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Medvedev Studio
 * Author URI:        https://medvedev.studio
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       cntools
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CNTOOLS_VERSION', '1.0.1' );
define( 'CNTOOLS_PATH', plugin_dir_path( __FILE__ ) );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-cntools-activator.php
 */
function activate_cntools() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cntools-activator.php';
	Cntools_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-cntools-deactivator.php
 */
function deactivate_cntools() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cntools-deactivator.php';
	Cntools_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_cntools' );
register_deactivation_hook( __FILE__, 'deactivate_cntools' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-cntools.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_cntools() {

	$plugin = new Cntools();
	$plugin->run();

}
run_cntools();


function custom_post_type_faq() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Часто задаваемые вопросы', 'Post Type General Name', 'cntools' ),
        'singular_name'       => _x( 'Часто задаваемые вопрос', 'Post Type Singular Name', 'cntools' ),
        'menu_name'           => __( 'Часто задаваемые вопросы', 'cntools' ),
        'parent_item_colon'   => __( 'Parent ', 'cntools' ),
        'all_items'           => __( 'Все FAQ', 'cntools' ),
        'view_item'           => __( 'Просмотр', 'cntools' ),
        'add_new_item'        => __( 'Добавить новую', 'cntools' ),
        'add_new'             => __( 'Добавить', 'cntools' ),
        'edit_item'           => __( 'Редактировать', 'cntools' ),
        'update_item'         => __( 'Обновить', 'cntools' ),
        'search_items'        => __( 'Поиск', 'cntools' ),
        'not_found'           => __( 'Не смогли найти', 'cntools' ),
        'not_found_in_trash'  => __( 'Не смогли найти', 'cntools' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'Часто задаваемые вопросы', 'cntools' ),
        'description'         => __( 'Управление FAQs', 'cntools' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor',  'thumbnail', 'revisions', 'templates' ), // 'excerpt', //'custom-fields', //  'author',
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => false,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-editor-help',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,

    );

    // Registering your Custom Post Type
    register_post_type( 'faq', $args );

}

add_action( 'init', 'custom_post_type_faq', 0 );



function custom_post_type_slider() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Управление слайдерамы', 'Post Type General Name', 'cntools' ),
        'singular_name'       => _x( 'Слайды', 'Post Type Singular Name', 'cntools' ),
        'menu_name'           => __( 'Слайды', 'cntools' ),
        'parent_item_colon'   => __( 'Parent ', 'cntools' ),
        'all_items'           => __( 'Все Слайды', 'cntools' ),
        'view_item'           => __( 'Просмотр', 'cntools' ),
        'add_new_item'        => __( 'Добавить новую', 'cntools' ),
        'add_new'             => __( 'Добавить', 'cntools' ),
        'edit_item'           => __( 'Редактировать', 'cntools' ),
        'update_item'         => __( 'Обновить', 'cntools' ),
        'search_items'        => __( 'Поиск', 'cntools' ),
        'not_found'           => __( 'Не смогли найти', 'cntools' ),
        'not_found_in_trash'  => __( 'Не смогли найти', 'cntools' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'Слайды', 'cntools' ),
        'description'         => __( 'Слайды', 'cntools' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'thumbnail','templates',  ), // 'excerpt'  // 'excerpt', //'custom-fields', //  'author', // 'editor',  'revisions',
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'sliders_cat' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => false,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 6,
        'menu_icon'           => 'dashicons-images-alt',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,

    );

    // Registering your Custom Post Type
    register_post_type( 'slider', $args );

}

add_action( 'init', 'custom_post_type_slider', 0 );


add_action( 'init', 'create_slider_cat_hierarchical_taxonomy', 0 );
function create_slider_cat_hierarchical_taxonomy() {

    $labels = array(
        'name' => _x( 'Слайдеры', 'taxonomy general name' ),
        'singular_name' => _x( 'Слайдер', 'taxonomy singular name' ),
        'search_items' =>  __( 'Пойск слайдера' ),
        'all_items' => __( 'Все слайдеры' ),
//        'parent_item' => __( 'მშობელი კატეგორია' ),
//        'parent_item_colon' => __( 'მშობელი კატეგორია:' ),
        'edit_item' => __( 'Редактировать' ),
        'update_item' => __( 'Обновить' ),
        'add_new_item' => __( 'Новая' ),
        'new_item_name' => __( 'Название нового салйдера' ),
        'menu_name' => __( 'Слайдеры' ),
    );

    register_taxonomy('sliders_cat', array('slider'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_in_rest' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'sliders_cat' ),
    ));

}






function custom_post_type_video_review() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Видео обзоры', 'Post Type General Name', 'cntools' ),
        'singular_name'       => _x( 'Видео обзор', 'Post Type Singular Name', 'cntools' ),
        'menu_name'           => __( 'Видео обзоры', 'cntools' ),
        'parent_item_colon'   => __( 'Parent ', 'cntools' ),
        'all_items'           => __( 'Все видео обзоры', 'cntools' ),
        'view_item'           => __( 'Просмотр', 'cntools' ),
        'add_new_item'        => __( 'Добавить новую', 'cntools' ),
        'add_new'             => __( 'Добавить', 'cntools' ),
        'edit_item'           => __( 'Редактировать', 'cntools' ),
        'update_item'         => __( 'Обновить', 'cntools' ),
        'search_items'        => __( 'Поиск', 'cntools' ),
        'not_found'           => __( 'Не смогли найти', 'cntools' ),
        'not_found_in_trash'  => __( 'Не смогли найти', 'cntools' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'Видео обзоры', 'cntools' ),
        'description'         => __( 'Управление видео обзорамы', 'cntools' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor',  'thumbnail', 'revisions', 'templates' ), // 'excerpt', //'custom-fields', //  'author',
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => false,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 7,
        'menu_icon'           => 'dashicons-video-alt3',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,

    );

    // Registering your Custom Post Type
    register_post_type( 'video_review', $args );

}

add_action( 'init', 'custom_post_type_video_review', 0 );



function custom_post_type_partnership() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Запросы на партнерства', 'Post Type General Name', 'cntools' ),
        'singular_name'       => _x( 'Запросы на партнерства', 'Post Type Singular Name', 'cntools' ),
        'menu_name'           => __( 'Запросы на партнерства', 'cntools' ),
        'parent_item_colon'   => __( 'Parent ', 'cntools' ),
        'all_items'           => __( 'Все запросы на партнерства', 'cntools' ),
        'view_item'           => __( 'Просмотр', 'cntools' ),
        'add_new_item'        => __( 'Добавить новую', 'cntools' ),
        'add_new'             => __( 'Добавить', 'cntools' ),
        'edit_item'           => __( 'Редактировать', 'cntools' ),
        'update_item'         => __( 'Обновить', 'cntools' ),
        'search_items'        => __( 'Поиск', 'cntools' ),
        'not_found'           => __( 'Не смогли найти', 'cntools' ),
        'not_found_in_trash'  => __( 'Не смогли найти', 'cntools' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'Запросы на партнерства', 'cntools' ),
        'description'         => __( 'Управление запросов на партнерства', 'cntools' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor',  'thumbnail', 'revisions', 'templates' ), // 'excerpt', //'custom-fields', //  'author',
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => false,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 8,
        'menu_icon'           => 'dashicons-money',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,

    );

    // Registering your Custom Post Type
    register_post_type( 'partnership', $args );

}

add_action( 'init', 'custom_post_type_partnership', 0 );




function custom_post_type_callback() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Запросы на обратный звонок', 'Post Type General Name', 'cntools' ),
        'singular_name'       => _x( 'Запрос на обратный звонок', 'Post Type Singular Name', 'cntools' ),
        'menu_name'           => __( 'Запросы на обратный звонок', 'cntools' ),
        'parent_item_colon'   => __( 'Parent ', 'cntools' ),
        'all_items'           => __( 'Все Запросы', 'cntools' ),
        'view_item'           => __( 'Просмотр', 'cntools' ),
        'add_new_item'        => __( 'Добавить новую', 'cntools' ),
        'add_new'             => __( 'Добавить', 'cntools' ),
        'edit_item'           => __( 'Редактировать', 'cntools' ),
        'update_item'         => __( 'Обновить', 'cntools' ),
        'search_items'        => __( 'Поиск', 'cntools' ),
        'not_found'           => __( 'Не смогли найти', 'cntools' ),
        'not_found_in_trash'  => __( 'Не смогли найти', 'cntools' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'Обратный звонок', 'cntools' ),
        'description'         => __( 'Обратный звонок', 'cntools' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor',  'thumbnail', 'revisions', 'templates' ), // 'excerpt', //'custom-fields', //  'author',
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => false,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 9,
        'menu_icon'           => 'dashicons-phone',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,

    );

    // Registering your Custom Post Type
    register_post_type( 'callback', $args );

}

add_action( 'init', 'custom_post_type_callback', 0 );


// suppliers
function custom_post_type_suppliers() {

// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Поставщики', 'Post Type General Name', 'cntools' ),
        'singular_name'       => _x( 'Поставщики', 'Post Type Singular Name', 'cntools' ),
        'menu_name'           => __( 'Поставщики', 'cntools' ),
        'parent_item_colon'   => __( 'Parent ', 'cntools' ),
        'all_items'           => __( 'Все Поставщики', 'cntools' ),
        'view_item'           => __( 'Просмотр', 'cntools' ),
        'add_new_item'        => __( 'Добавить новую', 'cntools' ),
        'add_new'             => __( 'Добавить', 'cntools' ),
        'edit_item'           => __( 'Редактировать', 'cntools' ),
        'update_item'         => __( 'Обновить', 'cntools' ),
        'search_items'        => __( 'Поиск', 'cntools' ),
        'not_found'           => __( 'Не смогли найти', 'cntools' ),
        'not_found_in_trash'  => __( 'Не смогли найти', 'cntools' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'Поставщики', 'cntools' ),
        'description'         => __( 'Поставщики', 'cntools' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title',  'thumbnail', 'revisions', 'templates' ), // 'excerpt', //'custom-fields', //  'author', .. 'editor',
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => false,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 10,
        'menu_icon'           => 'dashicons-businessman',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,

    );

    // Registering your Custom Post Type
    register_post_type( 'suppliers', $args );

}

add_action( 'init', 'custom_post_type_suppliers', 0 );



add_action('admin_menu', 'my_admin_menu');
function my_admin_menu() {
    add_submenu_page('cntools', 'Часто задаваемые вопросы', 'Часто задаваемые вопросы', 'manage_options', 'edit.php?post_type=faq');
    add_submenu_page('cntools', 'Управление слайдерамы', 'Управление слайдерамы', 'manage_options', 'edit.php?post_type=slider');
    add_submenu_page('cntools', 'Видео обзоры', 'Видео обзоры', 'manage_options', 'edit.php?post_type=video_review');
    add_submenu_page('cntools', 'Запросы на партнерства', 'Запросы на партнерства', 'manage_options', 'edit.php?post_type=partnership');
    add_submenu_page('cntools', 'Запросы на обратный звонок', 'Запросы на обратный звонок', 'manage_options', 'edit.php?post_type=callback');
    add_submenu_page('cntools', 'Поставщики', 'Поставщики', 'manage_options', 'edit.php?post_type=suppliers');
    add_submenu_page('cntools', 'СДЕК ПВЗ', 'СДЕК ПВЗ', 'manage_options', 'edit.php?post_type=cdek_pvz');
}



function cntool_product_cat_option_list($value = false)
{

    $categories = get_categories( [
        'taxonomy'     => 'product_cat',
        'orderby'      => 'term_ID',
        'order'        => 'ASC',
        'hide_empty'   => 0
    ] );

    $select_options = '';

    if(!empty($categories))
    {
        foreach ($categories as $item)
        {
            $selected = !empty($value) && $value == $item->term_id  ? 'selected="selected"' : '';
            $select_options .= '<option ' . $selected . '  value="' . $item->term_id . '">' . $item->name . '</option>';
        }

    }

    return $select_options;
}


/**
 *  cntools_multi_form
 * @param $data
 * @param $fields
 * @return void
 */
function cntools_multi_form($data = false, $fields = []) {
    ?>

    <div class="form-control bblock template hide">
        <?php  if (!empty($fields['icon'])):  ?>
            <div class="d-block-icon-wrapper">
                <div class="icon icon-new_"></div>
                <div class="button button-small button-secondary js-add-icon" data-target=".icon-new_"><?= $fields['icon']['title']?></div>
            </div>
            <input type="hidden" class="icon-new_-hidden" name="form[new_][icon]" value="">
        <?php  endif;   ?>

        <?php  if (!empty($fields['elements'])):  ?>
            <?php  foreach($fields['elements'] as $element):  ?>
                <?php if ($element['type'] == 'text'): ?>
                    <input name="form[new_][<?= $element['name']?>]" type="text" id="<?= $element['name']?>" value="" placeholder="<?= $element['placeholder']?>">
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>

        <br>
        <span class="note">Подсказка: <?= $fields['note']?><a class="remove js-remove" style="float: right" href="#remove" data-id="">удалить</a></span>
    </div>

    <form action="" method="post">

        <input name="deleting" type="hidden" value="">

        <?php if (!empty($data)) : ?>
            <?php foreach ($data as $key => $item): ?>
                <div class="form-control bblock  <?= $key == 0 ? 'first' : 'cloned' ?>">
                    <?php  if (!empty($fields['icon'])):  ?>
                        <div class="d-block-icon-wrapper">
                            <div class="icon icon-<?=$key?>">
                                <?php if(!empty($item['icon'])): ?>
                                    <img src="<?php echo $item['icon']?>">
                                <?php endif;?>
                            </div>
                            <div class="button button-small button-secondary js-add-icon" data-target=".icon-<?=$key?>"><?= $fields['icon']['title']?></div>
                        </div>
                        <input type="hidden" class="icon-<?=$key?>-hidden" name="form[<?= $key?>][icon]" value="<?= $item['icon']?>">
                    <?php  endif;   ?>

                    <?php  if (!empty($fields['elements'])):  ?>
                        <?php  foreach($fields['elements'] as $element):  ?>
                            <?php if ($element['type'] == 'text'): ?>
                                <input name="form[<?= $key?>][<?= $element['name']?>]" type="text" id="<?= $element['name']?>" value="<?= !empty($item[$element['name']]) ? $item[$element['name']] : ''?>" placeholder="<?= $element['placeholder']?>">
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>

                    <br>
                    <span class="note">Подсказка: <?= $fields['note']?><a class="remove js-remove" style="float: right" href="#remove" data-id="<?= $key?>">удалить</a></span>


                </div>
            <?php endforeach;?>
        <?php else: ?>

            <div class="form-control bblock first">
                <?php  if (!empty($fields['icon'])):  ?>
                    <div class="d-block-icon-wrapper">
                        <div class="icon icon-new_"></div>
                        <div class="button button-small button-secondary js-add-icon" data-target=".icon-new_"><?= $fields['icon']['title']?></div>
                    </div>
                    <input type="hidden" class="icon-new_-hidden" name="form[new_][icon]" value="">
                <?php  endif;   ?>

                <?php  if (!empty($fields['elements'])):  ?>
                    <?php  foreach($fields['elements'] as $element):  ?>
                        <?php if ($element['type'] == 'text'): ?>
                            <input name="form[new_][<?= $element['name']?>]" type="text" id="<?= $element['name']?>" value="" placeholder="<?= $element['placeholder']?>">
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>

                <br>
                <span class="note">Подсказка: <?= $fields['note']?><a class="remove js-remove" style="float: right" href="#remove" data-id="">удалить</a></span>
            </div>

        <?php endif;?>



        <div class="button button-small button-secondary js-add-form"  style="float: right; margin: 0 15px 20px 0">Добавить еще</div>

        <div class="form-control">
            <input type="submit" name="update_form" id="submit" class="button button-primary" value="Обновить">
        </div>
    </form>
    <?php
}


/**
 * taxi_angel_get_pages_list
 * @param $value
 * @return string
 */
function cntools_get_pages_list($value=false)
{
    $select_options = '';
    $pages = get_pages([
        'post_type' => 'page',
        'post_status' => 'publish',
    ]);
    if(!empty($pages))
    {
        $selected = !empty($value) && $value == 'all'  ? 'selected="selected"' : '';
//        $select_options .= '<option ' . $selected . '  value="all"> Эту запись использовать для всех городов</option>';
        foreach ($pages as $item)
        {
            $selected = !empty($value) && $value == $item->ID  ? 'selected="selected"' : '';
            $select_options .= '<option ' . $selected . '  value="' . $item->ID . '">' . $item->post_title . '</option>';
        }

    }

    return $select_options;
}





/**
 * taxi_angel_get_pages_list
 * @param $value
 * @return string
 */
function cntools_get_pvz_citys($value=false)
{
    $select_options = '';
    $categories = get_categories( [
        'taxonomy'     => 'cdek_pvz_cat',
        'child_of'     => 0,
        'parent'       => '0',
        'orderby'      => 'name',
        'order'        => 'ASC',
        'hide_empty'   => true,
        'hierarchical' => 1,
    ] );
    if(!empty($categories))
    {
        $selected = !empty($value) && $value == 'all'  ? 'selected="selected"' : '';
//        $select_options .= '<option ' . $selected . '  value="all"> Эту запись использовать для всех городов</option>';
        foreach ($categories as $item)
        {
            $select_options .= '<optgroup label="' .  $item->name . '">';
            $sub_categories = get_categories( [
                'taxonomy'     => 'cdek_pvz_cat',
                'child_of'     => $item->term_id,
            ] );
            foreach ($sub_categories as $sub_item) {
                $selected = !empty($value) && $value == $sub_item->cat_ID  ? 'selected="selected"' : '';
                $select_options .= '<option ' . $selected . '  value="' . $sub_item->cat_ID . '">' . $sub_item->name . '</option>';
            }
            $select_options .= '</optgroup>';
        }

    }

    return $select_options;
}




/**
 * get_cntools_categories
 * @param $value
 * @return string
 */
function get_cntools_categories($value=false)
{
    $select_options = '';
    $categories = get_categories([
        'taxonomy' => 'product_cat'
    ]);

    if(!empty($categories))
    {

        foreach ($categories as $item)
        {
            $selected = !empty($value) && $value == $item->term_id  ? 'selected="selected"' : '';
            $select_options .= '<option ' . $selected . '  value="' . $item->term_id . '">' . $item->name . '</option>';
        }

    }

    return $select_options;
}



/**
 * cntools_get_yes_no
 * @param $value
 * @return string
 */
function cntools_get_blocks_on_main($value = false)
{
    $options = '';
    $variation = [
         'template-parts/front-page-categories' => 'Категории на главной',
         'template-parts/front-page-popular-products' => 'Популярные продукты',
         'template-parts/front-page-company-info' => 'Наша компания',
         'template-parts/front-page-faq' => 'Часто задаваемые вопросы',
         'template-parts/front-contacts' => 'Свяжитесь с нами',
         'template-parts/front-page-become-partner' => 'Хотите стать партнерем?',
    ];
    foreach ($variation as $key => $item)
    {
        $selected = !empty($value) && $value == $key  ? 'selected="selected"' : '';
        $options .= '<option ' . $selected . '  value="' . $key . '">' . $item . '</option>';
    }

    return $options;
}

/**
 * cntools_banner_styles_list
 * @return void
 */
function cntools_banner_styles_list($value = false)
{
    $style = [
        'blue_and_gray' => 'Синий и серый [blue_and_gray]',
        'yell_percent_bg' => 'Желтый фоном процента [yell_percent_bg]',
        'bg_image' => 'Фоновым изображением [bg_image]',
        'bg_image_2' => 'Фоновым изображением [bg_image_2]',
        'bg_image_blue' => 'Фоновым изображением (Синий) [bg_image_blue]',
        'bg_image_grey' => 'Фоновым изображением (серый) [bg_image_grey]',
        'bg_gradient_blue' => 'Синый градиент (серый) [bg_gradient_blue]',
    ];
    foreach ($style as $key => $item)
    {
        $selected = !empty($value) && $value == $key  ? 'selected="selected"' : '';
        $options .= '<option ' . $selected . '  value="' . $key . '">' . $item . '</option>';
    }
    return $options;
}



/**
 * cntools_get_yes_no
 * @param $value
 * @return string
 *
 */
function cntools_get_yes_no($value = false)
{
     $options = '';
     $variation = ['yes' => 'Да', 'no'=> 'Нет'];
     foreach ($variation as $key => $item)
     {
         $selected = !empty($value) && $value == $key  ? 'selected="selected"' : '';
         $options .= '<option ' . $selected . '  value="' . $key . '">' . $item . '</option>';
     }

     return $options;
}

/**
 * cntools_get_yes_no
 * @param $value
 * @return string
 *
 */
function cntools_get_titles($value = false, $key = false, $name = false)
{
     $result = get_product_properties_all_titles();
     foreach ($result as $k => $item)
     {
         $checked = !empty($value['k_' . $k]) && $value['k_' . $k] == $item  ? 'checked="checked"' : '';
         ?>
         <label>
             <input type="checkbox" name="form[<?= $key?>][<?=$name?>][k_<?= $k?>]" value="<?= $item?>" <?= $checked?>> &mdash; <?= $item?>
         </label>
<?php

     }
}


/**
 *  cntools_multi_form
 * @param $data
 * @param $fields
 * @return void
 */
function cntools_multiform($data = false, $fields = [], $static_fields = [], $result_static = false) {
    ?>

    <div class="form-control bblock template hide">
        <?php  if (!empty($fields['icon'])):  ?>
            <div class="d-block-icon-wrapper">
                <div class="icon icon-new_"></div>
                <div class="button button-small button-secondary js-add-icon" data-target=".icon-new_"><?= $fields['icon']['title']?></div>
            </div>
            <input type="hidden" class="icon-new_-hidden" name="form[new_][icon]" value="">
        <?php  endif;   ?>

        <?php  if (!empty($fields['img'])):  ?>
            <div class="d-block-img-wrapper">
                <div class="img img-new_"></div>
                <div class="button button-small button-secondary js-add-icon" data-target=".img-new_"><?= $fields['img']['title']?></div>
            </div>
            <input type="hidden" class="img-new_-hidden" name="form[new_][img]" value="">
        <?php  endif;   ?>

        <?php  if (!empty($fields['elements'])):  ?>
            <?php  foreach($fields['elements'] as $element):  ?>

                <?php if ($element['type'] == 'text'): ?>
                    <input name="form[new_][<?= $element['name']?>]" type="text" id="<?= $element['name']?>" value="" placeholder="<?= $element['placeholder']?>">
                <?php endif; ?>

                <?php if ($element['type'] == 'hidden'): ?>
                    <input name="form[new_][<?= $element['name']?>]" type="hidden" id="<?= $element['name']?>" value="" placeholder="<?= $element['placeholder']?>">
                <?php endif; ?>

                <?php if ($element['type'] == 'select'): ?>
                    <select name="form[new_][<?= $element['name']?>]">
                        <option value="">&mdash; <?= $element['placeholder']?>  &mdash;</option>
                        <?php echo $element['func']()?>
                    </select>
                <?php endif; ?>

            <?php endforeach; ?>
        <?php endif; ?>

        <br>
        <span class="note">Подсказка: <?= $fields['note']?><a class="remove js-remove" style="float: right" href="#remove" data-id="">удалить</a></span>
    </div>

    <form action="" method="post">

        <?php if ( !empty($static_fields) ): ?>
            <?php foreach ($static_fields as $item):  ?>
                <?php if($item['type'] == 'text'): ?>
                    <div class="form-control">
                        <label><?=$item['placeholder']?>:</label>
                        <input name="static[<?=$item['name']?>]" type="text" id="<?=$item['name']?>" value="<?= !empty($result_static[$item['name']]) ? $result_static[$item['name']] : '' ?>" placeholder="<?=$item['placeholder']?>">
                        <span class="note">Подсказка: <?= !empty($item['note']) ? $item['note'] : ''?></span>
                    </div>
                <?php endif; ?>

                <?php if($item['type'] == 'textarea'): ?>
                    <div class="form-control">
                        <label><?=$item['placeholder']?>:</label>
                        <textarea name="static[<?=$item['name']?>]" style="height: 300px;" placeholder="<?=$item['placeholder']?>"><?= !empty($result_static[$item['name']]) ? $result_static[$item['name']] : '' ?></textarea>
                        <span class="note">Подсказка: <?= !empty($item['note']) ? $item['note'] : ''?></span>
                    </div>
                <?php endif; ?>

                <?php if($item['type'] == 'img'): ?>
                    <div class="form-control">
                        <div class="d-block-img-wrapper">
                            <div class="img img-<?=$item['name']?>">
                                <?php if(!empty($result_static[$item['name']])): ?>
                                    <img src="<?php echo $result_static[$item['name']]['img']?>">
                                <?php endif;?>
                            </div>
                            <div class="button button-small button-secondary js-add-icon" data-target=".img-<?=$item['name']?>"><?=$item['placeholder']?></div>
                        </div>
                        <input type="hidden" class="img-<?=$item['name']?>-hidden" name="static[<?=$item['name']?>][img]" value="<?= !empty($result_static[$item['name']]['img']) ? $result_static[$item['name']]['img'] : '' ?>">
                    </div>
                <?php endif; ?>

            <?php endforeach;  ?>
        <?php endif; ?>

        <input name="deleting" type="hidden" value="">

        <?php if (!empty($data)) : ?>
            <?php foreach ($data as $key => $item): ?>
                <div class="form-control bblock  <?= $key == 0 ? 'first' : 'cloned' ?>">
                    <?php  if (!empty($fields['icon'])):  ?>
                        <div class="d-block-icon-wrapper">
                            <div class="icon icon-<?=$key?>">
                                <?php if(!empty($item['icon'])): ?>
                                    <img src="<?php echo $item['icon']?>">
                                <?php endif;?>
                            </div>
                            <div class="button button-small button-secondary js-add-icon" data-target=".icon-<?=$key?>"><?= $fields['icon']['title']?></div>
                        </div>
                        <input type="hidden" class="icon-<?=$key?>-hidden" name="form[<?= $key?>][icon]" value="<?= !empty($item['icon']) ? $item['icon'] : '' ?>">
                    <?php  endif;   ?>


                    <?php  if (!empty($fields['img'])):  ?>
                        <div class="d-block-img-wrapper">
                            <div class="img img-<?=$key?>">
                                <?php if(!empty($item['img'])): ?>
                                    <img src="<?php echo $item['img']?>">
                                <?php endif;?>
                            </div>
                            <div class="button button-small button-secondary js-add-icon" data-target=".img-<?=$key?>"><?= $fields['img']['title']?></div>
                        </div>
                        <input type="hidden" class="img-<?=$key?>-hidden" name="form[<?= $key?>][img]" value="<?= !empty($item['img']) ? $item['img'] : '' ?>">
                    <?php  endif;   ?>


                    <?php  if (!empty($fields['elements'])):  ?>
                        <?php  foreach($fields['elements'] as $element):  ?>
                            <?php if ($element['type'] == 'text'): ?>
                                <input name="form[<?= $key?>][<?= $element['name']?>]" type="text" id="<?= $element['name']?>" value="<?= !empty($item[$element['name']]) ?   str_replace('\&quot;', '&quot;',  esc_html($item[$element['name']]) ): ''?>" placeholder="<?= $element['placeholder']?>">
                            <?php endif; ?>

                            <?php if ($element['type'] == 'hidden'): ?>
                                <input name="form[<?= $key?>][<?= $element['name']?>]" type="hidden" id="<?= $element['name']?>" value="<?= !empty($item[$element['name']]) ? $item[$element['name']] : ''?>" placeholder="<?= $element['placeholder']?>">
                            <?php endif; ?>

                            <?php if ($element['type'] == 'select'): ?>
                                <?php $value = !empty($item[$element['name']]) ? $item[$element['name']] : '';?>
                                <select name="form[<?= $key?>][<?= $element['name']?>]">
                                    <option value="">&mdash; <?= $element['placeholder']?>  &mdash;</option>
                                    <?php echo $element['func']($value)?>
                                </select>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>

                    <br>
                    <span class="note">Подсказка: <?= $fields['note']?><a class="remove js-remove" style="float: right" href="#remove" data-id="<?= $key?>">удалить</a></span>


                </div>
            <?php endforeach;?>
        <?php else: ?>

            <div class="form-control bblock first">
                <?php  if (!empty($fields['icon'])):  ?>
                    <div class="d-block-icon-wrapper">
                        <div class="icon icon-new_"></div>
                        <div class="button button-small button-secondary js-add-icon" data-target=".icon-new_"><?= $fields['icon']['title']?></div>
                    </div>
                    <input type="hidden" class="icon-new_-hidden" name="form[new_][icon]" value="">
                <?php  endif;   ?>


                <?php  if (!empty($fields['img'])):  ?>
                    <div class="d-block-img-wrapper">
                        <div class="img img-new_"></div>
                        <div class="button button-small button-secondary js-add-icon" data-target=".img-new_"><?= $fields['img']['title']?></div>
                    </div>
                    <input type="hidden" class="img-new_-hidden" name="form[new_][img]" value="">
                <?php  endif;   ?>


                <?php  if (!empty($fields['elements'])):  ?>
                    <?php  foreach($fields['elements'] as $element):  ?>
                        <?php if ($element['type'] == 'text'): ?>
                            <input name="form[new_][<?= $element['name']?>]" type="text" id="<?= $element['name']?>" value="" placeholder="<?= $element['placeholder']?>">
                        <?php endif; ?>

                        <?php if ($element['type'] == 'hidden'): ?>
                            <input name="form[new_][<?= $element['name']?>]" type="hidden" id="<?= $element['name']?>" value="" placeholder="<?= $element['placeholder']?>">
                        <?php endif; ?>

                        <?php if ($element['type'] == 'select'): ?>
                            <select name="form[new_][<?= $element['name']?>]">
                                <option value="">&mdash; <?= $element['placeholder']?>  &mdash;</option>
                                <?php echo $element['func']()?>
                            </select>
                        <?php endif; ?>

                    <?php endforeach; ?>
                <?php endif; ?>

                <br>
                <span class="note">Подсказка: <?= $fields['note']?><a class="remove js-remove" style="float: right" href="#remove" data-id="">удалить</a></span>
            </div>

        <?php endif;?>



        <div class="button button-small button-secondary js-add-form"  style="float: right; margin: 0 15px 20px 0">Добавить еще</div>

        <div class="form-control">
            <input type="submit" name="update_form" id="submit" class="button button-primary" value="Обновить">
        </div>
    </form>
    <?php
}


//
//function CdekClient() {
//    $out = get_option('cntools-options-sdek');
//    $result = json_decode($out, true);
//    require_once 'public/vendor/autoload.php';
//    $client = new \CdekSDK\CdekClient($result['account'], $result['password']);
//    return $client;
//
//}
//
//
//
require_once 'public/vendor/autoload.php';
require 'public/CdekConnection.php';
require 'includes/Form.php';
//require 'includes/supplier-fields.php';