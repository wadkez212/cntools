<?php

/**
 * elementText
 * @param $args
 * @param $value
 * @return void
 */
function elementText($args, $value = false)
{
    ?>
    <?php if($args['type'] == 'text'): ?>
    <div class="form-control">
        <label><?=$args['placeholder']?>:</label>
        <input name="form[<?=$args['name']?>]"
               type="text"
               id="<?=$args['name']?>"
               value="<?= !empty($value[$args['name']]) ? $value[$args['name']] : '' ?>"
               placeholder="<?=$args['placeholder']?>">
        <span class="note">Подсказка: <?= !empty($item['note']) ? $args['note'] : ''?></span>
    </div>
    <?php endif; ?>
<?php
}


/**
 * elementTextArea
 * @param $args
 * @param $value
 * @return void
 */
function elementTextArea($args, $value = false)
{
    ?>
    <?php if($args['type'] == 'text'): ?>
    <div class="form-control">
        <label><?=$args['placeholder']?>:</label>
        <textarea name="form[<?=$args['name']?>]"
                  style="height: 300px;"
                  placeholder="<?= $args['placeholder']?>"><?= !empty($value[$args['name']]) ? $value[$args['name']] : '' ?></textarea>
        <span class="note">Подсказка: <?= !empty($item['note']) ? $args['note'] : ''?></span>
    </div>
<?php endif; ?>
    <?php
}

