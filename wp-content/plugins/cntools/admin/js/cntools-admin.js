window.soundsUrl = {
	newOrderSound : 'https://cntools.ru/wp-content/themes/cntools-app/assets/sounds/new_order.mp3'
}

/**
 *
 * @param config
 * @constructor
 */
function JSCntools(config) {
	this.config = config || {};
	window.custom_uploader = null;
	window.newOrderAudio = new Audio(soundsUrl.newOrderSound);
	this.params = {};
	this.init();
}

JSCntools.prototype = {
	init: function () {
		console.log('init');
		this.checker();
		$ = jQuery;
		this.unbind();
		this.bindEvents();
		_this = this;
		$('[data-toggle="datepicker"]').datepicker({
			language: 'ru-RU',
			format: 'mm.dd.yyyy'
		});
		Sortable.create(foo, {
			group: 'foo',
			animation: 100
		});
	},
	unbind: function () {
		$( '.js-add-icon' ).unbind( "click touch", this.addIcon );
	},
	bindEvents: function () {
		$(document).on('click touch', '#upload_image_button', this.runUploader);
		$(document).on('click touch', '.js-add-icon', this.addIcon);
		$(document).on('click touch', '.js-add-file', this.addFile);
		$(document).on('click touch', '.js-add-service', this.addService);
		$(document).on('click touch', '.js-add-form', this.addForm);
		$(document).on('click touch', '.js-remove', this.formRemove);
		$(document).on('change', '.js-soruce', this.changeSource);
		$(document).on('keyup click change', '#title', this.transliterTitle);
		$(document).on('click touch', '.js-send-payment', this.sendPayment);
		$(document).on('click touch', '.js-import-csv', this.importCsv);
		$(document).on('click touch', '.js-add-line', this.addLine);
		$(document).on('click touch', '.js-delete-tr', this.deleteTr);
		$(document).on('click touch', '.js-sound-trigger', this.soundTrigger);
		$(document).on('click touch', '.js-get-statistics', this.getStatistics);
		$(document).on('click touch', '.js-send-action', this.sendAction);
	},
	runUploader: function (e) {
		e.preventDefault();
		//If the uploader object has already been created, reopen the dialog
		if (custom_uploader) {
			custom_uploader.open();
			return;
		}
		//Extend the wp.media object
		custom_uploader = wp.media.frames.file_frame = wp.media({
			title: 'Choose Image',
			button: {
				text: 'Choose Image'
			},
			multiple: false
		});
		//When a file is selected, grab the URL and set it as the text field's value
		custom_uploader.on('select', function() {
			attachment = custom_uploader.state().get('selection').first().toJSON();
			$('#upload_image').val(attachment.url);
		});
		//Open the uploader dialog
		custom_uploader.open();
	},
	addIcon: function (e) {
		e.preventDefault();
		    var $self = $(this),
			$data = $self.data(),
			$parentBlock = $self.parents('.img-block').find($data.target);

			window.globalSelf = $self;

		console.log($data.target);

		//If the uploader object has already been created, reopen the dialog
		if (custom_uploader) {
			custom_uploader.open();
			return;
		}
		//Extend the wp.media object
		custom_uploader = wp.media.frames.file_frame = wp.media({
			title: 'Выбрать изображение',
			button: {
				text: 'Вставить изображение'
			},
			multiple: false
		});

		//When a file is selected, grab the URL and set it as the text field's value
		custom_uploader.on('select', function() {
			attachment = custom_uploader.state().get('selection').first().toJSON();
			// $($data.target + '-hidden').val(attachment.url);
			$(globalSelf.parents('.img-block').find('.hidden-input')).val(attachment.url);
			globalSelf.parents('.img-block').find(globalSelf.data('target')).html('<img src="' + attachment.url + '">');
		});
		//Open the uploader dialog
		custom_uploader.open();

	},
	addFile: function (e) {
		e.preventDefault();
		var $self = $(this),
			$data = $self.data(),
			$parentBlock = $self.parents('.img-block').find($data.target);

		window.globalSelf = $self;

		//If the uploader object has already been created, reopen the dialog
		if (custom_uploader) {
			custom_uploader.open();
			return;
		}
		//Extend the wp.media object
		custom_uploader = wp.media.frames.file_frame = wp.media({
			title: 'Выбрать файл',
			button: {
				text: 'Вставить файл'
			},
			multiple: false
		});

		//When a file is selected, grab the URL and set it as the text field's value
		custom_uploader.on('select', function() {
			attachment = custom_uploader.state().get('selection').first().toJSON();
			var fileArr = attachment.url.split('/'), fileName = fileArr[fileArr.length-1];
			globalSelf.parents('.file-selector-block').find(globalSelf.data('target')).html(fileName);
			globalSelf.parents('.file-selector-block').find(globalSelf.data('target')).attr('href', attachment.url);
		});
		//Open the uploader dialog
		custom_uploader.open();

	},
	addService: function () {
		let elem = $('.first').clone();
		let result = elem.removeClass('first').addClass('cloned');
		result.find("input").val("")
		result.find("input").attr("name", 'form[new_' + $('.cloned').length + ']');
		result.find(".js-remove").attr("data-id", '');
		$(this).before(result);
	},
	addForm: function () {
		let elem = $('.bblock').last().clone();
		let result = elem.removeClass('template').addClass('cloned').removeClass('hide');
		let generateCode = Math.floor((Math.random() * 10000000) + 1);
		$(this).before(result);
		$(result).find('.key-block').remove();
		$(result.find('[name]')).each(function (ix, val) {
			$(val).val('');
			var pattern = /\[[0-9]*?\]/gm;
			var newAttrName = $(val).attr('name').replace(pattern, '[' + generateCode + ']');
			$(val).attr('name', newAttrName);
		});
	},
	formRemove: function () {
		if(confirm('Удалить поле?')){
			let deletingVal = $('input[name="deleting"]').val();
			$('input[name="deleting"]').val(deletingVal + ',' + $(this).data('id'))
			$(this).parent().parent().remove();
		}
	},
	changeSource: function () {
		if($(this).val() == 'page')
		{
			$('.cat').addClass('hide');
			$('.page').removeClass('hide');
		}

		if($(this).val() == 'category')
		{
			$('.page').addClass('hide');
			$('.cat').removeClass('hide');
		}
	},
	transliter: function (str) {
		const ru = {
			'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd',
			'е': 'e', 'ё': 'e', 'ж': 'j', 'з': 'z', 'и': 'i',
			'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
			'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u',
			'ф': 'f', 'х': 'h', 'ц': 'c', 'ч': 'ch', 'ш': 'sh',
			'щ': 'shch', 'ы': 'y', 'э': 'e', 'ю': 'u', 'я': 'ya',
			' ': '_',  ',': '',  '.': '',  '/': '',  '/ ': '', ',': '_',
			'(': '', ')': '', '"' : '', '{' : '', '}': ''
		}, n_str = [];

		str = str.replace(/[ъь]+/g, '').replace(/й/g, 'i');

		for ( var i = 0; i < str.length; ++i ) {
			n_str.push(
				ru[ str[i] ]
				|| ru[ str[i].toLowerCase() ] == undefined && str[i]
				|| ru[ str[i].toLowerCase() ].replace(/^(.)/, function ( match ) { return match.toUpperCase() })
			);
		}

		return n_str.join('').toLowerCase();
	},
	transliterTitle: function () {
		var $self = $(this),
			$currentVal = $self.val(),
			$target = $self.parent().find('#key');

		// $target.val(_this.transliter($currentVal));
		if($target.val() == '' || $target.val() == undefined)
		{
			let generateCode = Math.floor((Math.random() * 10000000) + 1);
			$target.val('cn_' + generateCode);
		}

	},
	sendPayment: function () {
		var $self = $(this),
			$data = $self.data(),
			$buttonText = $self.text();

		$.ajax({
			method: 'post',
			data: 'action=generate_payment&post_id=' + $data.id + '&target=' + $data.target,
			url: ajaxurl,
			beforeSend: function () {
				$self.text('Обработка...');
			},
			success: function(response){
				$self.text($buttonText);
				$('.js-msg-result').html(response);
			}
		});
	},
	importCsv: function ()
	{
		var $self = $(this), $currentText = $self.text(), $file_url = $('.file-selector-block').find('a').attr('href');
		console.log($file_url);
		console.log($self);
		jQuery.ajax({
			method: 'post',
			data: 'action=import_by_csv&file_url='+$file_url,
			url: ajaxurl,
			beforeSend: function () {
				$self.text('Загрузка товаров...');
			},
			success: function(response){

				$self.text($currentText);
			}
		});
	},
	addLine: function () {
		var $self = $(this), $data = $self.data();
		var $clone = $($data.line).html();
		$($data.target).append($clone);
	},
	deleteTr: function () {
		var $self = $(this), $data = $self.data();
		if(confirm('Удалить?')){
			$self.parent().parent().remove();
		}
	},
	soundTrigger: function () {
		play(soundsUrl.newOrderSound);
		$.toast({
			heading: 'Новый заказ',
			text : "Системе посуптл новый заказ <a href='/wp-admin/edit.php?post_type=shop_order'>Проверить</a>",
			position:'bottom-right',
			hideAfter: 15000,
			bgColor:'green',
			textColor:'#eee',
		});
	},
	checker: function () {
		jQuery.ajax({
			method: 'post',
			data: 'action=checker',
			url: window.ajaxurl,
			beforeSend: function () {
				console.log('checker before')
			},
			success: function(response){
				var result = JSON.parse(response);
				if(result.status == 'orders')
				{
					$.each(result.data, function (ix, val){
						_this.runNotification('Заказ '+ val.order_num, 'Поступил новый заказ - <a href="' + val.url + '">открыть</a>');
					});
					_this.playSound('order');
				}
			}
		});
	},
	getStatistics: function () {
		var $self = $(this), $buttonText = $self.text();

		var postData = $('.form-control input, .form-control select').serialize();

		$.ajax({
			method: 'post',
			data: 'action=get_statistics&' + postData,
			url: ajaxurl,
			beforeSend: function () {
				$self.text('Обработка...');
			},
			success: function(response){
				$self.text($buttonText);
				console.log(response);
			}
		});
	},
	runNotification: function (title, message, postion = 'bottom-right', hideAfter = 20000, bgColor='green', textColor = '#eee') {
		$.toast({
			heading: title,
			text : message, //"В Системе посуптл новый заказ <a href='/wp-admin/edit.php?post_type=shop_order'>Проверить</a>",
			position: postion,
			hideAfter: hideAfter,
			bgColor: bgColor,
			textColor: textColor,
		});
	},
	playSound: function (type = false) {
		if(type)
		{
			switch (type)
			{
				case 'order': var sound = soundsUrl.newOrderSound; break;
				case 'message': var sound = soundsUrl.newMessageSound; break;
				case 'message2': var sound = soundsUrl.newMessageSound2; break;
				default: sound = false;
			}

			if(!sound) return false;
			var audio = new Audio();
			audio.src = sound;
			var resp = audio.play();
			if (resp!== undefined) {
				resp.then(_ => {
				}).catch(error => {
				});
			}
		}
	},
	sendAction: function () {
		var $self = $(this)
			, $data = $self.data()
			, $selfText = $self.text()
			, $canRun = false
			, $postData = {}
			, $post = null
		;

		if($data.confirmation !== undefined) {
			if(confirm($data.confirmation))
			{
				$canRun = true;
			}
		} else {
			if($data.serialize !== undefined) {
				$postData = $($data.serialize).serialize();
				$canRun = true;
				$post = 'action=' + $data.action + '&' + $postData + '&post_type=' + $data.type

			} else {
				if (0 !== $data.length) {
					$postData = new URLSearchParams($data).toString();
					$canRun = true;
					$post = $postData
				}
			}
		}

		//console.log($data);

		if($canRun === true) {
			$.ajax({
				method: 'post',
				data: $post,
				url: ajaxurl,
				beforeSend: function () {
					$self.text('Обработка...');
				},
				success: function(response){
					$self.text($selfText);

					if(response == 'refresh') {
						window.location.href = window.location.href;
					}

					if(undefined !== typeof response.success && true === response.success) {

						if (undefined !== $data.rescontainer) {
							let container = $($data.rescontainer);
							response.data.forEach((el)=>{
								container.append(el);
							})
						}

						if (undefined !== $data.action && 'remove_offer' === $data.action) {
							let $offer = document.querySelector('#offer-' + $data.offer);
							let $offerParent = $offer.parentNode;
							$offerParent.removeChild($offer);
						}
					}

					if(undefined !== typeof response.success && false === response.success) {
						alert(response.data);
					}


				}
			});
		}
	}
}

/**
 *  Run JScntools
 */
jQuery(document).ready(function($) {
	JSCntools = new JSCntools();
	setInterval(JSCntools.checker, 15000);
});




