<?php
$mod_obj = $data['obj'];
?>

<div class="wrap" style="background: transparent; padding: 20px; font-size: 14px; line-height: 25px;">

    <div style="padding: 20px;">
        <h1 class="wp-heading-inline" style="color: #222; font-weight: 700; text-transform: uppercase;"> <?php echo __('CNTOOLS')?></h1>
        <p></p>
        <p class="text-muted">управление дополнительными возможностями сайта</p>
    </div>



    <?php $_GET['action'] = empty($_GET['action']) ? 'main' : $_GET['action'];  ?>


    <ul class="blue-tabs">
        <?php foreach($mod_obj->module_nav as $item) :?>
            <li <?php echo isset($_GET['action']) && $_GET['action'] == $item['slug'] ? 'class="active"' : '';?>>
                <a href="?page=<?= $_REQUEST['page']?>&action=<?= $item['slug'] ?>">
                    <?php echo __($item['title'] )?>
                </a>
            </li>
        <?php endforeach;?>
    </ul>

    <div style="padding: 8px;">

        <?php
            if ($content)
            {
                include_once $content;
            }
            else
            {
                echo  'Content not exists!';
            }
        ?>

        <br>
        <hr>

    </div>
</div>