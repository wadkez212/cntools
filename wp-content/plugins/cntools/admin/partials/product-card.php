<?php
$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-product-specifications');
?>

<h2 style="text-transform: uppercase; text-align: center;">Дополнительные поля для карточки товара</h2>

<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'dynamic' => [
            [
                'type' => 'text',
                'name' => 'title',
                'placeholder' => 'Название характеристики',
            ],

            [
                'type' => 'hidden',
                'name' => 'key',
                'placeholder' => '',
            ],
            'note' => 'Характеристика которая будет отображаться в карточке товара'
        ]
    ], $Form->result);
    ?>
</div>

