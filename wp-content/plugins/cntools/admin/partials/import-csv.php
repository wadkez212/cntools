<?php
    $out = getCntoolsModuleOption('cntools-options-import-rules');

    //dd($out);

//dd(get_product_properties_all_titles());

?>
<h2 style="text-transform: uppercase; text-align: center;">Импорт товаров CSV</h2>

<div class="admin-flex">
    <div class="inner fleft">
        <?php
            foreach ($out as $item )
            {

                file_put_contents(wp_get_upload_dir()['path'] . '/' . $item['key'] . 'csv', '');

                if(!empty($item['fields'])) {
                    create_csv([array_filter($item['fields'])], $item['key']);

                    $attachment = array(
                        'guid'           => wp_upload_dir()['url'] . '/' . basename( $item['key'] ) . '.csv',
                        'post_mime_type' => 'text/csv',
                        'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $item['key'] ) ),
                        'post_content'   => '',
                        'post_status'    => 'inherit'
                    );

                    $attach_id = wp_insert_attachment( $attachment, $item['title'] );
                    echo '<h4><a href="' . wp_upload_dir()['url']  .'/'.  $item['key'] . '.csv' .  '"> ' . $item['title'] . ' </a></h4>';
                }


            }
        ?>

        <div class="file-selector-block" style="background: #f0f0f0; text-align: center; padding: 15px;">
            <a href="" class="file-display-block" target="_blank" style="display: block; margin: 10px;"></a>
            <button class="button button-secondary js-add-file" data-target=".file-display-block">Загрузить файл (*.CSV)</button><br>
            <button class="button button-primary js-import-csv"  style="padding: 20px; margin-top: 10px;" data-action="import_csv">Импортировать</button>
        </div>

    </div>







    <div class="inner fright">
        <strong>История импорта</strong>
        <ul class="">
            <li><a href="#">test-products.csv</a></li>
            <li><a href="#">test-products1.csv</a></li>
            <li><a href="#">test-products2.csv</a></li>
        </ul>
    </div>
</div>