<?php
$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-product-benefits-list');
?>

<h2 style="text-transform: uppercase; text-align: center;">Список преимуществ</h2>

<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'dynamic' => [
            [
                'type' => 'text',
                'name' => 'title',
                'placeholder' => 'Название',
            ],

            [
                'type' => 'hidden',
                'name' => 'key',
            ],
            'note' => 'Преимущество'
        ]
    ], $Form->result);
    ?>
</div>
