<?php
$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-sber');
?>

<h2 style="text-transform: uppercase; text-align: center;">Настройки подклучения к API СБЕРБАНКА</h2>

<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'static' => [
            [
                'type' => 'text',
                'name' => 'login',
                'placeholder' => 'Логин',
            ],
            [
                'type' => 'text',
                'name' => 'password',
                'placeholder' => 'Пароль',
            ],
        ]
    ], $Form->result);
    ?>
</div>


