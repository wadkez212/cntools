<?php
$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-supplier-fields');
?>

<h2 style="text-transform: uppercase; text-align: center;">Дополнительные поля для раздела поставшиков</h2>

<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'dynamic' => [
            [
                'type' => 'text',
                'name' => 'title',
                'placeholder' => 'Название характеристики',
            ],

            [
                'type' => 'hidden',
                'name' => 'key',
            ],
            'note' => 'Поле поставшика'
        ]
    ], $Form->result);
    ?>
</div>
