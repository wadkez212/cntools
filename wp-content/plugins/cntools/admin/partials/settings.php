<?php

if(!empty($_POST['update']))
{

    $result = $_POST['form'];
    update_option($_POST['option_name'], json_encode($_POST['form']), JSON_UNESCAPED_UNICODE);
    header('Location: ?page=' . $_GET['page'] . '&action=settings&ok=1');
}
else
{
    $out = get_option('cntools-common-settings');
    $result = !empty($out) ? json_decode($out, true): false;
}


$pages = get_pages([
    'post_type' => 'page',
    'parent' => 0,
]);

?>
<h2 style="text-transform: uppercase; text-align: center;">Настройки</h2>
<!--<p style="text-align: center; max-width: 50%; display: block; margin: auto">Обшая информация</p>-->

<div class="inner-content">
    <?php if (!empty($_GET['ok'])): ?>
        <div class="alert-ok">Запрос успешно выполнен!</div>
    <?php endif;?>

    <form action="" method="post">
        <input name="option_name" type="hidden"  value="cntools-common-settings">

        <div class="form-control">
            <label>Максимальный срок гарантии в месяцах:</label>
            <input name="form[guarantee]" type="text" id="email" value="<?= !empty($result['guarantee']) ? $result['guarantee'] : '' ?>" placeholder="Максимальный срок гарантии в месяцах">
            <span class="note">Подсказка: Этот срока будет добавлен в админке карточки товара для указания нужного срока гарантии</span>
        </div>


        <div class="form-control">
            <label>Родительская страница для всех городов:</label>
            <select name="form[city_parent]" style="width: 100%; max-width: unset; padding: 3px;">
                <option value="">&mdash; Выбрать &mdash;</option>
                <?php  foreach($pages as $item) : ?>
                    <?php $selected = !empty($result['city_parent']) && $result['city_parent'] == $item->ID? 'selected="selected"' : '' ?>
                    <option value="<?= $item->ID?>" <?= $selected;?>><?= $item->post_title?></option>
                <?php endforeach; ?>
            </select>
            <span class="note">Подсказка: все дочерние страницы будут обрабатываться, как города</span>
        </div>

        <div class="form-control">
            <input type="submit" name="update" id="submit" class="button button-primary" value="Обновить">
        </div>

    </form>
</div>


