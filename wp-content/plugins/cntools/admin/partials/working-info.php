<?php
$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-producer-working-info');
?>

<h2 style="text-transform: uppercase; text-align: center;">Адреса и часы работы по городам</h2>

<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'dynamic' => [

            [
                'type' => 'select',
                'name' => 'city_id',
                'func' => 'cntools_get_pvz_citys',
                'placeholder' => 'Выбрать город',
            ],

            [
                'type' => 'select',
                'name' => 'default',
                'func' => 'cntools_get_yes_no',
                'placeholder' => 'По умолчанию',
            ],


            [
                'type' => 'text',
                'name' => 'slogan',
                'placeholder' => 'Слоган в шапке',
            ],

            [
                'type' => 'text',
                'name' => 'address',
                'placeholder' => 'Адрес',
            ],

            [
                'type' => 'text',
                'name' => 'pcode',
                'placeholder' => 'Индекс',
            ],

            [
                'type' => 'text',
                'name' => 'hours',
                'placeholder' => 'Часы работы (напр.: 09:00-21:00)',
            ],


            [
                'type' => 'text',
                'name' => 'phone',
                'placeholder' => 'Телефон',
            ],


            [
                'type' => 'text',
                'name' => 'phone_additional',
                'placeholder' => 'Телефон доп.',
            ],

            [
                'type' => 'text',
                'name' => 'coords',
                'placeholder' => 'Координаты адреса на карте Яндекс',
            ],

            'note' => 'Город'
        ]
    ], $Form->result);
    ?>
</div>