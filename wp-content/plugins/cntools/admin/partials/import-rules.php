<?php
$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-import-rules');
?>

<h2 style="text-transform: uppercase; text-align: center;">Правила CSV для категории</h2>

<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'dynamic' => [
            [
                'type' => 'text',
                'name' => 'title',
                'placeholder' => 'Имя файла',
            ],
            [
                'type' => 'hidden',
                'name' => 'key',
            ],
            [
                'type' => 'Select',
                'name' => 'category',
                'placeholder' => 'Выбрать категорию',
                'func' => 'get_cntools_categories'
            ],
            [
                'type' => 'Checkboxlist',
                'name' => 'fields',
                'placeholder' => 'Спислок данных',
                'func' => 'cntools_get_titles'
            ],
            'note' => 'Правило для категории'
        ]
    ], $Form->result);
    ?>
</div>


