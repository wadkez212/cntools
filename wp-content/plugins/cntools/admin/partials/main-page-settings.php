<?php
$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-main-page-settings');
?>

<h2 style="text-transform: uppercase; text-align: center;">Управление блоками на главной странице</h2>
<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'dynamic' => [
            [
                'type' => 'text',
                'name' => 'title',
                'placeholder' => 'Название блока (будет отображаться на сайте)',
            ],

            [
                'type' => 'text',
                'name' => 'text',
                'placeholder' => 'Короткий текст',
            ],

            [
                'type' => 'select',
                'name' => 'block',
                'func' => 'cntools_get_blocks_on_main',
                'placeholder' => 'Выбрать блок',
            ],
            'note' => 'Блок на главной'
        ]
    ], $Form->result);
    ?>
</div>

