<?php

if(!empty($_POST['form']['description']))
{
    $_POST['form']['description'] = nl2br($_POST['form']['description']);
}
$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-out-company');
?>

<h2 style="text-transform: uppercase; text-align: center;">Блок - Наша компания</h2>

<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'static' => [
            [
                'type' => 'image',
                'name' => 'logo',
                'placeholder' => 'Лого',
                'width' => '330px',
                'height' => '52px',
            ],
            [
                'type' => 'text',
                'name' => 'text_under_logo',
                'placeholder' => 'Текст под лого',
            ],

            [
                'type' => 'text',
                'name' => 'text_under_text',
                'placeholder' => 'Текст (Пример: 15 Лет вместе)',
            ],

            [
                'type' => 'Texteditor',
                'name' => 'description',
                'placeholder' => 'Описаные',
            ],
        ],
        'dynamic' => [
            [
                'type' => 'image',
                'name' => 'icon',
                'placeholder' => 'Иконка',
                'width' => '70px',
                'height' => '61px',
            ],
            [
                'type' => 'text',
                'name' => 'line',
                'placeholder' => 'Первая линия текста',
            ],

            [
                'type' => 'text',
                'name' => 'line2',
                'placeholder' => 'Вторая линия текста',
            ],
        ]
    ], $Form->result);
    ?>
</div>

