<?php
$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-payment-system-list');
?>

<h2 style="text-transform: uppercase; text-align: center;">Список платежных систем</h2>

<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'dynamic' => [
            [
                'type' => 'image',
                'name' => 'icon',
                'placeholder' => 'Иконка',
                'width' => '120px',
                'height' => '50px',
            ],
            [
                'type' => 'text',
                'name' => 'line',
                'placeholder' => 'Первая линия текста"',
            ],
            'note' => 'Платежная система'
        ]
    ], $Form->result);
    ?>
</div>



