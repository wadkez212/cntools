<?php
$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-product-product-packing-info');

?>

<h2 style="text-transform: uppercase; text-align: center;">Дополнительные поля для Информация об упаковке</h2>

<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'dynamic' => [
            [
                'type' => 'text',
                'name' => 'title',
                'placeholder' => 'Название характеристики',
            ],

            [
                'type' => 'hidden',
                'name' => 'key',
            ],
            'note' => 'Характеристика упаковки'
        ]
    ], $Form->result);
    ?>
</div>




