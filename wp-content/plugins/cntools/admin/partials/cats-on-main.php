<?php
$cntools_option_key = 'cntools-options-cats-on-main';



if(!empty($_POST['update_form']))
{

    $result = $_POST['form'];
    update_option($cntools_option_key, json_encode($_POST['form'], JSON_UNESCAPED_UNICODE));
    header('Location: ?page=' . $_GET['page'] . '&action=' . $_GET['action'] . '&ok=1');
}
else {
    $out = get_option($cntools_option_key);
    $result = !empty($out) ? json_decode($out, true): false;
}
?>


<h2 style="text-transform: uppercase; text-align: center;">Вывод категории на главной</h2>


<div class="inner-content">
    <?php if (!empty($_GET['ok'])): ?>
        <div class="alert-ok">Запрос успешно выполнен!</div>
    <?php endif;?>


    <div class="form-control bblock template hide">
        <div class="d-block-icon-wrapper">
            <div class="icon icon-new_"></div>
            <div class="button button-small button-secondary js-add-icon" data-target=".icon-new_">Иконка</div>
        </div>
        <input type="hidden" class="icon-new_-hidden" name="form[new_][icon]" value="">
        <select name="form[new_][cat_id]">
            <option value="">&mdash; Выбрать категорию &mdash;</option>
            <?= cntool_product_cat_option_list($value['cat_id'])?>
        </select>

        
        <br>
        <span class="note">Подсказка: Категория<a class="remove js-remove" style="float: right" href="#remove" data-id="">удалить</a></span>
    </div>

    <form action="" method="post">

        <input name="deleting" type="hidden" value="">

        <?php if (!empty($result)) : ?>
            <?php foreach ($result as $key => $item): ?>
                <?php $value = $item;?>
                <div class="form-control bblock  <?= $key == 0 ? 'first' : 'cloned' ?>">
                    <div class="d-block-icon-wrapper">
                        <div class="icon icon-<?=$key?>">
                            <?php if(!empty($item['icon'])): ?>
                            <img src="<?php echo $item['icon']?>">
                            <?php endif;?>
                        </div>
                        <div class="button button-small button-secondary js-add-icon" data-target=".icon-<?=$key?>">Иконка</div>
                    </div>
                    <input type="hidden" class="icon-<?=$key?>-hidden" name="form[<?= $key?>][icon]" value="<?= $value['icon']?>">
                    <select name="form[<?= $key?>][cat_id]">
                        <option value="">&mdash; Выбрать категорию &mdash;</option>
                        <?= cntool_product_cat_option_list($value['cat_id'])?>
                    </select>

                    <br>
                    <span class="note">Подсказка: Категория<a class="remove js-remove" style="float: right" href="#remove" data-id="<?= $key?>">удалить</a></span>
                </div>
            <?php endforeach;?>
        <?php else: ?>

            <div class="form-control bblock first">
                <div class="d-block-icon-wrapper">
                    <div class="icon icon-new_"></div>
                    <div class="button button-small button-secondary js-add-icon" data-target=".icon-new_">Иконка</div>
                </div>
                <input type="hidden" class="icon-new_-hidden" name="form[new_][icon]" value="">
                <select name="form[new_][cat_id]">
                    <option value="">&mdash; Выбрать категорию &mdash;</option>
                    <?= cntool_product_cat_option_list($value['cat_id'])?>
                </select>
                
                <br>
                <span class="note">Подсказка: Категория<a class="remove js-remove" style="float: right" href="#remove" data-id="">удалить</a></span>
            </div>

        <?php endif;?>



        <div class="button button-small button-secondary js-add-form"  style="float: right; margin: 0 15px 20px 0">Добавить еще</div>

        <div class="form-control">
            <input type="submit" name="update_form" id="submit" class="button button-primary" value="Обновить">
        </div>
    </form>

</div>

