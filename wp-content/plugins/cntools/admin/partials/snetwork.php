<?php
$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-snetwork');
?>

<h2 style="text-transform: uppercase; text-align: center;">Ссылки на соц.сети</h2>

<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'dynamic' => [
            [
                'type' => 'image',
                'name' => 'icon',
                'placeholder' => 'Иконка',
                'width' => '50px',
                'height' => '50px',
            ],
            [
                'type' => 'text',
                'name' => 'url',
                'placeholder' => 'Ссылка на страницу в социальную сеть',
            ],

            [
                'type' => 'text',
                'name' => 'name',
                'placeholder' => 'Название',
            ],
            'note' => 'Блок соцыалной сети'
        ]
    ], $Form->result);
    ?>
</div>

