<?php
$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-product-equipment');

?>

<h2 style="text-transform: uppercase; text-align: center;">Варианты комплектации </h2>

<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'dynamic' => [
            [
                'type' => 'text',
                'name' => 'title',
                'placeholder' => 'Название характеристики',
            ],

            [
                'type' => 'hidden',
                'name' => 'key',
            ],
            'note' => 'Вариант комплектации'
        ]
    ], $Form->result);
    ?>
</div>


