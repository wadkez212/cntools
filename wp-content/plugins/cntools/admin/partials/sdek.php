<?php
$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-sdek');
?>

<h2 style="text-transform: uppercase; text-align: center;">Настройки подклучения к СДЕК</h2>

<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'static' => [
            [
                'type' => 'text',
                'name' => 'account',
                'placeholder' => 'Account/Идентификатор',
            ],
            [
                'type' => 'text',
                'name' => 'password',
                'placeholder' => 'Secure password/Пароль',
            ],
        ]
    ], $Form->result);
    ?>
</div>


