<?php
$mod_obj = $data['obj'];
?>

<div class="wrap" style="background: transparent; padding: 20px; font-size: 14px; line-height: 25px;">

    <div style="padding: 20px;">
        <h1 class="wp-heading-inline" style="color: #222; font-weight: 700; text-transform: uppercase;"> <?php echo __('CNTOOLS')?></h1>
        <p></p>
        <p class="text-muted">управление дополнительными возможностями сайта</p>
    </div>



    <?php $_GET['action'] = empty($_GET['action']) ? 'main' : $_GET['action'];  ?>


    <div class="flex">
        <div class="flex-inner left">
            <ul class="module-vert-menu">
                <?php foreach($mod_obj->module_nav as $item) :?>
                    <?php if ($item['slug'] == '#'): ?>
                    <li class="parent-section"><?= $item['icon']?> <?php echo __($item['title'] )?></li>
                    <?php else: ?>
                    <li <?php echo isset($_GET['action']) && $_GET['action'] == $item['slug'] ? 'class="active"' : '';?>>
                        <a href="?page=<?= $_REQUEST['page']?>&action=<?= $item['slug'] ?>">
                            <?php echo __($item['title'] )?>
                        </a>
                    </li>
                    <?php endif; ?>
                <?php endforeach;?>
            </ul>

        </div>
        <div class="flex-inner right">
            <?php
            if ($content)
            {
                include_once $content;
            }
            else
            {
                echo  '<div style="margin: 40px auto; text-align: center; padding: 20px; color: red; font-size: 20px;">Контент не подключен!</div>';
            }
            ?>

            <br>
            <br>
            <br>

        </div>
    </div>

</div>