<?php

$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-producer-countries-list');
?>

<h2 style="text-transform: uppercase; text-align: center;">Блок - Наша компания</h2>

<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'dynamic' => [
            [
                'type' => 'image',
                'name' => 'icon',
                'placeholder' => 'Флаг',
                'width' => '48px',
                'height' => '48px',
            ],
            [
                'type' => 'text',
                'name' => 'title',
                'placeholder' => 'Название страны',
            ],

            [
                'type' => 'hidden',
                'name' => 'key',
            ],
            'note' => 'Страна производитель'
        ],
    ], $Form->result);
    ?>
</div>



