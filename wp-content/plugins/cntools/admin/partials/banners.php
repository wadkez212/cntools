<?php


$Form = new Kinaloko\Form();
$Form->Handler('cntools-options-mini-banners');
?>

<h2 style="text-transform: uppercase; text-align: center;">Мини баннеры</h2>

<div class="inner-content">
    <?php
    $Form->RequestResult();
    $Form->Render([
        'dynamic' => [
            [
                'type' => 'image',
                'name' => 'icon',
                'placeholder' => 'Изображение',
                'width' => '350px',
                'height' => '200px',
            ],
            [
                'type' => 'text',
                'name' => 'line',
                'placeholder' => 'Первая линия текста',
            ],
            [
                'type' => 'text',
                'name' => 'line2',
                'placeholder' => 'Вторая линия текста',
            ],
            [
                'type' => 'text',
                'name' => 'line3',
                'placeholder' => 'Третья линия текста',
            ],
            [
                'type' => 'text',
                'name' => 'line4',
                'placeholder' => 'Четветая линия текста',
            ],
            [
                'type' => 'text',
                'name' => 'url',
                'placeholder' => 'Ссылка',
            ],

            [
                'type' => 'select',
                'name' => 'style',
                'func' => 'cntools_banner_styles_list',
                'placeholder' => 'Выбрать стиль баннера',
            ],

            'note' => 'Минни баннер на главной'
        ]
    ], $Form->result);
    ?>
