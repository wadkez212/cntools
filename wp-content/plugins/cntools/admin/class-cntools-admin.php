<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://medvedev.studio
 * @since      1.0.0
 *
 * @package    Cntools
 * @subpackage Cntools/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Cntools
 * @subpackage Cntools/admin
 * @author     Medvedev Studio < info@medvedev.studio>
 */
class Cntools_Admin {

    public $template = 'template-second';

    public $module_nav = [
        [
            'title' => 'Общие настройки',
            'slug' => '#',
            'icon' => '<span class="dashicons dashicons-dashboard"></span>'
        ],

        [
            'title' => 'Главная',
            'slug' => 'main',
        ],

        [
            'title' => 'Настройки',
            'slug' => 'settings',
        ],

        [
            'title' => 'Адреса и часы работы по городам',
            'slug' => 'working-info',
        ],

        [
            'title' => 'Настройки главной страницы',
            'slug' => '#',
            'icon' => '<span class="dashicons dashicons-admin-home"></span>'
        ],

        [
            'title' => 'Управление блоками на главной странице',
            'slug' => 'main-page-settings',
        ],


        [
            'title' => 'Мини баннеры на глявной',
            'slug' => 'banners',
        ],

        [
            'title' => 'Блок - Наша компания на глявной',
            'slug' => 'our-company',
        ],


        [
            'title' => 'Ссылки на социяльные сети',
            'slug' => 'snetwork',
        ],


        [
            'title' => 'Вывод категории на главной',
            'slug' => 'cats-on-main',
        ],

        [
            'title' => 'Статистика',
            'slug' => '#',
            'icon' => '<span class="dashicons dashicons-chart-pie"></span>'
        ],

        [
            'title' => 'Статистика по поставшикам',
            'slug' => 'statistics',
        ],


        [
            'title' => 'Настройка карточки товара',
            'slug' => '#',
            'icon' => '<span class="dashicons dashicons-cart"></span>'
        ],
        [
            'title' => 'Дополнительные поля для карточки товара',
            'slug' => 'product-card',
        ],

        [
            'title' => 'Поля поставшика',
            'slug' => 'supplier-fields',

        ],

        [
            'title' => 'Варианты Комплектации',
            'slug' => 'product-equipment',
        ],

         [
            'title' => 'Список преимуществ',
            'slug' => 'product-benefits-list',
        ],

         [
            'title' => 'Поля для бока Информация об упаковке',
            'slug' => 'product-packing-info',
        ],



        [
            'title' => 'Список стран производителей',
            'slug' => 'producer-countries-list',
        ],


        [
            'title' => 'Импорт/Экспорт',
            'slug' => '#',
            'icon' => '<span class="dashicons dashicons-controls-repeat"></span>'
        ],


        [
            'title' => 'Правила CSV для категории',
            'slug' => 'import-rules',
        ],

        [
            'title' => 'Импорт товаров CSV',
            'slug' => 'import-csv',
        ],


        [
            'title' => 'Оплата, доставка',
            'slug' => '#',
            'icon' => '<span class="dashicons dashicons-money"></span>'
        ],

        [
            'title' => 'Список платежных систем',
            'slug' => 'payment-system-list',
        ],


        [
            'title' => 'Настройки подклучения к СДЕК',
            'slug' => 'sdek',
        ],

        [
            'title' => 'Настройки подклучения к API СБЕРБАНКА',
            'slug' => 'api-sber',
        ],
    ];



	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Cntools_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Cntools_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/cntools-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name. '-toast-js', plugin_dir_url( __FILE__ ) . 'libs/toast/jquery.toast.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name. '-datepicker-js', plugin_dir_url( __FILE__ ) . 'libs/datapicker/datepicker.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name. '-fas', get_template_directory_uri(). '/assets/fontawesome/css/all.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Cntools_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Cntools_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

        wp_enqueue_media();
		wp_enqueue_script( $this->plugin_name . '-cntools-admin', plugin_dir_url( __FILE__ ) . 'js/cntools-admin.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name . '-toast-js', plugin_dir_url( __FILE__ ) . 'libs/toast/jquery.toast.min.js', array(), $this->version, false );
		wp_enqueue_script( $this->plugin_name . '-datapicker-js', plugin_dir_url( __FILE__ ) . 'libs/datapicker/datepicker.js', array(), $this->version, false );
		wp_enqueue_script( $this->plugin_name . '-Sortable-js', plugin_dir_url( __FILE__ ) . 'js/Sortable.js', array('jquery'), $this->version, false );
        wp_localize_script($this->plugin_name, "ajax_obj", array('ajaxurl' => admin_url( 'admin-ajax.php' )));
//        wp_localize_script($this->plugin_name. '-sounds', "soundsUrl", array(
//            'newOrderSound' => get_template_directory_uri() . '/assets/sounds/new_order.mp3',
//            'newMessageSound' => get_template_directory_uri() . '/assets/sounds/new_message.mp3',
//            'newMessageSound2' => get_template_directory_uri() . '/assets/sounds/new_message_2.mp3',
//        ));

	}



    /**
     * Register Overview Page (Top-Level Page)
     */
    public function register_overview_page() {
        add_menu_page(
            __( 'CNTOOLS', $this->plugin_name ),
            __( 'CNTOOLS', $this->plugin_name ),
            'read',
            'cntools',
            array( $this, 'select_page' ),
            'none',
            1
        );
    }


    public function wp_roles_array() {
        $editable_roles = get_editable_roles();
        foreach ($editable_roles as $role => $details) {
            $sub['role'] = esc_attr($role);
            $sub['name'] = translate_user_role($details['name']);
            $roles[] = $sub;
        }
        return $roles;
    }



    public function select_page()
    {
        global $wpdb;
        $this->data['obj'] = $this;
        $partial = isset($_GET['action']) ? $_GET['action'] : 'main';
        $content = CNTOOLS_PATH . 'admin/partials/' . $partial . '.php';
        if (!file_exists($content)) $content = false;

        return $this->include_partial($content, $this->data);
    }



    public function include_partial($content, $data = false) {
        $template = CNTOOLS_PATH . 'admin/partials/' . $this->template . '.php';

        if(file_exists($template))
        {
            include_once($template);
        }
        else
        {
            echo 'Template not exists!';
        }
    }


}
