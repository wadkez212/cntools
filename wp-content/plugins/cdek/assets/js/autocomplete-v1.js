(function ($) {
    $(document).ready(function () {
        $('body').on('input', '#billing_city', null, function () {
            $('#billing_state').val('');
            console.log($('#city-list').length)
            if ($('#city-list').length === 0) {
                $('#billing_city').after('<div id="city-list"></div>');
            }
            console.log(event.target.value)
            $.ajax({
                method: "GET",
                url: "/wp-json/cdek/v1/get-region",
                data: {
                    city: event.target.value
                },
                success: function (response) {
                    let city = JSON.parse(response);
                    console.log(city)
                    $('#city-list').empty();
                    for (let i = 0; i < city.items.length; i++) {
                        $("#city-list").append(`<div class="city-elem" data-code="${city.items[i].ek4code}">${city.items[i].name}</div>`);
                    }
                },
                error: function (error) {
                    console.log({error: error});
                }
            });
        })

        $('body').on('click', '.city-elem', null, function (event) {
            let cityData = event.currentTarget.innerText.split(',');
            $('#billing_city').val(cityData[0])
            $('#billing_state').val(cityData[1]);
            $('#billing_city').change();
            $('#billing_state').change();
            $('#city-list').empty();
            console.log(cityData[0])
            console.log(event.currentTarget.dataset.code)
        })
    })
})(jQuery);