<?php

namespace Cdek;

interface Data
{
    public function getFields();
}