<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Портал_Cntools
 */

?>

<?php $user = wp_get_current_user(); ?>
<?php if(!empty($user->roles[0]) && $user->roles[0] == 'supplier'): ?>
    <?php get_template_part( 'template-parts/footer-supplier'); ?>
<?php else: ?>
    <?php get_template_part( 'template-parts/footer-default'); ?>
<?php endif; ?>

</body>
</html>
