
<section class="product-reviews product-ask-questions">
    <h2 class="title">Вопросы и ответы: Фреза G7-2FS-1200-L (12х12х45х100 мм) АКСИС 00001371172 — Тест товар 3</h2>
    <div class="flex content-wrapper">
        <div class="flex-inner left">
                <span class="text">Есть вопросы о товаре?</span>
            <button type="button" class="button js-add-question" data-action="add_question">Задать вопрос</button>

        </div>
        <div class="flex-inner right">
            <h4>Иван И.</h4>
            <div class="date">08.07.2002</div>
            <span class="text">
                Фрезерная машина оправдала мои ожидания. Я занимаюсь созданием разных предметов интерьера и аксессуаров из дерева.
                Обработка материала получается точной и аккуратной. Скорость и глубина регулируются. Пользоваться удобно.
                Инструмент устойчивый. Вибрация минимальна. Удобные ручки. В целом все качественно сделано в этой фрезерной машине.
            </span>

        </div>
    </div>
</section>