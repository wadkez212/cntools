<?php
/**
 * Description tab
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.0.0
 */

defined( 'ABSPATH' ) || exit;

global $post;

?>



<div class="flex">
    <div class="flex-inner properties-wrapper">
        <div class="description">
            <?php the_content(); ?>
        </div>
        <div class="properties">
            <?php cntool_tech_properties_list(get_the_ID(), 'Технические характеристики')?>
        </div>

        <div class="yellow-dot-list">
            <?php cntool_product_options_list('cntools-options-product-benefits-list', get_the_ID(), 'Преимущества')?>
        </div>

    </div>
    <div class="flex-inner properties-wrapper-right">

        <div class="yellow-dot-list">
            <?php cntool_product_producer_country('cntools-options-producer-countries-list', get_the_ID(), 'Производитель')?>
        </div>


        <div class="yellow-dot-list">
            <?php cntool_product_options_list('cntools-options-product-equipment', get_the_ID(), 'Комплектация')?>
        </div>

        <div class="yellow-dot-list">
            <?php cntool_product_options_list('cntools-options-product-product-packing-info', get_the_ID(), 'Информация об упаковке', true)?>
        </div>

    </div>
</div>
