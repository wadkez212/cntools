<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 *
 * @see woocommerce_default_product_tabs()
 */
$product_tabs = [
    'descriptions' => array ( 'title' => 'Описание и характеристики', 'priority' => 10, 'callback' => 'cntools_get_descriptions', ),
    'reviews' => array ( 'title' => 'Отзывы', 'priority' => 10, 'callback' => 'cntools_get_reviews', ),
    'get_methods' => array ( 'title' => 'Способы получения', 'priority' => 11, 'callback' => 'cntools_get_method_tab', ),
    'ask_question' => array ( 'title' => 'Задать вопрос', 'priority' => 12, 'callback' => 'cntools_ask_question_tab', ),
    'related_products' => array ( 'title' => 'Похожые продукты', 'priority' => 13, 'callback' => 'cntools_related_products_tab', ),
    'video_reviews' => array ( 'title' => 'Видеообзор', 'priority' => 14, 'callback' => 'cntools_video_reviews_tab', ),
];

//dd($product_tabs);

if ( ! empty( $product_tabs ) ) : ?>

		<ul class="product-tabs-control">
			<?php $i = 0; foreach ( $product_tabs as $key => $product_tab ) : ?>
				<li class="<?php echo esc_attr( $key ); ?>_tab <?= $i == 0 ? 'active' : '' ?>" id="tab-title-<?php echo esc_attr( $key ); ?>" role="tab" aria-controls="tab-<?php echo esc_attr( $key ); ?>">
					<a href="#tab-<?php echo esc_attr( $key ); ?>">
						<?php echo wp_kses_post( apply_filters( 'woocommerce_product_' . $key . '_tab_title', $product_tab['title'], $key ) ); ?>
					</a>
				</li>
			<?php $i ++;  endforeach; ?>
		</ul>

		<?php foreach ( $product_tabs as $key => $product_tab ) : ?>
			<div class="cntoots-tab-panel " id="tab-<?php echo esc_attr( $key ); ?>">
				<?php
				if ( isset( $product_tab['callback'] ) ) {
					call_user_func( $product_tab['callback'], $key, $product_tab );
				}
				?>
			</div>
		<?php endforeach; ?>

		<?php do_action( 'woocommerce_product_after_tabs' ); ?>


<?php endif; ?>
