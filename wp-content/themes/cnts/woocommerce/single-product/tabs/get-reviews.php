<?php
?>

<section class="product-reviews">
    <h2 class="title">Отзвыви: Фреза G7-2FS-1200-L (12х12х45х100 мм) АКСИС 00001371172 — Тест товар 3</h2>
    <div class="flex content-wrapper">
        <div class="flex-inner left">
            <h4>1 из 5</h4>
            <div class="stars">
                <span class="stars-wrapper">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </span>
                <span class="text">1 отзыва(а)</span>
            </div>
            <button type="button" class="button js-add-review" data-action="add_review">Написать отзыв</button>

        </div>
        <div class="flex-inner right">
            <div class="stars">
                <span class="stars-wrapper">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </span>
            </div>

            <h4>Иванов И.</h4>
            <div class="date">08.07.2002</div>
            <span class="text">
                Фрезерная машина оправдала мои ожидания. Я занимаюсь созданием разных предметов интерьера и аксессуаров из дерева.
                Обработка материала получается точной и аккуратной. Скорость и глубина регулируются. Пользоваться удобно.
                Инструмент устойчивый. Вибрация минимальна. Удобные ручки. В целом все качественно сделано в этой фрезерной машине.
            </span>

        </div>
    </div>
</section>
