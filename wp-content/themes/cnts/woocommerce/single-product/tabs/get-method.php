<section class="product-get-methods">
    <h2 class="title">Способы получения заказа: </h2>
    <div class="flex content-wrapper">
        <div class="flex-inner method-item">
            <div class="get-method-block">
                <span class="icon">
                    <span class="fa fa-user-check"></span>
                </span>
                <h3 class="title">Самовывоз</h3>
                <p class="description">Сегодня, из 1 магазина</p>
            </div>
        </div>

        <div class="flex-inner method-item">
            <div class="get-method-block">
                <span class="icon">
                    <span class="fa fa-shipping-fast"></span>
                </span>
                <h3 class="title">Доставка курьером</h3>
                <p class="description">Затра от 190 р</p>
            </div>
        </div>


        <div class="flex-inner method-item">
            <div class="get-method-block">
                <span class="icon">
                    <span class="fa fa-plane"></span>
                </span>
                <h3 class="title">Транспортной кампанией</h3>
                <p class="description">Сегодня, из 1 магазина</p>
            </div>
        </div>


    </div>
</section>