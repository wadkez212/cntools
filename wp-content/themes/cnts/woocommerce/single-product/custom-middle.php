<?php
global $product;
global $wpdb;
$post_id = get_the_ID();
$table_name = $wpdb->prefix . "offers";

$min_price = $wpdb->get_results( "SELECT MIN(price) as price FROM $table_name WHERE product_id = $post_id LIMIT 1" );

?>
<section class="container product-single">
    <?php if (class_exists('WooCommerce') && is_woocommerce()) : ?>
        <?php woocommerce_breadcrumb(); ?>
    <?php endif; ?>
    <h1 class="product-title"><?php woocommerce_template_single_title();?></h1>
    <div class="row">
        <div class="col-12">
            <?php woocommerce_template_single_meta();?>
        </div>
        <div class="col-md-7">
            <?php woocommerce_show_product_images(); ?>
        </div>
        <div class="col-md-5">
            <div class="product-info card" style="border: none;">
                <div class="card-body">
                    <div class="row">
                        <div class="col" style="text-align: center;">
                            <span class="text text-primary item-compare js-add-to" data-target="compare" data-id="37702"><i class="fa fa-balance-scale"></i> На сравнение</span>
                            <span class="text text-danger item-favorite js-add-to" data-target="favorite" data-id="37702"><i class="fa fa-heart"></i> В избранное</span>
                        </div>
                    </div>
                    <?php if(empty($min_price[0]->price)):?>
                        <a href="#" class="connect-to-manager">
                            <span><i class="fa fa-envelope"></i></span>
                            <span>Сообщить о поступлении</span>
                        </a>
                    <br>
                    <?php else: ?>
<!--                    <div class="price-single">от --><?//= $min_price[0]->price?><!-- <span class="woocommerce-Price-currencySymbol">₽</span></div>-->

                    <br>
                    <a href="#myTab" class="btn btn-primary js-active-offers-tab"   style="width: 100%; padding: 22px;">К предложениям</a>
                    <br>
                    <br>
                    <?php endif;?>
                    <div class="connect-to-manager js-callback-order" data-title="Связаться с менеджером" data-bs-toggle="modal" data-bs-target="#callback_button">
                        <span><i class="fa fa-headset"></i></span>
                        <span>Связаться с менеджером</span>
                    </div>




                        <?php cntool_product_guarantee()?>
                        <?php cntool_tech_properties_list(get_the_ID(), false, 6, 'attrs-intro-list')?>
                </div>
            </div>
        </div>


        <div class="col-12 product-tabs">
            <?php require 'custom-tabs.php'?>
        </div>
    </div>

</section>