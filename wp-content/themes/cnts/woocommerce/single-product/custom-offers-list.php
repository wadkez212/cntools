<?php $cdek = new CdekConnection(); ?>
<h2 class="title"><span>Все предложения по товару:</span> <?= the_title()?></h2>

        <?php
        $suppliers = get_posts([
            'post_type' => 'suppliers'
        ]);


        $offers = $wpdb->get_results( "SELECT * FROM $table_name WHERE product_id = $post_id ORDER BY price ASC" );

        $suppliers_arr = [];
        foreach ($suppliers as $supplier) {
            $suppliers_arr[$supplier->ID] = $supplier;
        }
        ?>
<?php
    $product_in_cart = false;
    if(in_array(get_the_ID(), array_column(WC()->cart->cart_contents, 'product_id'))) {
        $product_in_cart = true;
    }
?>

<?php
$receiver_pcode = get_geo()['pcode'];
if(empty($receiver_pcode)) {
    $pvzs = getPvzs();
    $pvz_id = !empty($pvzs[0]->ID) ? $pvzs[0]->ID : '';
    $receiver_pcode = get_post_meta($pvz_id, 'PostalCode', 1);
}

?>

<?php foreach ($offers as $offer): ?>
   <?php
     $offer_pcode = get_post_meta($offer->supplier_id, 'cn_5405871', 1);

    $delivery_data = $cdek->getTarif($offer_pcode, $receiver_pcode, [
            'weight' => $offer->weight,
            'length' => $offer->length,
            'width'  => $offer->width,
            'height' => $offer->height,
            'supplier_id' => $offer->supplier_id,
            'product_id' => get_the_ID()
    ]);


    $perma = explode('/', trim(get_permalink($offer->supplier_id), '/'));
    $prefix = substr($perma[count($perma)-1], 0, 3);


    ?>
    <div class="card card-shadow offers-list-card" style="margin-bottom: 20px;">
        <div class="card-body">
            <div class="row">

                <div class="col-md-9 properties">
                    <ul class="options">
                        <li>
                            <span class="title">Поставщик</span>
                            <span class="value" style="text-transform: uppercase;"><?= $prefix . str_pad($offer->supplier_id, 8, '0', STR_PAD_LEFT);?></span>
                        </li>
                        <li>
                            <span class="title">Бренд</span>
                            <span class="value"><?= $offer->brand?></span>
                        </li>
                        <li>
                            <span class="title">Наличие</span>
                            <span class="value"><?= $offer->quantity?>  Штук</span>
                        </li>
                        <li>
                            <span class="title">Доставка в вашем городе</span>
                            <?php if(!empty($delivery_data['price'])): ?>
                                <span class="value"><?= $delivery_data['price']?> ₽ ( <?= $delivery_data['min']?> - <?= $delivery_data['max']?> д. )</span>
                            <?php  else: ?>
                                <span class="value text-danger">Не определено</span>
                            <?php endif; ?>
                        </li>

                        <li>
                            <span class="title">Стоимость</span>
                            <span class="value"><?= $offer->price?>  ₽</span>
                        </li>

                    </ul>
                </div>
                <div class="col-md-3 addtocart">
                    <?php
                    $is_in_cart = false;
//                    dd(WC()->cart->generate_cart_id( $offer->product_id, 0, [], ['offer_price' => $offer->price, 'supplier_id' =>  $offer->supplier_id]));
                    $cart_item_key = WC()->cart->generate_cart_id( $offer->product_id, 0, [], ['offer_price' => $offer->price, 'supplier_id' =>  $offer->supplier_id]);


                    if( WC()->cart->find_product_in_cart( $cart_item_key ) ) {
                        $is_in_cart = true;
                    }
                    ?>

                        <div class="buy-block" style="margin-top: 0px; <?= $is_in_cart || $product_in_cart ? 'display: none;' : '' ?>" >
                            <div class="row">
                                <div class="col-12">
                                    <div class="offer-price"><span data-price="<?= $offer->price?>"><?= $offer->price?></span> ₽</div>
                                </div>
                                <div class="col-4">
                                    <div class="minus js-change-quantity" data-operator="minus" data-quantity="<?= $offer->quantity?>">-</div>
                                </div>
                                <div class="col-4">
                                    <input type="text" class="form-control form-control-sm js-product-quantity product-quantity" data-qty name="quantity" data-quantity="<?= $offer->quantity?>" style="text-align: center" aria-label=""  value="1">
                                </div>
                                <div class="col-4">
                                    <div class="plus js-change-quantity" data-operator="plus" data-quantity="<?= $offer->quantity?>">+</div>
                                </div>
                                <div class="col-12">
                                    <div class="btn btn-primary btn-sm add-to-cart js-add-to-cart"
                                       data-product="<?= $offer->product_id?>"
                                       data-price="<?= $offer->price?>"
                                       data-supplier="<?= $offer->supplier_id?>"><i class="fa fa-shopping-cart"></i> Добавить в корзину</div>
                                </div>
                            </div>
                        </div>

                        <?php if($is_in_cart): ?>
                            <div class="row">
                                <div class="col-12"></div>
                                <div class="col-12">
                                    <a href="/cart/" class="btn btn-primary go-to-cart btn-sm"><i class="fa fa-shopping-cart"></i> Перейти в корзину</a>
                                </div>
                                <div class="col-12">
                                    <a href="#remove" data-id="<?= $cart_item_key?>" class="text text-sm text-danger remove-from-cart js-remove-form-cart">Удалить из корзины</a>
                                </div>
                            </div>
                        <?php endif; ?>


                    <div class="text text-warning" style="text-align: center; display: <?= $product_in_cart && !$is_in_cart ? 'block' : 'none'?>;">
                        <div class="fake-element"></div>
                        <div class="fake-element"></div>
                        <div class="fake-element"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endforeach;?>

use Public\CdekConnection;

