
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
        <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">Предложения</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="description-tab" data-bs-toggle="tab" data-bs-target="#description-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false">Описание</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false">Характеристики</button>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact-tab-pane" type="button" role="tab" aria-controls="contact-tab-pane" aria-selected="false">Отзывы</button>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
        <?php require 'custom-offers-list.php'?>
    </div>
    <div class="tab-pane fade properties" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
        <br>
        <div class="row">
            <div class="col-8">
                <?php cntool_tech_properties_list(get_the_ID(), 'Технические характеристики')?>
            </div>
            <div class="col-4">

            </div>
        </div>
        <br>
        <br>
        <br>
    </div>
    <div class="tab-pane fade" id="description-tab-pane" role="tabpanel" aria-labelledby="contact-tab" tabindex="0">
        <div class="row">
            <div class="col-xxl-12">
                <div class="description" style="margin-top: 20px;">
                    <h3>Описание товара:</h3>
                <?php the_content(); ?>
                </div>
            </div>
        </div>

        <br>
        <br>
        <br>
    </div>
    <div class="tab-pane fade" id="disabled-tab-pane" role="tabpanel" aria-labelledby="disabled-tab" tabindex="0">...</div>
</div>

