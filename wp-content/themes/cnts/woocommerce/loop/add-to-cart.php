<?php
/**
 * Loop Add to Cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/add-to-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
global $wpdb;
$price = wc_price( wc_get_price_to_display( $product ) );
$table_name = $wpdb->prefix . "offers";
$post_id = get_the_ID();
$offer_info = $wpdb->get_results( "SELECT MIN(price) as price, count('ID') as quan FROM $table_name WHERE product_id = $post_id LIMIT 1" );

?>
    <div class="product-row">
        <div class="row">
            <div class="col" style="text-align: center;">
                <span class="text text-primary item-compare js-add-to" data-target="compare" data-id="<?= $post_id?>"><i class="fa fa-balance-scale"></i></span>
                <span class="text text-danger item-favorite js-add-to" data-target="favorite" data-id="<?= $post_id?>"><i class="fa fa-heart"></i></span>
            </div>
        </div>
        <?php if( !empty($offer_info[0]->price) ): ?>
        <div class="col-in price" style="text-align: center; font-weight: bold; margin-top: 15px; font-size: 18px">
            от <?= $offer_info[0]->price?> <span class="woocommerce-Price-currencySymbol">₽</span>
        </div>
        <div class="row">
            <div class="col" style="text-align: center; margin: auto;">
                <div class="badge text-bg-light js-view-offers-list"
                   data-id="<?= $post_id?>"
                   data-action="view_offers_list"
                   data-title="Предложения по товару: <?= get_the_title($post_id)?>"
                   style="font-weight: normal; padding: 8px;">Предложении(<?= $offer_info[0]->quan?>)</div>
            </div>
        </div>
        <?php  else: ?>
            <div class="row">
                <div class="col" style="text-align: center; margin: auto;">
                    <a href="#" class="badge text-bg-light js-subscribe-on-notification"
                       data-id="<?= $post_id?>"
                       data-action="subscribe_on_notification"
                       data-serialize=".js-notification-wrapper input"
                       data-title="Товар: <?= get_the_title($post_id)?>"
                       style="font-weight: normal; padding: 8px; margin-top: 30px;"><i class="fa fa-envelope text-muted"></i> Сообщить о поступлении</a>
                </div>
            </div>
        <?php  endif; ?>

        <div class="col-in add-form">
<!--                <a href="#" class="btn btn-yellow">Подробнее</a>-->
                <?php
//                    echo apply_filters(
//                        'woocommerce_loop_add_to_cart_link', // WPCS: XSS ok.
//                        sprintf(
//                            '<a href="%s" data-quantity="%s" class="add-cart-button %s" %s><span class="fa fa-shopping-cart"></span> %s</a>',
//                            esc_url( $product->add_to_cart_url() ),
//                            esc_attr( isset( $args['quantity'] ) ? $args['quantity'] : 1 ),
//                            esc_attr( isset( $args['class'] ) ? $args['class'] : 'button' ),
//                            isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '',
//                            esc_html( $product->add_to_cart_text() )
//                        ),
//                        $product,
//                        $args
//                    );
                ?>
        </div>
    </div>
</div>
