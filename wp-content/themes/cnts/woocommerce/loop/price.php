<?php
/**
 * Loop Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
global $wpdb;


$price = wc_price( wc_get_price_to_display( $product ) );
$regular_price = wc_get_price_to_display( $product, array( 'price' => $product->get_regular_price() ) );

$post_id = get_the_ID();
$table_name = $wpdb->prefix . "offers";

$min_price = $wpdb->get_results( "SELECT MIN(price) as price FROM $table_name WHERE product_id = {$product->ID} LIMIT 1" );

?>
<div class="product-info">
<?php if ( $price_html = $product->get_price_html() ) : ?>

            <?php if(!empty($product->get_sale_price()) && $product->get_sale_price() < $product->get_regular_price()): ?>
            <?php

                $sale_prc = false;
                {
                    $sale_prc = ($product->get_regular_price()-$product->get_sale_price())/($product->get_regular_price()/100);
                }

                ?>
                <span class="sale-price" style="text-align: center"> от <? $min_price?> ₽</span>
            <?php else: ?>
                <span class="sale-price"></span>
            <?php endif; ?>
<?php endif; ?>

