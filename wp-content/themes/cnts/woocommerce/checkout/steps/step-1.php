<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
setcookie("cntools_checkout_steps_data", "", time() - 3600,"/");
//setcookie("TestCookie", "", time() - 3600, "/~rasmus/", "example.com", 1); cookie-domain:cntools.ru cookie-name:cntools_checkout_steps_data 
?>

<div class="row">
	<div class="col-12">
		<h4 class="title-h4 mb-3">Выберите тип покупателя</h4>
		<span class="step-subtitle">и заполните поля с информацией</span>
	</div>   
</div>

<div class="note-block">
    <span class="icon-info"><span>!</span></span>
    Уже покупали на сайте. Войдите в <strong>личный кабинет</strong> или введите E-mail.
</div>

<div class="row">

	<!-- FL button block	 -->
    <div class="col-lg-6 js-billing-user-type step-tab step-bg1 mt-3 mb-lg-3">
    <?php
    foreach ($fields as $key => $field) {
        if(in_array($key, ['billing_wooccm12', ]))
            cntools_wc_form_field($key, $field, $checkout->get_value($key), 'block', false, true);
    }
    ?>
    </div>
	<!-- JuL button block	 -->
    <div class="col-lg-6 js-billing-user-type step-tab step-bg2 mt-lg-3 mb-3">
        <?php
        foreach ($fields as $key => $field) {
            if(in_array($key, ['billing_wooccm13']))
                cntools_wc_form_field($key, $field, $checkout->get_value($key), 'block', false);
        }
        ?>
    </div>
	
	<br>
	
	<!-- FL content block	 -->
	<div class="col-md-6">
		<!-- *** woocommerce/includes/wc-funtions-order-fields.php *** -->
		<div class="row step-fields-block">
			<div class="col-12">
				<?php // Имя
				foreach ($fields as $key => $field) {
					if(in_array($key, ['billing_first_name']))
						cntools_wc_form_field($key, $field, $checkout->get_value($key),  'block', true);
				}
				?>
			</div>
			<div class="col-12">
				<?php // Фамилия
				foreach ($fields as $key => $field) {
					if(in_array($key, [ 'billing_last_name']))
						cntools_wc_form_field($key, $field, $checkout->get_value($key),  'block', true);
				}
				?>
			</div>
			<div class="col-12">
				<?php // E-mail
				foreach ($fields as $key => $field) {
					if(in_array($key, ['billing_email',]))
						cntools_wc_form_field($key, $field, $checkout->get_value($key),  'block', true);
				}
				?>
			</div>
			<div class="col-12">
				<?php // Телефон
				foreach ($fields as $key => $field) {
					if(in_array($key, ['billing_phone',]))
						cntools_wc_form_field($key, $field, $checkout->get_value($key),  'block', true);
				}
				?>
			</div>
		</div>		
	</div>
	
	<!-- JuL content block	 -->
    <div class="col-md-6 js-iur-fields">
		<div class="row step-fields-block">
			<div class="col-12">
				
				<?php // Тип организации
				foreach ($fields as $key => $field) {
					if(in_array($key, [ 'billing_wooccm24']))
						cntools_wc_form_field($key, $field, $checkout->get_value($key), 'none');
				}
				?>
			</div>
			
			<div class="col-12">
				<?php // Название компании
					foreach ($fields as $key => $field) {
						if(in_array($key, [ 'billing_company'])) {
							cntools_wc_form_field($key, $field, $checkout->get_value($key), 'none'); }
					}
				?>
			</div>
			<div class="col-12">
				<?php //ИНН
					foreach ($fields as $key => $field) {
						if(in_array($key, [ 'billing_wooccm15']))
							cntools_wc_form_field($key, $field, $checkout->get_value($key), 'none');
					}
				?>
			</div>
			<div class="col-12">
				<?php //КПП
					foreach ($fields as $key => $field) {
						if(in_array($key, [ 'billing_wooccm16']))
							cntools_wc_form_field($key, $field, $checkout->get_value($key), 'none');
					}
				?>
			</div>
			<div class="col-12">
				<?php // БИК
					foreach ($fields as $key => $field) {
						if(in_array($key, [ 'billing_wooccm23']))
							cntools_wc_form_field($key, $field, $checkout->get_value($key), 'none');
					}
				?>
			</div>
			<div class="col-12">
				<?php // Расч счет
					foreach ($fields as $key => $field) {
						if(in_array($key, [ 'billing_wooccm21']))
							cntools_wc_form_field($key, $field, $checkout->get_value($key), 'none');
					}
				?>
			</div>
			<div class="col-12">
				<?php // Юр Адрес
					foreach ($fields as $key => $field) {
						if(in_array($key, [ 'billing_wooccm19']))
							cntools_wc_form_field($key, $field, $checkout->get_value($key), 'none');
					}
				?>
			</div>		
		</div>
    </div>
</div>