<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>

<?php
global $wpdb;
//$order = wc_get_order();
//dd($checkout->get_value('billing_last_name'));
// 'billing_last_name', 'billing_first_name', 'billing_wooccm11'
$zones = WC_Shipping_Zones::get_zones();
$methods = array_column( $zones, 'shipping_methods' );
$shipping_methods = $methods[0];

?>

<form name="checkout" method="post" class="checkout cntools-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
    <input type="hidden" class="billing_country" name="billing_country" value="RU">
    <input type="hidden" class="billing_city" name="billing_city" value="<?= $geo['name']?>">
	
	<!-- Вывод скрытых полей -->
	<?php  if(!empty($steps_inputs_arr[0])): ?>
	<?php foreach ($steps_inputs_arr as $filed => $value): ?>
	<input type="hidden" name="<?= is_array($value) ? $filed . '[]' : $filed ?>" value="<?= is_array($value) ? $value[0] : $value ?>">
	<?php endforeach; ?>
	<?php endif; ?>
	
	<!-- Вывод товара - начало Блока -->
	<div class="row verify-order">
		<div class="col-xxl-12">
		<!-- <h5>Состав заказа</h5> -->			
        <?php
		$card_title = '';
        foreach( WC()->cart->get_cart() as $cart_item_key => $cart_item ):
			$table_name = $wpdb->prefix . "offers";
			$offer = $wpdb->get_results( "SELECT * FROM $table_name WHERE supplier_id = {$cart_item['supplier_id']} AND product_id = {$cart_item['product_id']}", 'ARRAY_A');
			$offer = !empty($offer[0]) ? $offer[0] : false;
			$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
			if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
				$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
				$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
			?>
						
			<!-- Вывод товара - Карточка -->
			<div class="card offers-list-card mb-4 border-0">
				
				<!-- card title -->
<!-- 				<div class="row mb-3"> -->
					<?php 
		$curr_title = str_pad($cart_item['supplier_id'], 8, '0', STR_PAD_LEFT);
		if($card_title == '' || $card_title != $curr_title){
			echo '<div class="row mb-3">';
			echo '<div class="col-sm-12 col-md-5 mb-3 mb-md-0 text-md-start align-self-center card-title-box">';
			echo '<h2 class="title-h2 mb-0">Поставщик '.$curr_title.'</h2>';
			echo '</div>';
			echo '<div class="col-sm-12 col-md-7 text-md-end card-count-box">';
				echo '<span class="cart-card-text me-1">Доставка</span>';
				echo '<span class="cart-card-label">21.09.2022</span>';
				echo '<span class="cart-card-label"><i>350</i>₽</span>';
			echo '</div>';
		echo '</div>';
		}
		$card_title = $curr_title;
	?>		
				<div class="card-body card-shadow">
					<div class="row">
						<!-- Card Image -->
						<div class="col-xl-2 product-thumbnail">
							<div class="thumbnail-wrapper">
								<?php
				$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

				if ( ! $product_permalink ) {
					echo $thumbnail; // PHPCS: XSS ok.
				} else {
					printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
				}
								?>
							</div>
						</div>

						<!-- Card content -->
						<div class="col-xl-8 properties">
							<ul class="options">
								<!-- card title -->
								<li class="noafter nobefore card-caption">
									<span class="title">
										<?php
				if ( ! $product_permalink ) {
					echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
				} else {
					echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
				}

				do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

				// Meta data.
				echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

				// Backorder notification.
				if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
					echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
				}
										?>
									</span>
								</li>
								<!-- card items -->
								<li style="width: 100%;">
									<span class="title">Бренд</span>
									<span class="value"><?= str_pad($cart_item['supplier_id'], 8, '0', STR_PAD_LEFT);?></span>
								</li>						
								<li style="width: 100%;">
									<span class="title">Цена за штуку</span>
									<span class="value"> <?= apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.?></span>
								</li>
							</ul>
						</div>
						<div class="col-xl-2 d-flex flex-column align-items-center justify-content-center">
							<span class="d-block mb-md-2 ">
								<span class="step-card-total"><?php echo WC()->cart->get_product_price($_product); ?></span>
							</span>
							<span class="title">Количество</span>
							<span class="value mt-2">1</span>
						</div>
						
					</div>
				</div>
			</div>

            <?php  } ?>
        <?php  endforeach; ?>
		</div>
	</div>
</form>