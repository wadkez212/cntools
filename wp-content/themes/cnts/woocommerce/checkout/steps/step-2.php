<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$methods_handlers_arr = [
        'local_pickup:2' => 'self-service.php',
        'flat_rate:3' => 'delivery.php',
        'flat_rate:4' => 'by-transport-company.php',
];
$get_method_key = empty($_GET['method']) ? 'local_pickup:2' : $_GET['method'];
$get_method_type = !empty($_GET['method']) && isset($methods_handlers_arr[$_GET['method']]) ? $methods_handlers_arr[$_GET['method']] : $methods_handlers_arr['local_pickup:2'];
?>
<div class="row">
	
	<div class="row">
		<div class="col-12 mb-4">
			
			<h4 class="title-h4 mb-3">Выберите ПВЗ</h4>
			<span class="step-subtitle">Через поиск по адресу или на карте</span>
		</div>   
	</div>
	
    <!-- Получение -->
    <div class="city-selector">
        <div class="step-city-label mb-2 mb-sm-0">
			<i class="fas fa-map-marker-alt"></i>
			<?= $geo['name']?>
		</div>
        <span class="js-change-geo" data-action="get_city_list" data-title="Список городов" data-bs-toggle="modal" data-bs-target="#callback_button" data-city="<?= $geo['city_id']?>">У вас другой город</span>
    </div>

    <?php

    $zones = WC_Shipping_Zones::get_zones();
    $methods = array_column( $zones, 'shipping_methods' );
    $shipping_methods = $methods[0];
    ?>

    <input type="hidden" name="shipping_method[0]" value="<?= $get_method_key?>">


    <ul class="select-get-method mb-2">
        <?php foreach ($shipping_methods as $key => $method): ?>
        <?php if($method->enabled == 'yes') : ?>
        <li data-action="<?= $method->id?>_<?= $key?>"
            data-value="<?= $method->id?>:<?= $key?>"
            data-cost="<?= $method->cost?>"
            data-address="<?= $method->title?>"
             class="<?= $get_method_key == $method->id . $key ? 'active' : ''?>">
            <a href="/checkout/?step=2&method=<?= $method->id?><?= $key?>"><?= $method->title?></a>
        </li>
        <?php endif; ?>
        <?php endforeach?>
    </ul>

    <?php require_once 'get-methods/' . $get_method_type ?>

<!-- 
    <div class="col-md-12">
        <br>
        <div data-href="/checkout/?step=1" class="js-steps-nav button-cancel btn btn-white bradius30">Вернуться</div>
        <div data-href="/checkout/?step=3" class="js-steps-nav button btn btn-primary">Продолжить</div>
    </div> -->
</div>



