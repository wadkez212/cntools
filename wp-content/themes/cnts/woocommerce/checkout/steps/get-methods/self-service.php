<?php
$page_meta = get_post_meta($geo['city_id'], 'city');

$add_map   = '';
$addresses = '';

//$region_code = $cdek->getRegionCodeByName(get_the_title($geo['city_id']));
//$city_code = $cdek->getCityCode(get_the_title($geo['city_id']), $region_code);
$city_code = $geo['city_code'];

foreach ($cdek->getPvz($city_code) as $point) {
    $geo['coords'] = $point->coordY .',' . $point->coordX;
    $addresses .= '<option value="' . $point->Code . '" data-coords="' . $point->coordY .',' . $point->coordX . '" data-pcode="' . $point->PostalCode . '">' . $point->Address . '</option>';

    $add_map .= '
    .add(new ymaps.Placemark([' . $point->coordY .',' . $point->coordX . '], {			
				balloonContentHeader: \'<span class="js-point-select map-header" id="point-' .$point->Code .'">' . $point->Address . '</span><br> \',
                balloonContentBody: \'<div class="map-content-block"><span class="map-label">График работы:</span><span class="map-value">8:00-18:00</span> </div><div class="map-content-block"><span class="map-label">Дата доставки:</span><span class="map-value">От 2 дней</span></div>\',
				balloonContentFooter: \'<button class="map-btn" onclick="setPickupPoint()">Забрать отсюда</button>\',
                iconCaption: \'\'
            }, {
                  preset: \'islands#blueCircleDotIconWithCaption\',
                iconCaptionMaxWidth: \'50\',
            }))
    ';
}
if( !empty($page_meta[0]['point']['coords']) ):
    foreach ($page_meta[0]['point']['coords'] as $key => $value)
    {
         if($key == 0) continue;


        $addresses .= '<option value="' . $key . '">' . $page_meta[0]['point']['address'][$key] . ' (' . $page_meta[0]['point']['price'][$key] . 'р.)' . '</option>';


        $add_map .= '
        .add(new ymaps.Placemark([' . $value . '], {
			balloonContent: \'<strong class="js-point-select" 
				id="point-' .$key .'">' . $page_meta[0]['point']
					['address'][$key] . '</strong> \',
                    iconCaption: \'\'
                }, {
                    preset: \'islands#blueCircleDotIconWithCaption\',
                    iconCaptionMaxWidth: \'50\',
                   
                }
		))';
    }
endif;
//
//   dd($page_meta);
?>

<!-- Адрес ПВЗ -->
<div class="step-fields-block" style="max-width:370px;">
	<div class="col-12">
		<div class="mb-3" style="display: block;">
			<label class="form-label" for="Имя">Адрес ПВЗ:</label>
			<input type="text" data-required="" class="form-control " name="shipping_address_1" id="shipping_address" placeholder="" value="Выберите пункт доставки">
		</div>
	</div>
</div>

<!-- Изменение пункта выдачи - ЮЛ -->
<div class="row position-relative order-1 order-lg-0">
	<div class="col-10 col-lg-12 d-flex flex-column flex-lg-row align-items-center card-shadow rounded-2 mx-2 mt-2 mb-4 p-3 pickup-point">
		<div class="col-12 col-lg-6 mb-2 mb-lg-0 text-center text-lg-start">
			<h3>Площадь Льва Толстого</h3>
			<div>
				<span class="title">
					График работы:
				</span>
				<span class="value">8:00 - 18:00</span>
			</div>
			<div>
				<span class="title">
					Срок доставки:
				</span>
				<span class="value">от 2 дней</span>
			</div>
		</div>
		<div class="col-12 col-lg-6 text-center text-lg-end">
			<button class="btn btn-yellow rounded-5" style="max-width:265px;padding:11px;">Изменить пункт выдачи</button>
		</div>
	</div>
</div>

<!-- Выбрать пункт выдачи - Шаг 2 -->
<div class="address-searcher">
    <div class="col-8 mb-4">
        <select id="address-searcher-select" class="form-select" data-required name="self_service" onchange="changeSelectEvent()">
            <option value="0">Выбрать пункт выдачи</option>
            <?= $addresses?>			
        </select>
    </div>	

	<?php // var_dump($fields);?>
    <div id="date-delivery-block" class="col-md-12">
          <div class="row">
              <div class="col-8">
                  <?php

                  foreach ($fields_shipping as $key => $field) {
                      if(in_array($key, ['shipping_wooccm9'])) {
                          cntools_wc_form_field($key, $field, $checkout->get_value($key), 'block', true);						  	
                      }
                  }
                  ?>
              </div>
              <div class="col-12 js-delivery-price-display-block">

              </div>
          </div>
    </div>
    <div class="col-md-12">
        <div class="ya-map" id="map" style="min-height: 450px; margin-top: 10px; background: #f0f0f0;"></div>
    </div>
</div>

<script  type="text/javascript">
    ymaps.ready(init);
    var myMap;

    function init() {
        myMap = new ymaps.Map("map", {
                center:[<?= $geo['coords']?>],
                zoom: 9
            }, {
                searchControlProvider: 'yandex#search'
            });

        myMap.geoObjects<?= $add_map?>
    }

    $(document).on('change click', '[name="self_service"]', function (){
        var $selected = $(this).find(":selected");
        var $coords = $selected.data('coords'), $val = $selected.val();

        //myMap.setCenter($coords.split(','));
        myMap.setZoom(14);

        $('#point-' + $val).trigger('click');

    });
</script>
