<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


$cntools_option_key = 'cntools-options-payment-system-list';
$out = get_option($cntools_option_key);
$result = !empty($out) ? json_decode($out, true): false;

//dd($result);
?>

<div class="note-block">
    <span class="icon-info"><span>!</span></span>
    После подтверждения заказа вам будет отправлено ссылка на оплату в зависиомтв от выброоного метода оплаты!
</div>
<?php
$available_gateways = WC()->payment_gateways()->get_available_payment_gateways();
?>


<div class="row">
    <div class="col-12">



        <ul class="self-service-list">
            <?php $i = 0; foreach ($available_gateways as $gateway): ?>
            <li>
                <label>
                    <input type="radio" <?= $i == 0  ? 'checked' : ''?> name="payment_method" value="<?= $gateway->id?>">
                    <span></span> <?= $gateway->settings['title']?>
                </label>

                <?php if ($gateway->id == 'cheque'): ?>
                    <div class="payment-title">Поддерживаемые платежные системы:</div>

                    <ul class="payment-icon-list">
                        <?php foreach ($result as $item): ?>
                        <li><img src="<?= $item['icon']?>" title="<?= $item['line']?>"></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

            </li>
           <?php $i++; endforeach; ?>
        </ul>


    </div>
    <div class="col-md-12">
        <br>
        <div data-href="/checkout/?step=2" class="js-steps-nav button-cancel btn btn-white bradius30">Вернуться</div>
        <div data-href="/checkout/?step=4" class="js-steps-nav button btn btn-primary">Продолжить</div>
    </div>
</div>
