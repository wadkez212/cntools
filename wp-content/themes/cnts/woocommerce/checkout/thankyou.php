<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

global  $wpdb;


//dd( get_post_meta($order->get_order_number(), "supplier_id" ));

$tmp = $wpdb->get_row("SELECT `order_item_id` FROM {$wpdb->prefix}woocommerce_order_items WHERE order_id = {$order->get_order_number()} AND order_item_type = 'shipping'");

//dd($order->get_data());
//dd($order);

if(!empty($tmp->order_item_id)) {

//    $out = $wpdb->update( $wpdb->prefix . 'woocommerce_order_itemmeta',
//        [ 'meta_value' => '111'],
//        [ 'meta_id' => 895 ],
//        [ '%s', '%d' ],
//        [ '%d' ]
//    );


    $url = 'https://3dsec.sberbank.ru/payment/rest/register.do?userName=t7814802479-api&password=EzR9zTTW&orderNumber=' . $order->get_order_number() . '&amount=' . $order->get_subtotal()*100 . '&returnUrl=https://cntools.ru/result-success&failUrl=https://cntools.ru/result-fail';


    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

//for debug only!
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    $resp = curl_exec($curl);
    curl_close($curl);
//    var_dump($resp);

    if(!empty($resp)) {
        $resp = json_decode($resp);
    }


    if(!empty($resp->errorCode)) {

    } else {
        $out = $wpdb->insert( $wpdb->prefix . 'supplier_orders',
            [
                'cart_hash' => $order->get_cart_hash(),
                'order_id' => $order->get_order_number(),
                'product_id' => 895,
                'supplier_id' => 'test',
                'status' => 895,
                'price' => $order->get_subtotal(),
                'quantity' => 895,
                'orderId' => $resp->orderId,
                'formUrl' => $resp->formUrl,
                'create_at' => date('Y-m-d H:i:s'),
                'update_at' => date('Y-m-d H:i:s'),
            ]
        );


    }


}



?>
<div class="card card-shadow">
    <div class="card-body">
        <div class="row">
            <div class="row">
                <div class="col-12">
                    <div class="note-block">
                        <span class="icon-info" style="background: green"><span>!</span></span>
                        Спасибо. Ваш заказ принять. Скоро с вами свяжется наш менеджер.
                    </div>

                    <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

                        <li class="woocommerce-order-overview__order order">
                            <?php esc_html_e( 'Order number:', 'woocommerce' ); ?>
                            <strong><?php echo $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
                        </li>

                        <li class="woocommerce-order-overview__date date">
                            <?php esc_html_e( 'Date:', 'woocommerce' ); ?>
                            <strong><?php echo wc_format_datetime( $order->get_date_created() ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
                        </li>

                        <?php if ( is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email() ) : ?>
                            <li class="woocommerce-order-overview__email email">
                                <?php esc_html_e( 'Email:', 'woocommerce' ); ?>
                                <strong><?php echo $order->get_billing_email(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
                            </li>
                        <?php endif; ?>

                        <li class="woocommerce-order-overview__total total">
                            <?php esc_html_e( 'Total:', 'woocommerce' ); ?>
                            <strong><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></strong>
                        </li>

                        <?php if ( $order->get_payment_method_title() ) : ?>
                            <li class="woocommerce-order-overview__payment-method method">
                                <?php esc_html_e( 'Payment method:', 'woocommerce' ); ?>
                                <strong><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></strong>
                            </li>
                        <?php endif; ?>

                    </ul>


                </div>
            </div>
        </div>
    </div>
</div>

<?php get_template_part( 'template-parts/front-page-popular-products'); ?>
