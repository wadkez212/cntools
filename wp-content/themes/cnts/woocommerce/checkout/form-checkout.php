<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$geo = get_geo();

$cart_data =  WC()->cart->get_cart();
$product_ids = array_column($cart_data, 'product_id');

$steps_arr = [
   1 => 'Контактные данные',
   2 => 'Адрес доставки',
   3 => 'Заказ создан',
  // 4 => 'Оплата',
];

$step = empty($_GET['step']) ? 1 : $_GET['step'];

$fields = $checkout->get_checkout_fields('billing');
$fields_shipping = $checkout->get_checkout_fields('shipping');
$cdek = new Public\CdekConnection();
$cdek->getPvz();

if(!empty($_COOKIE['cntools_checkout_steps_data'])) {
	$string = strtr($_COOKIE['cntools_checkout_steps_data'], ['\\' => '']);
	$steps_inputs_arr = json_decode($string, true);
}
?>

<br>

<!-- Breadcrumbs -->
<ul class="nav nav-pills nav-fill checkout-steps" id="myTab" role="tablist">
	<?php foreach($steps_arr as $skey => $title): ?>
	<li class="nav-item <?= $skey == $step ? 'active' : '' ?>" role="presentation">
		<a href="/checkout/?step=<?=$skey?>" class="nav-link"><b><?= $title?></b></a>
		<span></span>
	</li>
	<?php endforeach; ?>
</ul>

<br>

<div class="row" style="margin-bottom: 60px;">
	
	<!-- Left column -->
    <div class="col-xl-8 order-1 order-xl-0">
        <div class="cart_totals properties">
            <div class="card-body steps">
                <div class="cntools-checkout">
                <?php
                    if(file_exists(__DIR__ . '/steps/step-' . $step . '.php')) {
                        require_once 'steps/step-' . $step . '.php';
                    }
                ?>
                </div>
            </div>
        </div>
    </div>
	
	<!-- Right column -->
    <div class="col-xl-4 mt-4 mt-xl-0">
		 
		<!-- Step 3-->
		<?php  if(!empty($_GET['step'])): ?>
		<?php if( in_array($_GET['step'], [3]) ): ?>
		<div class="cart_totals properties">
			<div class="card-body order-done">
				<div class="card card-shadow mb-4 p-4 blue-bg">
					<div class="note-block d-flex flex-column justify-content-center align-items-center bg-transparent m-auto p-0">
						<span class="position-relative top-0 start-0 icon-info mb-3" style="width:64px;height:64px;background: #0FBE00;">
							<span style="font-size: 36px;">✓</span>
						</span>
						<span class="title-step3 mb-3 <?php if( in_array($_GET['step'], [3]) )  echo 'text-white'?>">Ваш заказ создан!</span>
					</div>
					<ul class="options order-done-list">
						<li class="noafter nobefore d-flex justify-content-between mb-2 flex-wrap">
							<span class="title <?php if( in_array($_GET['step'], [3]) )  echo 'text-white'?>">Номер заказа:</span>
							<span class="value <?php if( in_array($_GET['step'], [3]) )  echo 'text-white'?>">69122</span>
						</li>
						<li class="noafter nobefore d-flex justify-content-between mb-2 flex-wrap" >
							<span class="title <?php if( in_array($_GET['step'], [3]) )  echo 'text-white'?>">Стоимость:</span>
							<span class="value <?php if( in_array($_GET['step'], [3]) )  echo 'text-white'?>"><span class="woocommerce-Price-amount amount">12600.00</span> ₽</span>
						</li>
					</ul>
					<div class="note-block fw-semibold">
						<span class="icon-info">
							<span>!</span>
						</span>
						<span style="font-weight: 600;font-size: 16px;line-height: 20px;color: #3A3A3A;">После обработки заказа менеджер свяжется с вами и вышлет ссылку на оплату</span>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<?php endif; ?>
		
		<!-- Step 1 -->
        <div class="cart_totals properties">			
			<div class="card-body">
				<div class="card card-shadow mb-4 p-4">
					<h4 class="title-h4 mb-3">Ваш заказ</h4>
					<ul class="options">
						<li class="noafter nobefore d-flex justify-content-between mb-2" style="width: 100%;">
							<span class="title">Количество поставщиков:</span>
							<span class="value"><?php echo '?3'; ?></span>
						</li>
						<li class="noafter nobefore d-flex justify-content-between mb-2" style="width: 100%;">
							<span class="title">Всего товаров:</span>
							<span class="value"><span class="woocommerce-Price-amount amount"><?php echo WC()->cart->get_cart_contents_count() ?></span></span>
						</li>
						<li class="noafter nobefore d-flex justify-content-between mb-2 flex-wrap" style="width: 100%;">
							<span class="title">Стоимость товаров:</span>
							<span class="value"><?php wc_cart_totals_subtotal_html(); ?></span>
						</li>
						<li class="noafter nobefore d-flex justify-content-between mb-3" style="width: 100%;">
							<span class="title">Срок доставки:</span>
							<span class="value"><b><?php echo '?4'; ?></b>дня</span>
						</li>
					</ul>
					
					<!-- Кнопка "Оформить заказ" в правой колонке -->
					<?php  if($step == 1 || $step == 2): ?>
						<div class="col-md-12">
	<div data-href="/checkout/?step=<?php echo $step+1 ?>" class="js-steps-nav button btn btn-primary steps-proceed-btn">Оформить заказ</div>
						</div>
					<?php endif; ?>
					
					<!-- Step 3 КОНТАКТНЫЕ ДАННЫЕ -->
					<?php  if(!empty($_GET['step'])): ?>
					<?php if( in_array($_GET['step'], [3]) ): ?>
					<h4 class="title-h4">Контактные данные</h4>	
					<ul class="options">
						
					<?php foreach($fields as $field) : ?>
					<?php 
						
						$key = $field['key'];
						
						$value = !empty($steps_inputs_arr[$key]) ? $steps_inputs_arr[$key] : false;						
// 						$value = $value == 1 && in_array($key,['billing_wooccm12', 'billing_wooccm13']) ? 'Да' : $value;
						$fields_arr = [
							'billing_wooccm12',
							'billing_last_name',
							'billing_first_name',
							'billing_email',
							'billing_phone',
						];
						
// 						if($key == 'billing_wooccm13' && $value == 1) {
						if(!empty($steps_inputs_arr['billing_wooccm13'])){	
							$fields_arr = [
								'billing_wooccm24',
								'billing_company',
								'billing_wooccm15',
								'billing_wooccm16',
								'billing_wooccm19', 
								'billing_wooccm21',
								'billing_wooccm23',
								'billing_last_name',
								'billing_first_name',
								'billing_wooccm11',
								'billing_email',
								'billing_phone'
							];
						}
						?>
						<?php if(in_array($field['key'], $fields_arr)) : ?>
						<?php if(!empty($value)): ?>
						<li class="noafter nobefore d-flex justify-content-between mb-2 flex-wrap" style="width: 100%;">
							<span class="title"><?= $field['label']?>:</span>
							<span class="value"><?= $value?></span>
						</li>
						<?php endif; ?>
						<?php endif; ?>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
				<?php endif; ?>
				</div>
				
				<!-- Step 2 - Получение -->		
				<?php  if(!empty($_GET['step'])): ?>
				<?php if( in_array($_GET['step'], [2, 3]) ): ?>
				<div class="card card-shadow mb-4 p-4 s3-order-list">
					<h4 class = "title-h4 mb-3">Состав заказа</h4>

					<div class="step-delivery-wrapper">
						<h3 class="step-delivery-label">
							Поставщик <b>?000000000</b>
						</h3>
						<div class="step-delivery-block">
							<span class="step cart-card-label">2 дня</span>
							<span class="step cart-card-label">
								<i>?350</i>₽
							</span>
							<a class="step-card-label" data-bs-toggle="modal" data-bs-target="#stepCardModal">
								<b>?2</b> товара
							</a>
						</div>
					</div>

					<div class="step-delivery-wrapper">
						<h3 class="step-delivery-label">
							Поставщик <b>?000000000</b>
						</h3>
						<div class="step-delivery-block">
							<span class="step cart-card-label">2 дня</span>
							<span class="step cart-card-label">
								<i>?350</i>₽
							</span>
							<a class="step-card-label step-link" data-bs-toggle="modal" data-bs-target="#stepCardModal">
								<b>?2</b> товара
							</a>
						</div>
					</div>

					<div class="step-delivery-wrapper">
						<h3 class="step-delivery-label">
							Поставщик <b>?000000000</b>
						</h3>
						<div class="step-delivery-block">
							<span class="step cart-card-label">2 дня</span>
							<span class="step cart-card-label">
								<i>?350</i>₽
							</span>
							<a class="step-card-label step-link" data-bs-toggle="modal" data-bs-target="#stepCardModal">
								<b>?2</b> товара
							</a>
						</div>
					</div>

				</div>
				<?php endif; ?>
				<?php endif; ?>				
            </div>
        </div>
    </div>
</div>

<!-- Модальное окно -->
<?php
	global $wpdb;
	$count_of_suppliers = 0;
	$card_title = '';
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
		$table_name = $wpdb->prefix . "offers";
		$offer = $wpdb->get_results( "SELECT * FROM $table_name WHERE supplier_id = {$cart_item['supplier_id']} AND product_id = {$cart_item['product_id']}", 'ARRAY_A');
		$offer = !empty($offer[0]) ? $offer[0] : false;
		$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
		$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
		if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
			$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
			$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
		}
	}
?>
<div class="modal fade" id="stepCardModal" tabindex="-1" aria-labelledby="stepCardModalLabel" aria-hidden="true">
 <div class="modal-dialog" style="max-width:800px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Закрыть"></button>
			</div>
			<div class="modal-body">
				<div class="card offers-list-card mb-4 border-0">			
					<div class="card-body">
						<div class="row">
							<!-- Card Image -->
							<div class="col-xl-2 product-thumbnail">
								<div class="thumbnail-wrapper">
									<?php
									$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

									if ( ! $product_permalink ) {
										echo $thumbnail; // PHPCS: XSS ok.
									} else {
										printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
									}
									?>
								</div>
							</div>

						<!-- Card content -->
							<div class="col-xl-8 properties">
								<ul class="options text-center">
									<!-- card title -->
									<li class="noafter nobefore card-caption">
										<span class="title">
											<?php
											if ( ! $product_permalink ) {
												echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
											} else {
												echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
											}

											do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

											// Meta data.
											echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

											// Backorder notification.
											if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
												echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
											}
											?>
										</span>
								</li>
								<!-- card items -->
								<li style="width: 100%;">
									<span class="title">АРТИКУЛ</span>
									<span class="value"><?= str_pad($cart_item['supplier_id'], 8, '0', STR_PAD_LEFT);?></span>
								</li>						
								<li style="width: 100%;">
									<span class="value mt-2">1</span>
									<b>шт. /</b>
									<span class="value"> <?= apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.?></span>
								</li>
							</ul>
						</div>						
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>