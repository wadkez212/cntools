<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$fields = $checkout->get_checkout_fields('billing');


?>






<ol class="checkout-steps">
    <li class="active" data-target="1">Контактные данные <span></span></li>
    <li  data-target="2">Получение <span></span></li>
    <li  data-target="3">Оплата <span></span></li>
    <li  data-target="4">Подтверждение заказа <span></span></li>
</ol>

<form name="checkout" method="post" class="checkout cntools-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
    <div class="checkout-steps-wrapper">
        <!--  Контактные данные  -->
        <div class="step-block step-active step-1">
            <?php require_once 'steps/step-1.php'?>
        </div>

        <!--  Получение  -->
        <div class="step-block step-2">
            <?php require_once 'steps/step-2.php'?>
        </div>

        <!--  Оплата  -->
        <div class="step-block step-3">
            <?php require_once 'steps/step-3.php'?>
        </div>

        <!--  Подтверждение заказа   -->
        <div class="step-block step-4">
            <?php require_once 'steps/step-4.php'?>
        </div>

        <div class="flex">
            <div class="flex-inner">
                <div class="js-button-back button-cancel">Вернуться</div>
                <div class="js-button-next button">Продолжить</div>

                <span class="js-send-block" style="display: none;">
                <?php echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order" value="Отправить" data-value="Отправить">Отправить</button>' ); // @codingStandardsIgnoreLine ?>
                <?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>
                </span>
                <?php do_action( 'woocommerce_checkout_place_order' ); ?>

            </div>
        </div>
    </div>
</form>
<br>





