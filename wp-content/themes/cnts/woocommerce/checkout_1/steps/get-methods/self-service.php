<?php
global $woocommerce;
$fields = $woocommerce->checkout->get_checkout_fields('billing');

$page = get_page_by_path( 'goroda/moskva' );
$page_meta = get_post_meta($page->ID, 'city');

$add_map = '';
$addresses = '';


$cdek = new Public\CdekConnection();
$cdek->getPvz();

//dd($cdek->getPvz());

foreach ($cdek->getPvz() as $point) {

//    $addresses .= '<li>
//               <label>
//                   <input class="form-check-input" type="checkbox" name="self_service"  id="point-' .$point->Code .'">
//                   <span></span>';
//    $addresses .= $point->Address;
//    $addresses .= '</label>
//           </li>';

    $addresses .= '<option value="' . $point->Code . '" data-coords="' . $point->coordY .',' . $point->coordX . '">' . $point->Address . '</option>';

    $add_map .= '
    .add(new ymaps.Placemark([' . $point->coordY .',' . $point->coordX . '], {
                balloonContent: \'<strong class="js-point-select" id="point-' .$point->Code .'">' . $point->Address . '</strong> \',
                iconCaption: \'\'
            }, {
                  preset: \'islands#blueCircleDotIconWithCaption\',
                iconCaptionMaxWidth: \'50\',
            }))
    ';
}

foreach ($page_meta[0]['point']['coords'] as $key => $value)
{
     if($key == 0) continue;
//     $addresses .= '<li>
//               <label>
//                   <input type="checkbox" class="form-check-input" name="self_service"  id="point-' .$key .'">
//                   <span></span>';
//     $addresses .= $page_meta[0]['point']['address'][$key] . ' (' . $page_meta[0]['point']['price'][$key] . 'р.)';
//     $addresses .= '</label>
//           </li>';

    $addresses .= '<option value="' . $key . '">' . $page_meta[0]['point']['address'][$key] . ' (' . $page_meta[0]['point']['price'][$key] . 'р.)' . '</option>';


    $add_map .= '
    .add(new ymaps.Placemark([' . $value . '], {
                balloonContent: \'<strong class="js-point-select" id="point-' .$key .'">' . $page_meta[0]['point']['address'][$key] . '</strong> \',
                iconCaption: \'\'
            }, {
                preset: \'islands#blueCircleDotIconWithCaption\',
                iconCaptionMaxWidth: \'50\',
               
            }))
    ';
}
//
//   dd($page_meta);
?>
<div class="row address-searcher">
    <div class="col-12">
        <select class="form-control-select2 select2" name="self_service">
            <option value="0">Выбрать пункт выдачи</option>
            <?= $addresses?>
        </select>
        <script>
            $(".select2").select2({
                tags: true
            });
        </script>
        <br>
        <br>
    </div>
    <div class="col-12 ya-map" id="map" style="min-height: 400px;"></div>
</div>


<div class="row">
    <div class="col-12">
        <?php
        foreach ($fields as $key => $field) {
            if(in_array($key, ['shipping_wooccm9'])) {
                cntools_wc_form_field($key, $field, $checkout->get_value($key));
            }
        }
        ?>
    </div>
</div>


<script  type="text/javascript">

    ymaps.ready(init);
    var myMap;

    function init() {
        myMap = new ymaps.Map("map", {
                center:[59.9, 30.3], //
                zoom: 9
            }, {
                searchControlProvider: 'yandex#search'
            });

        myMap.geoObjects<?= $add_map?>


    }

    $(document).on('change click', '[name="self_service"]', function (){
        var $selected = $(this).select2().find(":selected");
        var $coords = $selected.data('coords'), $val = $selected.val();

        myMap.setCenter($coords.split(','));
        myMap.setZoom(14);

        $('#point-' + $val).trigger('click');

    });




</script>