<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$geo = get_geo();
?>

    <!-- Получение -->
    <div class="city-selector">
        Выберите способ полученяия заказа в <strong><?= get_the_title($geo['city_id'])?></strong>
        <span class="js-change-geo" data-action="get_city_list" data-title="Список городов" data-bs-toggle="modal" data-bs-target="#callback_button" data-city="<?= $geo['city_id']?>">У вас другой город?</span>
    </div>

    <?php
//   $packages = WC()->shipping()->get_packages();
//
//    $shipping_methods = [];
//   foreach ($packages as $package) {
//       if(!empty($package['rates']) ) {
//           $shipping_methods = $package['rates'];
//           break;
//       }
//   }





    $zones = WC_Shipping_Zones::get_zones();
    $methods = array_column( $zones, 'shipping_methods' );
    $shipping_methods = $methods[0];



   ?>

    <input type="hidden" name="shipping_method[0]" value="" class="shipping_method">

    <input type="hidden" class="billing_country" name="billing_country" value="RU">
    <input type="hidden" class="billing_city" name="billing_city" value="Санкт-перетбург">

    <input type="hidden" class="billing_address_1" name="billing_address_1" value="">
    <input type="hidden" class="billing_address_2" name="billing_address_2" value="">
    <input type="hidden" class="shipping_address_1" name="billing_address_2" value="">
    <input type="hidden" class="shipping_address_2" name="billing_address_2" value="">
<!--local_pickup:2-->
    <div class="select-get-method">
        <?php foreach ($shipping_methods as $key => $method): ?>
        <li class="js-get-method" data-action="<?= $method->id?>_<?= $key?>" data-value="<?= $method->id?>:<?= $key?>"
            data-cost="<?= $method->cost?>" data-address="<?= $method->title?>">
            <a class="" href="#"><?= $method->title?></a>
        </li>
        <?php endforeach?>
    </div>

   <div class="selected-get-method-wrapper">
   </div>
