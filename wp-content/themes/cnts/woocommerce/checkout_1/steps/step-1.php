<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

?>
<div class="note-block">
    <span class="icon-info"><span>!</span></span>
    Уже покупали на сайте. Войдите в <strong>личный кабинет</strong> или введите E-mail.
</div>
<br>
<div class="row">

    <div class="col-md-2">
    <?php
    foreach ($fields as $key => $field) {
        if(in_array($key, ['billing_wooccm12', ]))
            cntools_wc_form_field($key, $field, $checkout->get_value($key));
    }
    ?>
    </div>
    <div class="col-md-2">
        <?php
        foreach ($fields as $key => $field) {
            if(in_array($key, ['billing_wooccm13']))
                cntools_wc_form_field($key, $field, $checkout->get_value($key));
        }
        ?>
    </div>

    <div class="col-md-12">
        <?php
        foreach ($fields as $key => $field) {
            if(in_array($key, ['billing_company',]))
                cntools_wc_form_field($key, $field, $checkout->get_value($key), 'none');
        }
        ?>
    </div>

    <div class="col-md-12">
    </div>

    <div class="col-md-3">
        <?php
        foreach ($fields as $key => $field) {
            if(in_array($key, [ 'billing_last_name']))
                cntools_wc_form_field($key, $field, $checkout->get_value($key));
        }
        ?>
    </div>
    <div class="col-md-3">
        <?php
        foreach ($fields as $key => $field) {
            if(in_array($key, ['billing_first_name']))
                cntools_wc_form_field($key, $field, $checkout->get_value($key));
        }
        ?>
    </div>
    <div class="col-md-3">
        <?php
        foreach ($fields as $key => $field) {
            if(in_array($key, ['billing_wooccm11']))
                cntools_wc_form_field($key, $field, $checkout->get_value($key));
        }
        ?>
    </div>

    <div class="col-md-9">
        <?php
        foreach ($fields as $key => $field) {
            if(in_array($key, ['billing_email',]))
                cntools_wc_form_field($key, $field, $checkout->get_value($key));
        }
        ?>
    </div>

    <div class="col-md-9">
        <?php
        foreach ($fields as $key => $field) {
            if(in_array($key, ['billing_phone',]))
                cntools_wc_form_field($key, $field, $checkout->get_value($key));
        }
        ?>
    </div>
</div>