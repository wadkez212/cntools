<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$fields = $checkout->get_checkout_fields('billing');

?>

<br>
<ul class="nav nav-pills nav-fill checkout-steps" id="myTab" role="tablist">
    <li class="nav-item active" role="presentation">
        <button class="nav-link active" id="home-tab" data-target="1" data-bs-toggle="tab" data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true"><b>Контактные данные</b></button>
        <span></span>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="profile-tab"   data-target="2" data-bs-toggle="tab" data-bs-target="#profile-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false" disabled><b>Получение</b></button>
        <span></span>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="contact-tab" data-target="3" data-bs-toggle="tab" data-bs-target="#contact-tab-pane" type="button" role="tab" aria-controls="contact-tab-pane" aria-selected="false" disabled><b>Оплата</b></button>
        <span></span>
    </li>
    <li class="nav-item" role="presentation">
        <button class="nav-link" id="disabled-tab" data-target="4" data-bs-toggle="tab" data-bs-target="#disabled-tab-pane" type="button" role="tab" aria-controls="disabled-tab-pane" aria-selected="false" disabled><b>Подтверждение заказа</b></button>
        <span></span>
    </li>
</ul>
<br>
<form name="checkout" method="post" class="checkout cntools-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
            <?php require_once 'steps/step-1.php'?>
        </div>
        <div class="tab-pane fade" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
            <?php require_once 'steps/step-2.php'?>
        </div>
        <div class="tab-pane fade" id="contact-tab-pane" role="tabpanel" aria-labelledby="contact-tab" tabindex="0">
            <?php require_once 'steps/step-3.php'?>
        </div>
        <div class="tab-pane fade" id="disabled-tab-pane" role="tabpanel" aria-labelledby="disabled-tab" tabindex="0">
            <?php require_once 'steps/step-4.php'?>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <div class="js-button-back button-cancel btn btn-white bradius30">Вернуться</div>
            <div class="js-button-next button btn btn-primary">Продолжить</div>

            <?php echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="btn btn-primary js-send-block"  style="display: none;" name="woocommerce_checkout_place_order" id="place_order" value="Отправить" data-value="Отправить">Отправить</button>' ); // @codingStandardsIgnoreLine ?>
            <?php wp_nonce_field( 'woocommerce-process_checkout', 'woocommerce-process-checkout-nonce' ); ?>

            <?php do_action( 'woocommerce_checkout_place_order' ); ?>
        </div>
    </div>
</form>
<br>
<br>
<br>





