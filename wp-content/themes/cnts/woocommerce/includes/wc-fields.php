<?php


add_filter( 'woocommerce_billing_fields', 'additional_billing_fields', 20, 1 );
function additional_billing_fields($billing_fields) {
    if ( is_wc_endpoint_url( 'edit-address' ) ) {
        foreach ( extra_register_fields() as $fkey => $values ) {
            if ( in_array($fkey, ['billing_last_name']) ) {
                $billing_fields['billing_'.$fkey] = $values;
            }
        }
    }
    return $billing_fields;
}