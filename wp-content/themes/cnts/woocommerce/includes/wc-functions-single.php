<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/*
 * Wrappers single product
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
add_action( 'woocommerce_before_main_content', 'estore_add_breadcrumbs', 20 );
function estore_add_breadcrumbs(){
    ?>
    <div class="breadcrumb_dress">
        <div class="container dd">
            <?php woocommerce_breadcrumb([
                'wrap_before' => '<nav class="breadcrumb">',
                'wrap_after'  => '</nav>',
            ]); ?>
        </div>
    </div>

    <?php
}


add_action( 'woocommerce_before_single_product', 'goonline_wrapper_product_start_bootstrap', 5 );

function goonline_wrapper_product_start_bootstrap () {
    require dirname(__DIR__, '1') . '/single-product/custom-middle.php';
}


function goonline_wrapper_product_start() {

    global $product;
    global $wpdb;
    $post_id = get_the_ID();
    $table_name = $wpdb->prefix . "offers";

    $post_thumbnail_id = $product->get_image_id();
    $min_price = $wpdb->get_results( "SELECT MIN(price) as price FROM $table_name WHERE product_id = {$product->ID} LIMIT 1" );

//    dd($min_price);
    ?>



    <div class="container product-single">
        <?php if (class_exists('WooCommerce') && is_woocommerce()) : ?>
            <?php woocommerce_breadcrumb(); ?>
        <?php endif; ?>
        <div class="flex">
            <div class="flex-inner">
                <h1 class="product-title"><?php woocommerce_template_single_title();?></h1>
            </div>
        </div>
        <div class="flex product-code-and-rate">
            <div class="flex-inner rate">
                <?php woocommerce_template_single_meta();?>
            </div>
        </div>
        <div class="flex">
            <div class="flex-inner product-images">
                <div class="flex">
                    <div class="flex-inner image">
                        <?php woocommerce_show_product_images(); ?>
                    </div>
                </div>
            </div>

            <div class="flex-inner">
                <div class="product-attrs-intro">
                    <?php cntool_product_guarantee()?>
                    <?php cntool_tech_properties_list(get_the_ID(), false, 6, 'attrs-intro-list')?>
                </div>
            </div>

            <div class="flex-inner product-cart-tools">
                <div class="cart-block">
<!--                    <div class="flex-inner price" style="text-align: center; padding: 10px;">-->
<!--                        от --><?//= $min_price[0]->price ?><!-- <span class="woocommerce-Price-currencySymbol" style="color: #888;">₽</span>-->
<!--                    </div>-->
<!--                    --><?php //woocommerce_template_single_rating();?>
<!--                    --><?php //woocommerce_template_single_price();?>
<!--                    --><?php //woocommerce_template_single_add_to_cart();?>
                    <button class="add-cart-button" href="#offers">К предложениям</button>
                    <button type="submit" class="quick-shop-button" value="25">Связатся с менеджером</button>
                    <?php // cntools_fast_order_button() ?>

                    <?php //cntools_single_product_info() ?>
                    <?php  //woocommerce_template_single_excerpt();?>
                    <?php // woocommerce_show_product_sale_flash();?>
                    <br>
                    <br>
                    <br>
                </div>


            </div>


        </div>

        <div class="flex">
            <div class="flex-inner">
                <br>
                <br>
                <h2>Краткое описание и характеристики</h2>
                 <?= the_content()?>
            </div>
        </div>


        <div class="flex all-offers">
            <div class="flex-inner">
                <h2 class="title">Все предложения по товару: <?= the_title()?></h2>
                <br>




                <?php
                $suppliers = get_posts([
                    'post_type' => 'suppliers'
                ]);


                $offers = $wpdb->get_results( "SELECT * FROM $table_name WHERE product_id = $post_id ORDER BY price ASC" );

                $suppliers_arr = [];
                foreach ($suppliers as $supplier) {
                    $suppliers_arr[$supplier->ID] = $supplier;
                }
                ?>


                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Код поставщика</th>
                        <th>Бренд</th>
                        <th>Наличие</th>
                        <th style="text-align: right">Стоимость</th>
                        <th style="text-align: right">Оформление заказа</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($offers as $offer): ?>
                        <tr>
                            <td><?= str_pad($offer->supplier_id, 8, '0', STR_PAD_LEFT);?></td>
                            <td>&mdash;</td>
                            <td><?= $offer->quantity?> штук</td>
                            <td style="text-align: right"><?= $offer->price?></td>
                            <td style="text-align: right"><div class="js-add-to-cart" data-product="<?= $offer->product_id?>" data-price="<?= $offer->price?>" data-supplier="<?= $offer->supplier_id?>"><i class="fa fa-shopping-cart"></i> <span>В корзину</span></div></td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="flex">
            <div class="flex-inner product-tabs">
                <?php woocommerce_output_product_data_tabs();?>
            </div>
        </div>


    </div>
    <?php
}



add_action( 'woocommerce_after_single_product', 'goonline_wrapper_product_end', 5 );
function goonline_wrapper_product_end() {

}


remove_all_filters('woocommerce_single_product_summary');
remove_all_filters('woocommerce_before_single_product_summary');
remove_all_filters('woocommerce_after_single_product_summary');

