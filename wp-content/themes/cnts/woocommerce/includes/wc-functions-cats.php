<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

function woocommerce_template_loop_product_link_open() {
    global $product;

    $link = apply_filters( 'woocommerce_loop_product_link', get_the_permalink(), $product );

    echo '<a href="' . esc_url( $link ) . '" class="product-img-wrapper">';
}

/**
 * Insert the closing anchor tag for products in the loop.
 */
function woocommerce_template_loop_product_link_close() {
    echo '</a>';
}