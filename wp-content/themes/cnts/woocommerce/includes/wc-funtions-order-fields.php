<?php

function cntools_wc_form_field($key, $args, $value = null, $display = 'block', $required = false, $checked = false) {

    if($args['type'] == 'checkbox') {
        ?>
        <div class="mb-3 checkbox-wrapper">
            <input type="checkbox" <?= $checked ? 'checked' : ''?> <?= $required ? 'data-required' : '' ?> class="<?= !empty($args['input_class']) ? join(' ', $args['input_class'])  : ''?> form-check-input" name="<?= $key?>" value="1"  id="<?= $args['name']?>">
            <label for="<?= $args['name']?>">
            <?= $args['label']?>
            </label>
        </div>
        <?php
    }

   if($args['type'] == 'select') {
       ?>
	<div class="mb-3" style="display: <?= $display?>;">
		<label class="form-label" for="<?= $args['label']?>"><?= $args['label']?>*</label>
		<select id="<?php echo $args['key']?>"
				class="form-control select-field <?= !empty($args['class']) ? join(' ', $args['class'])  : ''?>" name="<?php echo $args['name']?>"
				<?= $required ? 'data-required' : '' ?>>

			<?php foreach($args['options'] as $list => $item): ?>
			<option value="<?php echo $item ?>">
				<?php echo $item ?>
			</option>
			<?php endforeach; ?>
		</select>
	</div>
<?php
   }

   if($args['type'] == 'text') {
       ?>
       <div class="mb-3" style="display: <?= $display?>;">
           <label class="form-label" for="<?= $args['label']?>"><?= $args['label']?>*</label>
           <input type="text" <?= $required ? 'data-required' : '' ?> class="form-control <?= !empty($args['input_class']) ? join(' ', $args['input_class'])  : ''?>" name="<?= $key?>" id="<?= $args['name']?>" placeholder="" value="<?= $value?>">
       </div>
<?php
   }

    if($args['type'] == 'date') {
        ?>
        <div class="mb-3" style="display: <?= $display?>;">
            <label class="form-label" for="<?= $args['label']?>"><?= $args['label']?></label>
            <input type="text" <?= $required ? 'data-required' : '' ?> class="form-control  date-picker <?= !empty($args['input_class']) ? join(' ', $args['input_class'])  : ''?>" name="<?= $key?>" id="<?= $args['name']?>" placeholder="" value="<?= $value?>">
        </div>
        <?php
    }

    if($args['type'] == 'email') {
        ?>
        <div class="mb-3">
            <label class="form-label" for="<?= $args['label']?>"><?= $args['label']?>*</label>
            <input type="text" <?= $required ? 'data-required' : '' ?> class="form-control <?= !empty($args['input_class']) ? join(' ', $args['input_class'])  : ''?>" name="<?= $key?>" id="<?= $args['name']?>" placeholder="" value="<?= $value?>">
        </div>
        <?php
    }

    if($args['type'] == 'tel') {
        ?>
        <div class="mb-3">
            <label class="form-label" for="<?= $args['label']?>"><?= $args['label']?>*</label>
            <input type="text" <?= $required ? 'data-required' : '' ?> class="form-control <?= !empty($args['input_class']) ? join(' ', $args['input_class'])  : ''?>" name="<?= $key?>" id="<?= $args['name']?>" placeholder="" value="<?= $value?>">
        </div>
        <?php
    }
	
    if($args['type'] == 'state') {
        ?>
        <div class="mb-3">
            <label class="form-label" for="<?= $args['label']?>"><?= $args['label']?>*</label>
            <input type="text" <?= $required ? 'data-required' : '' ?> class="form-control <?= !empty($args['input_class']) ? join(' ', $args['input_class'])  : ''?>" name="<?= $key?>" id="<?= $args['name']?>" placeholder="" value="<?= $value?>">
        </div>
        <?php
    }

    if($args['type'] == 'hidden') {
        ?>
        <input type="hidden" <?= $required ? 'data-required' : '' ?> class="<?= !empty($args['input_class']) ? join(' ', $args['input_class'])  : ''?>" name="<?= $key?>" id="<?= $args['name']?>" value="<?= $value?>">
        <?php
    }
}