<?php

add_action( 'woocommerce_shipping_init', 'truemisha_add_shipping_method' );

function truemisha_add_shipping_method() {

    if ( ! class_exists( 'WC_Cnts_Shipping_Method' ) ) {

        class WC_Cnts_Shipping_Method extends WC_Shipping_Method {
            /**
             * Конструктор класса
             */
            public function __construct() {
                $this->id = 'cnts_shipping_method';
                $this->method_title = 'Способ получения заказа пвз СДЕК';
                $this->method_description = 'Какое-то описание способа доставки';
                $this->enabled = 'yes'; // эту опцию можно добавить внутрь настроек, в данном случае мы захардкодили её немного
                $this->init();
            }

            /**
             * Инициализируем настройки
             */
            function init() {
                // API настроек
                // $this->init_form_fields(); // Мы можем перезаписать этот метод, чтобы добавить свои настройки
                // $this->init_settings(); // This is part of the settings API. Loads settings you previously init.

                // Сохраняем настройки
                add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
            }

            /**
             * Расчёт стоимости доставки
             */
            public function calculate_shipping( $package = []) {
                $this->add_rate(
                    array(
                        'label'    => 'Какой-то описание', // можем использовать $this->get_title()
                        'cost'     => '10.99', // стоимость в текущей валюте магазина
                        'calc_tax' => 'per_item' // может быть per_item и per_order
                    )
                );
            }
        }
    }

}


add_filter( 'woocommerce_shipping_methods', 'truemisha_add_shipping_class' );

function truemisha_add_shipping_class( $methods ) {
    $methods[ 'truemisha_shipping_method' ] = 'WC_Cnts_Shipping_Method';
    return $methods;
}


add_action('woocommerce_checkout_order_processed', 'enroll_student', 10, 1);

function enroll_student($order_id)
{


//    echo $order_id;
//    dd($order_id);
//    update_post_meta($order_id, '_order_shipping', 222);
}
