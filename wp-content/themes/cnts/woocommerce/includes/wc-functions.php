<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * @return void
 */
function cntools_fast_order_button() {
?>
    <button type="submit" class="quick-shop-button" value="25">Быстрый заказ</button>
<?php
}


/**
 * @return void
 */
function cntools_single_product_info() {
    ?>
    <ul class="product-info">
        <li><span class="fa fa-check-circle"></span> В вашем городе: 40 Шт.</li>
        <li><span class="fa fa-user-circle"></span> Самовывоз:  сегодня, со склада.</li>
        <li><span class="fa fa-shipping-fast"></span> Курьером: Затра от 190 р.</li>
    </ul>
    <?php
}


/**
 * @param $post_id
 * @param $block_title
 * @return void
 */
function cntool_tech_properties_list($post_id, $block_title, $limit = false, $ulClass = 'options')
{
    $option_key = 'cntools-options-product-specifications';
    $out = get_option($option_key);
    $options_arr = !empty($out) ? json_decode($out, true): false;

    if(!empty($options_arr))
    {
        ?>
        <?php if($block_title): ?>
        <h3><?= $block_title?>:</h3>
        <?php endif;  ?>
        <ul class="<?= $ulClass?>">
<?php
        $n = 0;
        foreach ($options_arr as $item)
        {
            $title = $item['title'];
            $key = $item['key'];
            $value = get_post_meta($post_id, $key, true);
            if(!empty($value) && !is_array($value))
            {
                ?>
                <li><span class="title"><?= $title?></span> <span class="value"><?= $value?></span></li>
<?php
            }
            if ($limit && $limit == $n) break;
            $n++;
        }
        ?>
        </ul>
<?php
    }

}


/**
 * @param $post_id
 * @param $block_title
 * @return void
 */
function cntool_product_options_list($option_key, $post_id, $block_title, $show_value = false)
{
    if(empty($option_key)) return false;

    $out = get_option($option_key);
    $options_arr = !empty($out) ? json_decode($out, true): false;

    if(!empty($options_arr))
    {
        ?>
        <?php if($block_title): ?>
        <h3><?= $block_title?>:</h3>
        <?php endif;  ?>
        <ul class="list">
            <?php
            foreach ($options_arr as $item)
            {
                $title = $item['title'];
                $key   = $item['key'];
                $value = get_post_meta($post_id, $key, true);
                if(!empty($value) && !is_array($value))
                {
                    ?>
                    <li><?= $title?><?= $show_value ? ': ' . $value: ''?></li>
                    <?php
                }
            }
            ?>
        </ul>
        <?php
    }

}



/**
 * @param $post_id
 * @param $block_title
 * @return void
 */
function cntool_product_producer_country($option_key, $post_id, $block_title, $show_value = false)
{
    if(empty($option_key)) return false;

    $out = get_option($option_key);
    $options_arr = !empty($out) ? json_decode($out, true): false;


    $key = get_post_meta( $post_id, 'country', 1 );
    $title_keys_arr = array_flip(array_column($options_arr, 'key'));
    $title_key = !empty($title_keys_arr[$key]) ? $title_keys_arr[$key] : false;

    if(array_values($options_arr)[$title_key])
    {
        $title = array_values($options_arr)[$title_key]['title'];
        $flag = array_values($options_arr)[$title_key]['icon'];
    }


    if(!empty($options_arr))
    {
        ?>
        <?php if($block_title): ?>
        <h3><?= $block_title?>:</h3>
         <?php endif;  ?>
        <?php if(!empty($title)): ?>
            <div class="producer">
                <img src="<?= $flag?>" alt="<?= $title?>">  <?= $title?> &mdash; страна производитель
            </div>
        <?php endif;  ?>

        <?php
    }

}

/**
 * cntool_product_guarantee
 * @return void
 */
function cntool_product_guarantee()
{
    $result = get_post_meta( get_the_ID(), 'guarantee', 1 );
    if(!empty($result))
    {
        ?>
        <strong><i class="fa fa-shield-alt"></i> <span>Гарантия производителя <?= $result?> месяцев</span></strong>
<?php
    }

}



function woocommerce_template_loop_product_thumbnail() {
    ?>
    <?php
//     $rationClass = '';
//        $imgInfo = getimagesize(get_the_post_thumbnail_url(get_the_ID()));
//        $imageWidth = $imgInfo[0];
//        $imageHeight = $imgInfo[1];
//
//
//
//
//        if ($imageWidth < 254 || $imageHeight < 240) {
//
//        } else {
//            if ($imageHeight > 240) {
//                $imageWidth = $imageWidth / ($imageHeight/240);
//                $imageHeight = 240;
//            }
//
//            if($imageWidth > 254 && $imageHeight < 240) {
//                $imageWidth = 100;
//                $imageHeight = $imageHeight / ($imageWidth/254);
//            }
//        }
//
//        $divisor = gmp_intval( gmp_gcd( $imageWidth, $imageHeight ) );
//        $aspectRatio = $imageWidth / $divisor . ' / ' . $imageHeight / $divisor;
    ?>
    <div class="product-loop-image-wrapper">
    <?php if(get_the_post_thumbnail_url(get_the_ID())): ?>
       <img src="<?=get_the_post_thumbnail_url(get_the_ID())?>" style="aspect-ratio: <?= $aspectRatio?>;" alt="<?= get_the_title()?>">
    <?php endif;?>
    </div>
<?php
}



function codeGen($length = 6) {
    $result = '';
    for($i = 0; $i < $length; $i++) {
        $result .= mt_rand(0, 9);
    }
    return $result;
}




function parsePage($page_url = false) {
    $HTMLCode = file_get_contents($page_url);
    $dom = new DomDocument();
    @$dom->loadHTML(mb_convert_encoding($HTMLCode,'HTML-ENTITIES','UTF-8'));
    $xpath = new DomXPath( $dom );

    $title = $xpath->query("//h1");
    $img_src = $xpath->query('//a[@class="image-after selected"]/@href');

    $prop_1_titles = $xpath->query('//div[@class="product-data row"]/div/span');
    $prop_1_values = $xpath->query('//div[@class="product-data row"]/div[@class="col-xs-5 col-sm-4"]');

    $prop_titles = $xpath->query('//div[@class="tab-content"]/div/div[@class="product-data row"]/div/span');
    $prop_values = $xpath->query('//div[@class="tab-content"]/div/div[@class="product-data row"]/div[@class="col-xs-6 col-md-7"]');

    $description = $xpath->query('//div[@id="tab-description"]');

    $result = [];
    foreach ( $title as $item) {
        $result['title'] = $item->textContent;
    }

    foreach ( $img_src as $item) {
        $result['img'] = $item->textContent;
    }

    $i = 0;
    foreach ( $prop_1_titles as $item) {
        $result['prop_title'][] = trim($item->textContent, ':');
        if($i==1) break;
        $i++;
    }

    $i = 0;
    foreach ( $prop_1_values as $item) {
        $result['prop_values'][] = $item->textContent;
        if($i==1) break;
        $i++;
    }

    foreach ( $prop_titles as $item) {
        $result['prop_title'][] = $item->textContent;
    }

    foreach ( $prop_values as $item) {
        $result['prop_values'][] = $item->textContent;
    }

    foreach ( $description as $item) {
        $result['description'] = $item->textContent;
    }

    return $result;

}