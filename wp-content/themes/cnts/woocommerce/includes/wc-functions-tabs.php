<?php

function cntools_get_descriptions()
{
    require get_template_directory() . '/woocommerce/single-product/tabs/description.php';
}
function cntools_get_reviews()
{
    require get_template_directory() . '/woocommerce/single-product/tabs/get-reviews.php';
}

function cntools_get_method_tab()
{
    require get_template_directory() . '/woocommerce/single-product/tabs/get-method.php';
}

function cntools_ask_question_tab()
{
    require get_template_directory() . '/woocommerce/single-product/tabs/get-ask-questions.php';
}

function cntools_related_products_tab()
{
    woocommerce_output_related_products();
}

function cntools_video_reviews_tab()
{
    require get_template_directory() . '/woocommerce/single-product/tabs/get-video-reviews.php';
}
