<?php
/**
 * Cart totals
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-totals.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 2.3.6
 */

defined( 'ABSPATH' ) || exit;
?>
<?php $count_of_suppliers = $_SESSION['count_of_suppliers'];?>
<div class="cart_totals properties <?php echo ( WC()->customer->has_calculated_shipping() ) ? 'calculated_shipping' : ''; ?>">

	<?php do_action( 'woocommerce_before_cart_totals' ); ?>
    <h4 class="title-h4 mb-3">Ваш заказ</h4>
    <ul class="options">
        <li class="noafter d-flex justify-content-between mb-2" style="width: 100%;">
			<span class="title">Выбрано товаров:</span>
			<span class="value"><?php echo WC()->cart->get_cart_contents_count() ?> шт.</span>
		</li>
        <li class="noafter d-flex justify-content-between mb-2" style="width: 100%;">
			<span class="title">Общая стоимость:</span>
			<span class="value total-cart"><?php echo WC()->cart->get_cart_subtotal(); ?></span>
		</li>
		<li class="noafter d-flex justify-content-between mb-2" style="width: 100%;">
			<span class="title">Дата доставки:</span>
			<span class="value">21.01.2022</span>
		</li>
		<li class="noafter d-flex justify-content-between mb-3" style="width: 100%;">
			<span class="title">Количество поставщиков:</span>
			<span class="value"><?php echo $count_of_suppliers; ?></span>
		</li>
    </ul>

	<!-- Button loc: woocommerce/cart/proceed-to-checkout-button.php -->
	<div class="wc-proceed-to-checkout">
		<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
	</div>
	<?php do_action( 'woocommerce_after_cart_totals' ); ?>


</div>
