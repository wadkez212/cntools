<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */
global $wpdb;
defined('ABSPATH') || exit;

do_action('woocommerce_before_cart'); ?>
<div class="row">
    <div class="col-xl-9">

        <form class="woocommerce-cart-form" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
            <?php do_action('woocommerce_before_cart_table'); ?>

            <?php
            $count_of_suppliers = 0;
            $card_title = '';
            foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item):
                $table_name = $wpdb->prefix . "offers";
                $offer = $wpdb->get_results("SELECT * FROM $table_name WHERE supplier_id = {$cart_item['supplier_id']} AND product_id = {$cart_item['product_id']}", 'ARRAY_A');
                $offer = !empty($offer[0]) ? $offer[0] : false;
                $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
                if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                    $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
                    $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);
                    ?>
                    <!-- 		<div class="row mb-3"> -->
                    <!-- 			<div class="col-sm-12 col-md-5 mb-3 mb-md-0 text-md-start align-self-center card-title-box"> -->
                    <?php
                    $curr_title = str_pad($cart_item['supplier_id'], 8, '0', STR_PAD_LEFT);
                    if ($card_title == '' || $card_title != $curr_title) {
                        echo '<div class="row mb-3">';
                        echo '<div class="col-sm-12 col-md-5 mb-3 mb-md-0 text-md-start align-self-center card-title-box">';
                        echo '<h2 class="title-h2 mb-0">Поставщик ' . $curr_title . '</h2>';
                        echo '</div>';
                        echo '<div class="col-sm-12 col-md-7 text-md-end card-count-box">';
                        echo '<span class="cart-card-text me-1">Доставка</span>';
                        echo '<span class="cart-card-label">?21.09.2022</span>';
                        echo '<span class="cart-card-label"><i>?350</i>₽</span>';
                        echo '</div>';
                        echo '</div>';
                        $count_of_suppliers++;
                    }
                    $card_title = $curr_title;
                    ?>
                    <div class="card card-shadow offers-list-card">
                        <div class="card-body">
                            <div class="row">
                                <div class="d-flex flex-column flex-md-row col-md-10 properties">
                                    <div class="product-thumbnail ms-4 me-3">
                                        <div class="thumbnail-wrapper">
                                            <?php
                                            $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

                                            if (!$product_permalink) {
                                                echo $thumbnail; // PHPCS: XSS ok.
                                            } else {
                                                printf('<a href="%s">%s</a>', esc_url($product_permalink), $thumbnail); // PHPCS: XSS ok.
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <ul class="options">
                                        <li class="noafter nobefore card-caption">
                                <span class="d-inline-block title mb-2 title">
                                    <?php
                                    if (!$product_permalink) {
                                        echo wp_kses_post(apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key) . '&nbsp;');
                                    } else {
                                        echo wp_kses_post(apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s</a>', esc_url($product_permalink), $_product->get_name()), $cart_item, $cart_item_key));
                                    }

                                    do_action('woocommerce_after_cart_item_name', $cart_item, $cart_item_key);

                                    // Meta data.
                                    echo wc_get_formatted_cart_item_data($cart_item); // PHPCS: XSS ok.

                                    // Backorder notification.
                                    if ($_product->backorders_require_notification() && $_product->is_on_backorder($cart_item['quantity'])) {
                                        echo wp_kses_post(apply_filters('woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__('Available on backorder', 'woocommerce') . '</p>', $product_id));
                                    }
                                    ?>
                                </span>
                                        </li>
                                        <li style="width: 100%;">
                                            <span class="title">Бренд</span>
                                            <span class="value"><?php echo '<b>?Бренд</b>' ?></span>
                                        </li>
                                        <li style="width: 100%;">
                                            <span class="title">Цена за штуку</span>
                                            <span class="value"> <?= $_product->price ?></span>
                                        </li>
                                        <li style="width: 100%;">
                                            <span class="title">В наличии</span>
                                            <span class="value"><?php echo '<b>' . $_product->get_stock_quantity() . '</b>'; ?></span>
                                        </li>
                                    </ul>

                                </div>

                                <div class="d-flex flex-md-column col-12 mx-auto col-md-2 text-center align-items-center pt-md-3">
						<span class="d-block mb-md-2 cart-card-total">
							<span class="cart-card-total span-cart-total"
                                  data-cartprice="<?= $_product->price ?>"><?php echo apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key); ?></span>
						</span>
                                    <div class="input-group mb-md-3 offer-quantity justify-content-end justify-content-md-center">
                                        <span class="input-group-text js-change-quantity minus" data-operator="minus" data-quantity="<?= $offer['quantity'] ?>">-</span>
                                        <input type="number" class="form-control js-product-quantity qty px-1 text-center card-count" min="0" data-qty
                                               max="<?= $_product->get_max_purchase_quantity() ?>" name="<?= 'cart[' . $cart_item_key . '][qty]' ?>"
                                               data-quantity="<?= $cart_item['quantity'] ?>" value="<?= $cart_item['quantity'] ?>" />
                                        <span class="input-group-text js-change-quantity plus" data-operator="plus" data-quantity="<?= $offer['quantity'] ?>">+</span>
                                    </div>
                                    <?php
                                    echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                                        'woocommerce_cart_item_remove_link',
                                        sprintf(
                                            '<a href="%s" class="remove btn btn-outline-danger small remove-link-btn"  aria-label="%s" data-product_id="%s" data-product_sku="%s" title="Удалить позицию"><i class="fa fa-trash"></i>Удалить</a>',
                                            esc_url(wc_get_cart_remove_url($cart_item_key)),
                                            esc_html__('Remove this item', 'woocommerce'),
                                            esc_attr($product_id),
                                            esc_attr($_product->get_sku())
                                        ),
                                        $cart_item_key
                                    );
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php } ?>
            <?php endforeach;
            $_SESSION['count_of_suppliers'] = $count_of_suppliers; ?>
            <?php do_action('woocommerce_cart_contents'); ?>

            <div class="row">
                <div class="col-md-12">
                    <?php if (wc_coupons_enabled()) { ?>
                        <div class="coupon">
                            <label for="coupon_code"><?php esc_html_e('Coupon:', 'woocommerce'); ?></label><input type="text" name="coupon_code" class="input-text" id="coupon_code" value=""
                                                                                                                  placeholder="<?php esc_attr_e('Coupon code', 'woocommerce'); ?>"/>
                            <button type="submit"
                                    class="button <?php echo esc_attr(wc_wp_theme_get_element_class_name('button') ? ' ' . wc_wp_theme_get_element_class_name('button') : ''); ?>"
                                    name="apply_coupon" value="<?php esc_attr_e('Apply coupon', 'woocommerce'); ?>"><?php esc_attr_e('Apply coupon', 'woocommerce'); ?></button>
                            <?php do_action('woocommerce_cart_coupon'); ?>
                        </div>
                    <?php } ?>

                    <button type="submit"
                            class="button btn btn-primary<?php echo esc_attr(wc_wp_theme_get_element_class_name('button') ? ' ' . wc_wp_theme_get_element_class_name('button') : ''); ?>"
                            style="display: none;" name="update_cart" value="<?php esc_attr_e('Update cart', 'woocommerce'); ?>"><?php esc_html_e('Update cart', 'woocommerce'); ?></button>

                    <?php do_action('woocommerce_cart_actions'); ?>

                    <?php wp_nonce_field('woocommerce-cart', 'woocommerce-cart-nonce'); ?>
                </div>
            </div>


        </form>
        <br>
    </div>
    <div class="col-xl-3 pt-xl-5">
        <div class="card card-shadow mt-1">
            <div class="card-body">
                <?php do_action('woocommerce_before_cart_collaterals'); ?>
                <?php
                /**
                 * Cart collaterals hook.
                 * cart-totals.php (woocommerce/cart/cart-totals.php)
                 * @hooked woocommerce_cross_sell_display
                 * @hooked woocommerce_cart_totals - 10
                 */
                do_action('woocommerce_cart_collaterals');
                ?>
            </div>
        </div>

    </div>
    <div class="col-md-12">
        <?php do_action('woocommerce_after_cart'); ?>
    </div>
</div>

<script>
    jQuery(function ($) {

        $('body').on('change', '.qty', function () { // поле с количеством имеет класс .qty
            $('[name="update_cart"]').trigger('click');
        });

    });
</script>
