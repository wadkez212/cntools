<div class="row">
    <div class="col-md-8">
		<?php if (isset($supplier_data) && !empty($supplier_data)) { ?>
        <h5>ID: <?= $supplier_data->ID ;?></h5>
        <form method="post" action="" role="form">
			<input type="hidden" name="action" value="supplier_filling">
			<input type="hidden" name="supplier_filling" value="<?= wp_create_nonce($current_user->user_login . '_supplier_filling');?>">
            <?php

            if($result)
            {
                foreach($result as $item)
                {
                    // значение поля
                    $value = get_post_meta($supplier_data->ID, $item['key'], true );
                    ?>
                    <div class="mb-3">
                        <label class="form-label" for="<?= $item['key']?>"><?= $item['title']?></label>
                        <input type="text" name="supplier[<?= $item['key']?>]"  id="<?= $item['key']?>"  value="<?= $value?>" class="form-control">
                    </div>
            <?php

                }
            }
            ?>
            <button type="submit" name="update_profile" class="btn btn-primary">Обновить</button>
        </form>
		<?php } else { ?>
			<p>Карточка поставщика не заполнена. Обратитесь к администратору.</p>
		<?php }?>
    </div>
    <div class="col-md-4">

    </div>
</div>