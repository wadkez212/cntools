<?php
?>
<div class="row">
    <div class="col-md-12"></div>
    <div class="col-md-6">
        <div class="card card-hover">
            <div class="card-header">Продажи по месяцам</div>
            <div class="card-body">
                <canvas id="myChart"></canvas>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-hover">
            <div class="card-header">Сумма продаж по месяцам</div>
            <div class="card-body">
                <canvas id="myChartLine"></canvas>
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>
    const ctx = document.getElementById('myChart');
    const ctxLine = document.getElementById('myChartLine');

    new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Январь', 'февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            datasets: [{
                label: ' Кол-во продаж',
                data: [12, 19, 3, 5, 2, 3, 5, 14, 7, 42, 70, 0],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });


    new Chart(ctxLine, {
        type: 'line',
        data: {
            labels: ['Январь', 'февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            datasets: [{
                label: ' Сумма',
                data: [12000, 5600, 8000, 11000, 20000, 3475, 1500, 25000, 1452, 42000, 7000, 0],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script>

