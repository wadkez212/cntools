<?php
$products = $wpdb->get_results( "SELECT * FROM $table_name WHERE supplier_id = {$supplier_data->ID} ORDER BY price ASC" );
?>
    <div class="bg-light small" style="padding: 5px;">
        <a href="#import-template-gen" class="js-gen-product-template"
           data-action="generate_import_template"
           data-title="Добавить товары из из существующих категории"
           data-bs-toggle="modal" data-id="<?= $supplier_data->ID?>"
           data-bs-target="#supplier_modal">Добавить товары из системы</a> |
        <a href="#csv-importer" class="js-get-update-csv"
           data-action="ajax_common_handler"
           data-file="csv-generator"
           data-title="Шаблон обновления товаров"
           data-bs-toggle="modal"
           data-bs-target="#supplier_modal"
           data-id="<?= $supplier_data->ID?>">Шаблон обновления товаров</a> |
        <a href="#csv-importer" class="js-upload-and-update-csv"
           data-action="ajax_common_handler"
           data-file="csv-upload-and-update"
           data-title="Загрузить и обновить товары"
           data-bs-toggle="modal"
           data-bs-target="#supplier_modal"
           data-id="<?= $supplier_data->ID?>">Загрузить и обновить товары</a> |
    </div>


<?php if(!empty($_GET['out']) && $_GET['out'] == 'success'): ?>
    <br>
    <div class="alert alert-success">
        Ваш запрос успешно выполнен!
    </div>
    <br>
<?php endif;?>
        <table class="table table-hover  table-striped">
            <thead>
                <tr>
                    <th><input type="checkbox"></th>
                    <th>Картинка</th>
                    <th style="min-width: 200px;">Имя</th>
                    <th>Бренд</th>
                    <th>Категории</th>
                    <th>Артикул</th>
                    <th>Запасы</th>
                    <th>Цена</th>
                    <th>Цена со скидкой</th>
                    <th>Дата</th>
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($products)): ?>
                <?php foreach($products as $item): ?>
                <?php $categories = get_the_terms( $item->product_id, 'product_cat' ); ?>
                <tr>
                    <td><input type="checkbox"></td>
                    <td>
                        <div class="thumbnail-wrapper">
                        <?php echo  get_the_post_thumbnail($item->product_id, [50, 50], ['class' => 'img-rounded'])?>
                        </div>
                    </td>
                    <td>
                        <?= get_the_title($item->product_id)?>
                        <span class="text text-muted small">
                            <?= $item->unit?>, <?= $item->weight?>x<?= $item->weight?>x<?= $item->length?>x<?= $item->width?>x<?= $item->height?>
                        </span>
                    </td>
                    <td class="small text-success"><?= empty($item->brand) ? '&mdash;' : $item->brand?></td>
                    <td class="small text-muted"><?= join(', ', array_column($categories, 'name'))?></td>
                    <td><?= get_post_meta($item->product_id, '_sku', 1)?></td>
                    <td><input type="number" class="form-control input-sm" name="quantity" value="<?= $item->quantity?>"></td>
                    <td><input type="text" class="form-control input-sm" name="price"  value="<?= $item->price?>"></td>
                    <td><input type="text" class="form-control input-sm" name="price_sale"  value="<?= $item->price_sale?>"></td>
                    <td><?= date('d.m.Y H:i:s', strtotime($item->create_at))?></td>
                </tr>
                <?php endforeach; ?>
                <?php else: ?>
                <tr>
                    <td colspan="100%" class="text text-danger" style="text-align: center">

                            Нет товаров!
                        Для добавления нажмите на &mdash; <strong>"Добавить товары из системы"</strong>
                    </td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
