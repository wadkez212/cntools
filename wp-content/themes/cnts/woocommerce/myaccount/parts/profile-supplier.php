<?php
global $wpdb, $current_user;
$supplier_id = get_current_user_id();
$supplier_data = get_posts( array(
    'meta_key'    => 'supplier_id',
    'meta_value'  => get_current_user_id(),
    'post_type'   => 'suppliers',
) );

$supplier_data = !empty($supplier_data[0]) ? $supplier_data[0] : false;

$out = get_option('cntools-options-supplier-fields');
$result = !empty($out) ? json_decode($out, true) : false;

$endpoints = array_keys(wc_get_account_menu_items());
$content = !empty($_GET['content']) ? $_GET['content'] : 'profile';

$table_name = $wpdb->prefix . 'offers';

$req = sanitize_post($_POST, 'db');
if (empty($current_user)) $current_user = get_userdata(get_current_user_id());
$nonce = $current_user->user_login . '_supplier_filling';

if (isset($req['supplier_filling']) && wp_verify_nonce( $req['supplier_filling'], $nonce )) {
	if (isset($req['supplier']) && !empty( $req['supplier'])) {
		foreach ($req['supplier'] as $field=>$data) {
			$res = update_post_meta($supplier_data->ID, $field, $data);
		}
	}
}


?>
<div class="product-tabs">
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item" role="presentation">
        <a href="?content=profile" class="nav-link <?= $content == 'profile' ? 'active' : ''?>">Профиль</a>
    </li>
    <li class="nav-item" role="presentation">
        <a href="?content=orders" class="nav-link <?= $content == 'orders' ? 'active' : ''?>">Заказы</a>
    </li>
    <li class="nav-item" role="presentation">
        <a href="?content=products" class="nav-link <?= $content == 'products' ? 'active' : ''?>">Товары</a>
    </li>
    <li class="nav-item" role="presentation">
        <a href="?content=statistics" class="nav-link <?= $content == 'statistics' ? 'active' : ''?>">Статистика</a>
    </li>

    <li class="nav-item">
        <a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoints[5] ) ); ?>" class="nav-link"><i class="fa fa-power-off"></i> Выход</a>
    </li>
</ul>
</div>

<?php

require_once 'pages-supplier/' .$content . '.php';

