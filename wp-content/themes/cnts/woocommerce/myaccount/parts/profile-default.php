<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

$endpoints = array_keys(wc_get_account_menu_items());
//dd($endpoints);
$customer_orders = get_posts(
    apply_filters(
        'woocommerce_my_account_my_orders_query',
        array(
            'numberposts' => -1,
            'meta_key'    => '_customer_user',
            'meta_value'  => get_current_user_id(),
            'post_type'   => wc_get_order_types( 'view-orders' ),
            'post_status' => array_keys( wc_get_order_statuses() ),
        )
    )
);
?>
<span class="my-account-wrapper">
    <h1 class="cntools-entry-title">Мой кабинет</h1>
<div class="row">
    <div class="col-md-6 col-xxl-6">
        <div class="card card-shadow">
            <div class="card-body lk-element">
                <div class="lk-left">
                    <h3>Мои заказы</h3>
                    <?php if(!$customer_orders): ?>
                        <p>У вас пока нет заказов</p>
                    <?php else: ?>
                        <p>Кол-во заказов: <?= count($customer_orders)?></p>
                    <?php endif; ?>
                    <a class="btn btn-yellow" href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoints[1] ) ); ?>">Подробнее</a>
                </div>
                <span class="lk-right">
                    <i class="fa fa-boxes"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xxl-6">
        <div class="card card-shadow">
            <div class="card-body lk-element">
                <div class="lk-left">
                    <h3>Мои профиль [<a class="text text-muted" href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoints[5] ) ); ?>">Выход</a>]</h3>
                    <h5 class="name-client"><?= $current_user->last_name?> <?= $current_user->first_name?></h5>
                    <p><?= $current_user->user_email?></p>
                    <p><?= $current_user->billing_phone?></p>
                    <a class="btn btn-yellow" href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoints[4] ) ); ?>">Изменить</a>
                </div>
                <span class="lk-right">
                    <i class="fa fa-id-card"></i>
                </span>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-xxl-6">
        <div class="card card-shadow">
            <div class="card-body lk-element">
                <div class="lk-left">
                    <h3>Мои товары</h3>
                    <?php if(WC()->cart->get_cart_contents_count() == 0) : ?>
                        <p>У вас пока нет заказов</p>
                    <?php else: ?>
                        <p>В корзине ожидает товаров: <?= WC()->cart->get_cart_contents_count()?></p>
                    <?php endif;  ?>
                    <a class="btn btn-yellow" href="/cart/">В корзину</a>
                </div>
                <span class="lk-right">
                    <i class="fa fa-shopping-cart"></i>
                </span>
            </div>
        </div>
    </div>
</div>
</span>
<br>
<br>
<br>
<br>
<br>
