<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.1.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<div class="card card-shadow" style="max-width: 650px; margin: auto;">
    <div class="card-body" style="padding: 40px;">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#login-tab-pane" type="button" role="tab" aria-controls="login-tab-pane" aria-selected="true">Вход</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#reg-tab-pane" type="button" role="tab" aria-controls="reg-tab-pane" aria-selected="false">Регистрация</button>
            </li>
        </ul>

        <div class="tab-content" id="myTabContent" style="margin: 20px;">
            <div class="tab-pane fade show active" id="login-tab-pane" role="tabpanel" aria-labelledby="login-tab" tabindex="0">
                <form action="" method="post">
                    <?php do_action( 'woocommerce_login_form_start' ); ?>
                    <div class="form-group">
                        <label class="form-label">E-mail:</label>
                        <input type="text" name="username"  class="form-control" value="" placeholder="">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Пароль:</label>
                        <input type="password" name="password" class="form-control" value="" placeholder="">
                    </div>
                    <p class="small-text">
                        <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'woocommerce' ); ?></a>
                    </p>
                    <div class="form-group">
                        <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
                        <button class="btn btn-primary" type="submit" name="login"  value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>">Войти</button>
                    </div>
                </form>
            </div>

            <div class="tab-pane fade" id="reg-tab-pane" role="tabpanel" aria-labelledby="reg-tab" tabindex="0">
                <form action="" method="post">
                    <div class="form-group">
                        <label class="form-label">Фамилия:</label>
                        <input type="text" class="form-control"  name="surname" value="" placeholder="">
                    </div>

                    <div class="form-group">
                        <label class="form-label">Имя:</label>
                        <input type="text" class="form-control"  name="name" value="" placeholder="">
                    </div>

                    <div class="form-group">
                        <label class="form-label">Телефон:</label>
                        <input type="text" class="form-control"  name="phone" value="" placeholder="">
                    </div>

                    <div class="form-group">
                        <label class="form-label">E-mail:</label>
                        <input type="text" class="form-control" name="email" value="" placeholder="">
                    </div>

                    <div class="form-group">
                        <label class="form-label">Пароль:</label>
                        <input type="password" class="form-control" name="password" value="" placeholder="">
                    </div>


                    <div class="form-group">
                        <label class="form-label">Повторите пароль:</label>
                        <input type="password" class="form-control" name="password_repete" value="" placeholder="">
                    </div>

                    <div class="form-group">
                        <label class="checkbox-list">
                            <input type="checkbox" name="iur" value="1">
                            <span></span> Я представител юридического лица или ИП
                        </label>
                    </div>

                    <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>

                    <button class="btn btn-primary" type="submit"  name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>">Регистрация</button>

                </form>
            </div>
        </div>
    </div>
</div>

