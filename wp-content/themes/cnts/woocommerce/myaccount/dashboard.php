
<?php $user = wp_get_current_user(); ?>
<?php if(!empty($user->roles[0]) && $user->roles[0] == 'supplier'): ?>
    <?php require_once 'parts/profile-supplier.php'?>
<?php else: ?>
    <?php require_once 'parts/profile-default.php'?>
<?php endif; ?>