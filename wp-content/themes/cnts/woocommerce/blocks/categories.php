<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$current_page = sanitize_post( $GLOBALS['wp_the_query']->get_queried_object() );
//dd($current_page);
//dd($current_page->term_id);


function has_Children($cat_id)
{
    $children = get_terms(
        'product_cat',
        array( 'parent' => $cat_id, 'hide_empty' => false ),
    );
    if ($children){
        return true;
    }
    return false;
}

$parent = !empty($current_page->term_id) ? $current_page->term_id : 0;
if(!has_Children($current_page->term_id)) {
    $parent = $current_page->parent;
}



$categories = get_categories( [
    'taxonomy'     => 'product_cat',
//    'child_of'     => 0,
    'parent'       => $parent,
    'orderby'      => 'term_ID',
    'order'        => 'ASC',
    'hide_empty'   => 1,
//    'hierarchical' => 1,
//    'include'      => '',
    'exclude'      => [23],
//    'number'       => 0,
//    'pad_counts'   => false,
] );

//echo get_cat_ID();
//dd(get_the_ID());
//
//$category = get_category();
//$cat_id = $category->cat_ID;

?>

<div class="row filter">
    <?php foreach ($categories as $item): ?>
    <?php $thumbnail_id = get_term_meta( $item->term_id, 'thumbnail_id', true );?>
    <div class="col-sm-4 col-md-3" style="margin-bottom: 20px;">
        <a class="filter-item <?= $current_page->term_id == $item->term_id ? 'active' : ''?>" href="<?= get_category_link( $item->term_id )?>">
            <div class="row">
                <div class="col-md-3  filter-icon">
                    <?php  if(wp_get_attachment_url( $thumbnail_id )): ?>
                    <img src="<?= wp_get_attachment_url( $thumbnail_id )?>" class="img-fluid" alt="<?= $item->name?>">
                    <?php  endif; ?>
                </div>
                <div class="col-md-9 filter-text">
                    <div class="flex-vertical">
                    <?= $item->name?>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <?php endforeach; ?>
</div>
