<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


$option_key = 'cntools-options-product-specifications';
$out = get_option($option_key);
$results = !empty($out) ? json_decode($out, true): false;

//dd($results);

?>

<div class="row select-filter">
    <div class="col-12"><h2 class="title"><i class="fa fa-filter text text-warning"></i> Отфильтровать по:</h2></div>
    <?php $i = 0;  foreach ($results as $item): ?>
        <div class="col-md-3 <?= $i > 7 ? 'hide-selector-filter' : '' ?>" style="margin-bottom: 10px; ">
            <select name="<?= $item['key']?>" class="form-select">
                <option value=""><?= $item['title']?></option>
            </select>
        </div>
    <?php $i++; endforeach;?>

    <?php if($i > 7):?>
    <div class="col-md-3">
        <button type="button" class="btn btn-yellow js-toggle-selector-filter"
                data-hide="Скрыть фильтров"
                data-show="Фильтров еще"
                style="border-radius: 5px;"><span>Фильтров еще</span> (<?= $i-8?>)</button>
    </div>
    <?php endif; ?>
</div>

