<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


show_admin_bar(false);

define('THEME_VER', '1.0.0');

if (!defined('_S_VERSION')) {
    // Replace the version number of the theme on each release.
    define('_S_VERSION', '1.0.0');
}

add_theme_support('title-tag');


/**
 * @param $key
 * @return object
 */
function Request($key = 'post')
{
    $Request = (object)[
        'post' => $_POST,
        'get' => $_GET,
    ];

    if ($key) {
        $Request = (object)$Request->$key;
    }
    return $Request;
}


/**
 * @param $dir
 * @return void
 */
function cntools_require_funcs($dir)
{
    $scandir = scandir($dir);

    if (!empty($scandir)) {
        foreach ($scandir as $file) {
            if (!in_array($file, ['.', '..']) && !empty($file) && file_exists($dir . $file) && !is_dir($dir . $file)) {
                require $dir . $file;
            }
        }
    }
}


/**
 * Include Cntool theme main functions parts
 */
cntools_require_funcs(get_template_directory() . '/includes/');


/**
 * Include custom meta boxes
 */
cntools_require_funcs(get_template_directory() . '/includes/meta-boxes/');

/**
 * Include custom post types
 */
cntools_require_funcs(get_template_directory() . '/includes/custom-post-types/');

/**
 * Include Woo theme functions parts
 */
cntools_require_funcs(get_template_directory() . '/woocommerce/includes/');


function dd($data)
{
    echo '<pre>';
    var_export($data);
    echo '</pre>';
}


add_action('wp_head', 'custom_ajaxurl');

function custom_ajaxurl()
{

    echo '<script type="text/javascript">
           var front_ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
}

/**
 * @return string
 */
function privacy_policy()
{
    return '/pravovaya-informacziya/';
}


add_action('wp_footer', 'cart_refresh_update_qty');

function cart_refresh_update_qty()
{
    if (is_cart()) :
        ?>
        <script type="text/javascript">
            jQuery('div.woocommerce').on('click', '.plus, .minus', function () {
                jQuery("[name='update_cart']").removeAttr('disabled');
                jQuery("[name='update_cart']").trigger('click');
            });

        </script>
    <?php
    endif;
}

/**
 * Localize app.js
 */
add_action('wp_enqueue_scripts', 'add_my_ajax_scripts');
function add_my_ajax_scripts()
{
    wp_localize_script('cntools-app', 'cart_qty_ajax', array(
            'ajax_url' => admin_url('admin-ajax.php')
        )
    );
}

/**
 * Ajax cart updating
 */
add_action('wp_ajax_qty_cart', 'ajax_qty_cart');
add_action('wp_ajax_nopriv_qty_cart', 'ajax_qty_cart');
function ajax_qty_cart()
{
    $cart_item_key = $_POST['hash'];
    $threeball_product_values = WC()->cart->get_cart_item($cart_item_key);
    $threeball_product_quantity = apply_filters('woocommerce_stock_amount_cart_item', apply_filters('woocommerce_stock_amount', preg_replace("/[^0-9\.]/", '', filter_var($_POST['quantity'], FILTER_SANITIZE_NUMBER_INT))), $cart_item_key);
    $passed_validation = apply_filters('woocommerce_update_cart_validation', true, $cart_item_key, $threeball_product_values, $threeball_product_quantity);

    if ($passed_validation) {
        WC()->cart->set_quantity($cart_item_key, $threeball_product_quantity, true);
    }

    echo do_shortcode('[woocommerce_cart]');
    die();
}

