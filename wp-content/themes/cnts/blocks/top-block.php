<?php
GLOBAL $geo;
$result = getCntoolsModuleOption('cntools-options-producer-working-info');
$default_arr = array_flip(array_filter(array_combine(array_keys($result), array_column($result, 'default'))));
$default_key = $default_arr['yes'];
$default = $result[$default_key];
$geo = empty($geo) ? $default : $geo;

?>
<section class="top">
    <div class="container">
        <div class="flex">
            <div class="flex-inner">
                <ul class="options left-options">
                    <li class="selected-geo">
                        <a href="#"><i class="fas fa-map-marker-alt"></i> Ваш город: <span><?= get_the_title($geo['city_id'])?></span></a>
                        <ul class="geo-selector js-confirm-geo">
                            <li>
                                <p class="confirm-city-text">Вы из города: <span class="selected-city"><?= get_the_title($geo['city_id'])?>?</span></p>
                                <div style="text-align: center">
                                    <button class="cn-button cn-button-grey-sm">Нет</button>
                                    <button class="cn-button cn-button-blue-sm">Да</button>
                                </div>

                            </li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="fas fa-map-marked-alt"></i> Офис: <span><?= $geo['address']?></span></a></li>
                    <li><i class="far fa-clock"></i> <?= $geo['hours']?></li>
                </ul>
            </div>
            <div class="flex-inner align-right">
                <?php
                wp_nav_menu(
                    array(
                        'container'  => '',
                        'menu_class' => 'options right-options',
                        'theme_location' => 'menu-2',
                    )
                );
                ?>
            </div>
        </div>
    </div>
</section>
