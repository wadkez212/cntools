<?php
global $wpdb;
$results = $wpdb->get_results( "SELECT * FROM $wpdb->posts WHERE post_type='product' AND post_status='publish' LIMIT 5");
//var_export($results);
?>

<!--  popular products -->
<section class="section popular-products">
    <h2 class="front-section-title"><?php echo !empty($args['title']) ? $args['title'] : 'Популярные товары' ;?></h2>
    <br>
    <a class="all-popular" href="/shop/?orderby=popularity">Смотреть все</a>
    <div class="container cntools-box-slider-wrapper">
        <div class="cntools-box-slider">
            <?php echo do_shortcode('[products limit="16" columns="4" orderby="popularity" class="owl-carousel" on_sale="true" ]')?>
<!--        <ul class="products">-->
<!--            --><?php //foreach ($results as $item): ?>
<!--            <li class="cntools-box-slider-item item">-->
<!--                <a href="--><?//= get_post_permalink( $item->ID )?><!--">-->
<!--                    <div class="product-img-wrapper">-->
<!--                        <img src="--><?//= get_the_post_thumbnail_url( $item->ID )?><!--" alt="--><?//= $item->post_title?><!--">-->
<!--                    </div>-->
<!--                    <h2>--><?//= $item->post_title?><!--</h2>-->
<!--                    <div class="product-info">-->
<!--                        <div class="product-row">-->
<!--                            <div class="col-in">-->
<!--                                <span class="sale-price">1000 ₽<span class="sale-prc">-17%</span></span>-->
<!--                            </div>-->
<!--                            <div class="col-in">-->
<!--                                <span class="stock-status">В наличии</span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="product-row">-->
<!--                            <div class="col-in price">900 ₽</div>-->
<!--                            <div class="col-in add-form">-->
<!--                                <form action="" method="post" enctype="multipart/form-data">-->
<!--                                    <input type="hidden" name="quantity" value="1">-->
<!--                                    <button type="submit" class="add-cart-button" value="--><?//= $item->ID?><!--" ><span class="fa fa-shopping-cart"></span>В корзину</button>-->
<!--                                </form>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </a>-->
<!--            </li>-->
<!--            --><?php //endforeach; ?>
<!---->
<!--        </ul>-->
        </div>
        <div class="dot-wrapper"></div>
    </div>
</section>