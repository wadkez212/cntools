<?php
 $result = getCntoolsModuleOption('cntools-options-out-company');
?>
<section class="section our-company">
    <span class="overlay-text"><?php echo !empty($args['title']) ? $args['title'] : 'Наша компания' ;?></span>
    <div class="container">
        <div class="oc-wrapper">
            <div class="logo-row">
                <img src="<?= $result['logo']?>">
            </div>
            <div class="our-company-row">
                <div class="row-oc">
                    <div><?= $result['text_under_logo']?></div>
                    <div class="style-15"><?= $result['text_under_text']?></div>
                </div>
                <div class="row-oc">
                    <?= $result['description']?>
                </div>
            </div>

            <?php if($result): ?>
                <div class="flex our-company-info-blocks">
                    <?php foreach($result as $item):?>
                    <?php if(is_array($item)): ?>
                    <div class="flex-inner">
                        <div class="img-wrapper">
                            <img src="<?=$item['icon']?>" alt="<?=$item['name']?>">
                        </div>
                        <?php if (is_numeric($item['line'])): ?>
                        <h3><?=number_format($item['line'], 0, ' ', ' ')?></h3>
                        <?php else: ?>
                            <h3><?= $item['line']?></h3>
                        <?php endif; ?>
                        <p><?=$item['line2']?></p>
                    </div>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </div>

    </div>
</section>
