<?php
$categories = get_categories( [
    'taxonomy'     => 'product_cat',
//    'child_of'     => 0,
    'parent'       => '0',
    'orderby'      => 'term_ID',
    'order'        => 'ASC',
//    'hide_empty'   => 1,
//    'hierarchical' => 1,
    'exclude'      => [22, 23],
//    'include'      => '',
//    'number'       => 0,
//    'pad_counts'   => false,
] );

?>

<section class="section">
    <h2 class="front-section-title"><?php echo !empty($args['title']) ? $args['title'] : 'Выберите нужную категорию' ;?></h2>
    <div class="container">
        <ul class="categories-list">
            <?php foreach ($categories as $item): ?>
            <?php $thumbnail_id = get_term_meta( $item->term_id, 'thumbnail_id', true );?>
                <li>
                    <a href="<?= get_category_link( $item->term_id )?>">
                        <div class="img">
                            <img src="<?= wp_get_attachment_url( $thumbnail_id )?>">
                        </div>
                        <strong><?= $item->name?></strong>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</section>