
<!--  popular brands -->
<section class="section front-contacts">
    <h2 class="front-section-title"><?php echo !empty($args['title']) ? $args['title'] : 'Не удалось решить вопрось?' ;?></h2>
    <span class="left-bg"></span><span class="right-bg"></span>
    <div class="container">
        <div class="flex">
            <div class="flex-inner">
                <h2>Свяжитесь с нами самостоятельно</h2>

                <div class="phones">
                    <div class="title">Отдел по работе с клиентами:</div>
                    <div class="phone"><i class="fa fa-phone"></i> 8 900 324 56 78</div>

                    <div class="title">Отдел технической поддержки:</div>
                    <div class="phone"><i class="fa fa-phone"></i> 8 900 123 45 67</div>
                </div>

                <div class="more-info-block">
                    <span class="icon-info"><span>!</span></span>
                    <h3>Специалисты помогут разобраться в:</h3>
                    <ul class="more-list">
                        <li>Товарном ассортименте</li>
                        <li>Стоимости конкретного товара</li>
                        <li>Порядке совершения заказа</li>
                        <li>Способах оплаты</li>
                        <li>Вариантах доставки</li>
                    </ul>
                </div>
            </div>

            <div class="flex-inner">
                <h2>Или оставьте заявку и мы свяжемся с <br>Вами в течении 10 минут</h2>
                <form class="callback-form">
                    <input class="cntool-element" type="text" name="name" placeholder="Ваще имя">
                    <input class="cntool-element" type="text" name="phone" placeholder="+7 (999) 99-99-99">
                    <div class="form-footer">
                        <button class="button">Оставить заявку</button>
                        <div class="conf-policy">Нажимая кнопку отправить вы соглашаетесь с политикой конфидициа</div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>