<?php
$sliders = get_posts( array(
    'numberposts' => 5,
//    'category'    => 45,
    'orderby'     => 'date',
    'order'       => 'DESC',
    'include'     => array(),
    'exclude'     => array(),
    'meta_key'    => '',
    'meta_value'  =>'',
    'post_type'   => 'slider',
    'suppress_filters' => true,
) );

?>


<div class="slider-wrapper">
    <div class="cntools-slider">
        <?php $i = 0; ?>
        <?php foreach ($sliders as $item): ?>
            <?php
                $slider_theme = !empty(get_post_meta($item->ID, 'slider_theme', true)) ? get_post_meta($item->ID, 'slider_theme', true) : false;
                $url = !empty(get_post_meta($item->ID, 'url', true)) ? get_post_meta($item->ID, 'url', true) : false;
                $line = !empty(get_post_meta($item->ID, 'line', true)) ? get_post_meta($item->ID, 'line', true) : false;
                $line2 = !empty(get_post_meta($item->ID, 'line2', true)) ? get_post_meta($item->ID, 'line2', true) : false;
                $line3 = !empty(get_post_meta($item->ID, 'line3', true)) ? get_post_meta($item->ID, 'line3', true) : false;
            ?>


            <?php if($slider_theme == 'current_image'): ?>
                <div class="cntools-slider-item <?= $i == 0 ? 'active' : ''?>">
                    <img src="<?= get_the_post_thumbnail_url( $item->ID )?>" alt="<?= $item->post_title?>">
                </div>
                <span class="dg-design"></span>
            <?php endif; ?>


            <?php if($slider_theme == 'slide_blue'): ?>
                <div class="cntools-slider-item <?= $i == 0 ? 'active' : ''?>">
                    <img src="<?= get_the_post_thumbnail_url( $item->ID )?>" alt="<?= $item->post_title?>">
                </div>
                <span class="dg-design"></span>
            <?php endif; ?>


            <?php if($slider_theme == 'slide_yellow'): ?>
                <div class="cntools-slider-item <?= $i == 0 ? 'active' : ''?>">
                    <div class="img-wrapper">
                        <img src="<?= get_the_post_thumbnail_url( $item->ID )?>" alt="<?= $item->post_title?>">
                    </div>
                    <span class="dg-design"></span>
                </div>
            <?php endif; ?>


            <?php if($slider_theme == 'slide_grey'): ?>
                <div class="cntools-slider-item <?= $i == 0 ? 'active' : ''?>">
                    <img src="<?= get_the_post_thumbnail_url( $item->ID )?>" alt="<?= $item->post_title?>">
                </div>
            <?php endif; ?>

            <?php $i++ ?>
        <?php endforeach; ?>
        <div class="dot-wrapper"></div>
    </div>
</div>