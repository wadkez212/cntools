<?php
$result = getCntoolsModuleOption('cntools-options-mini-banners');
$count = count($result);
$width = 100/$count;

?>

<?php if($result): ?>
    <div class="flex banners">
        <?php foreach($result as $item):?>
            <?php if($item['style'] == 'bg_image_2' || $item['style'] == 'bg_image_blue' || $item['style'] == 'bg_image_grey'): ?>
                <a href="<?= $item['url']?>" class="flex-inner banner <?=$item['style']?>">
                    <p class="banner-text line-1"><?= $item['line']?></p>
<!--                    --><?php //if(!empty($item['line2'])): ?><!--<p class="banner-text line-2">--><?//= $item['line2']?><!--</p>--><?php //endif; ?>
<!--                    --><?php //if(!empty($item['line3'])): ?><!--<p class="banner-text line-3">--><?//= $item['line3']?><!--</p>--><?php //endif; ?>
<!--                    --><?php //if(!empty($item['line4'])): ?><!--<p class="banner-text line-4">--><?//= $item['line4']?><!--</p>--><?php //endif; ?>

                    <span class="bg-img-wrapper"><img src="<?= $item['icon']?>"></span>
                </a>
            <?php endif;?>

            <?php if($item['style'] == 'bg_gradient_blue'): ?>
                <a href="<?= $item['url']?>" class="flex-inner banner <?=$item['style']?>">
                    <p class="banner-text line-1"><?= $item['line']?></p>
                    <?php if(!empty($item['line2'])): ?><p class="banner-text line-2"><?= $item['line2']?></p><?php endif; ?>
                    <?php if(!empty($item['line3'])): ?><p class="banner-text line-3"><?= $item['line3']?></p><?php endif; ?>
                    <?php if(!empty($item['line4'])): ?><p class="banner-text line-4"><?= $item['line4']?></p><?php endif; ?>

                    <span class="bg-img-wrapper"><img src="<?= $item['icon']?>"></span>
                    <span class="circle"></span>
                </a>
            <?php endif;?>


        <?php endforeach; ?>
    </div>
<?php endif; ?>
