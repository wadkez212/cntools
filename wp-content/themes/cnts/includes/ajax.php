<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


/**
 * cntools_ajax_common_handler
 */
add_action( 'wp_ajax_ajax_common_handler', 'cntools_ajax_common_handler' );
add_action( 'wp_ajax_nopriv_ajax_common_handler', 'cntools_ajax_common_handler' );
function cntools_ajax_common_handler() {
    $Request = Request();
    if(empty($Request->file)) return false;
    $filename = get_template_directory() . '/template-parts/ajax-handlers/' . $Request->file . '.php';
    if(file_exists($filename)) {
        include $filename;
    }
    wp_die();
}









/**
 * cntools_checker
 */
add_action( 'wp_ajax_checker', 'cntools_checker' );
function cntools_checker() {
    $result = [];
    $orders = get_posts( array(
        'post_type'   => 'shop_order',
        'post_status' => 'wc-on-hold',
    ) );

    $result['data'] = false;
    if (!empty($orders)) {
        $result['status'] = 'orders';
        foreach ($orders as $item)
        {
            $item_meta = get_post_meta($item->ID, 'notification', 1);

            if($item_meta == '' || empty($item_meta)) {
                $result['data'][] = [
                    'order_num' => '#' . $item->ID,
                    'url' => '/wp-admin/post.php?action=edit&post=' . $item->ID
                ];
                add_post_meta($item->ID, 'notification', 1);
            }
        }
    }

    if (empty($result['data']))  $result['status'] = 'empty';

    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    wp_die();
}

/**
 * cntools_local_pickup_2
 */
add_action( 'wp_ajax_local_pickup_2', 'cntools_local_pickup_2' );
add_action( 'wp_ajax_nopriv_local_pickup_2', 'cntools_local_pickup_2' );
function cntools_local_pickup_2() {
    include get_template_directory() . '/woocommerce/checkout/steps/get-methods/self-service.php';
    wp_die();
}


/**
 * cntools_flat_rate_3
 */
add_action( 'wp_ajax_flat_rate_3', 'cntools_flat_rate_3' );
add_action( 'wp_ajax_nopriv_flat_rate_3', 'cntools_flat_rate_3' );
function cntools_flat_rate_3() {
    include get_template_directory() . '/woocommerce/checkout/steps/get-methods/delivery.php';
    wp_die();
}


add_action( 'wp_ajax_flat_rate_4', 'cntools_flat_rate_4' );
add_action( 'wp_ajax_nopriv_flat_rate_4', 'cntools_flat_rate_4' );
function cntools_flat_rate_4() {
    include get_template_directory() . '/woocommerce/checkout/steps/get-methods/by-transport-company.php';
    wp_die();
}


/**
 * cntools_get_categories
 */
add_action( 'wp_ajax_get_categories', 'cntools_get_categories' );
add_action( 'wp_ajax_nopriv_get_categories', 'cntools_get_categories' );
function cntools_get_categories() {
    include get_template_directory() . '/template-parts/get-categories.php';
    wp_die();
}


/**
 * cntools_add_in_cart
 */
add_action( 'wp_ajax_add_in_cart', 'cntools_add_in_cart' );
add_action( 'wp_ajax_nopriv_add_in_cart', 'cntools_add_in_cart' );
function cntools_add_in_cart() {
    $id = WC()->cart->add_to_cart( $_POST['product_id'], $_POST['quantity'], 0, array(), array( 'offer_price' => $_POST['price'], 'supplier_id' =>  $_POST['supplier_id'] ) );

    echo json_encode([
        'status' => 'success',
        'cart_item_id' => $id
    ], JSON_UNESCAPED_UNICODE);

    wp_die();
}



/**
 * cntools_get_city_list
 */
add_action( 'wp_ajax_get_city_list', 'cntools_get_city_list' );
add_action( 'wp_ajax_nopriv_get_city_list', 'cntools_get_city_list' );
function cntools_get_city_list() {
    include get_template_directory() . '/template-parts/inc-get-city-list.php';
    wp_die();
}

/**
 * cntools_callback_order
 */
add_action( 'wp_ajax_callback_order', 'cntools_callback_order' );
add_action( 'wp_ajax_nopriv_callback_order', 'cntools_callback_order' );
function cntools_callback_order() {
    include get_template_directory() . '/template-parts/inc-callback-order.php';
    wp_die();
}


/**
 * cntools_callback_order_handler
 */
add_action( 'wp_ajax_callback_order_handler', 'cntools_callback_order_handler' );
add_action( 'wp_ajax_nopriv_callback_order_handler', 'cntools_callback_order_handler' );
function cntools_callback_order_handler() {
    if(empty($_POST['phone']) || empty($_POST['name'])) {
        echo 'error';
    } else {
        $post_data = array(
            'post_title'    => sanitize_text_field( $_POST['name'] ) . ', ' . sanitize_text_field( $_POST['phone'] ),
            'post_content'  => sanitize_text_field( $_POST['name'] ) . ', ' . sanitize_text_field( $_POST['phone'] ),
            'post_status'   => 'draft',
            'post_author'   => 1,
            'post_type' => 'callback',
        );

// Вставляем запись в базу данных
        $post_id = wp_insert_post( $post_data );
        echo 'ok';
    }
    wp_die();
}


/**
 * cntools_subscribe_email
 */
add_action( 'wp_ajax_subscribe_email', 'cntools_subscribe_email' );
add_action( 'wp_ajax_nopriv_subscribe_email', 'cntools_subscribe_email' );
function cntools_subscribe_email() {
    if(empty($_POST['email'])) {
        echo 'error';
    } else {
        echo 'ok';
    }
    wp_die();
}





/**
 * cntools_view_offers_list
 */
add_action( 'wp_ajax_view_offers_list', 'cntools_view_offers_list' );
add_action( 'wp_ajax_nopriv_view_offers_list', 'cntools_view_offers_list' );
function cntools_view_offers_list() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'offers';
    include get_template_directory() . '/template-parts/inc-offers-list.php';
    wp_die();
}



/**
 * cntools_subscribe_on_notification
 */
add_action( 'wp_ajax_subscribe_on_notification', 'cntools_subscribe_on_notification' );
add_action( 'wp_ajax_nopriv_subscribe_on_notification', 'cntools_subscribe_on_notification' );
function cntools_subscribe_on_notification() {
    include get_template_directory() . '/template-parts/inc-subscribe-on-notification.php';
    wp_die();
}

/**
 * cntools_subscribe_on_notification
 */
add_action( 'wp_ajax_generate_import_template', 'cntools_generate_import_template' );
add_action( 'wp_ajax_nopriv_generate_import_template', 'cntools_generate_import_template' );
function cntools_generate_import_template() {
    include get_template_directory() . '/template-parts/inc-generate-import-template.php';
    wp_die();
}

/**
 * cntools_add_to_supplier_list_selected_products
 */
add_action( 'wp_ajax_add_to_supplier_list_selected_products', 'cntools_add_to_supplier_list_selected_products' );
add_action( 'wp_ajax_nopriv_add_to_supplier_list_selected_products', 'cntools_add_to_supplier_list_selected_products' );
function cntools_add_to_supplier_list_selected_products() {
    include get_template_directory() . '/template-parts/inc-add-to-supplier-list-selected-products.php';
    wp_die();
}

/**
 * cntools_generate_csv
 */
add_action( 'wp_ajax_generate_csv', 'cntools_generate_csv' );
add_action( 'wp_ajax_nopriv_generate_csv', 'cntools_generate_csv' );
function cntools_generate_csv() {
    include get_template_directory() . '/template-parts/inc-generate-csv.php';
    wp_die();
}


/**
 * cntools_subscribe_notification_handler
 */
add_action( 'wp_ajax_subscribe_notification_handler', 'cntools_subscribe_notification_handler' );
add_action( 'wp_ajax_nopriv_subscribe_notification_handler', 'cntools_subscribe_notification_handler' );
function cntools_subscribe_notification_handler() {
    if(empty($_POST['phone']) || empty($_POST['name'])) {
        echo 'error';
    } else {
        echo 'ok';
    }
    wp_die();
}

/**
 * cntools_send_request_become_partner
 */
add_action( 'wp_ajax_send_request_become_partner', 'cntools_send_request_become_partner' );
add_action( 'wp_ajax_nopriv_send_request_become_partner', 'cntools_send_request_become_partner' );
function cntools_send_request_become_partner() {
    if(empty($_POST['phone']) || empty($_POST['name'])) {
        if(empty($_POST['phone'])) {
            echo 'phone';
        }
        if(empty($_POST['name'])) {
            echo 'name';
        }

    } else {
        $post_data = array(
            'post_title'    => sanitize_text_field( $_POST['name'] ) . ', ' . sanitize_text_field( $_POST['phone'] ),
            'post_content'  => sanitize_text_field( $_POST['name'] ) . ', ' . sanitize_text_field( $_POST['phone'] ),
            'post_status'   => 'draft',
            'post_author'   => 1,
            'post_type' => 'partnership',
        );

// Вставляем запись в базу данных
        $post_id = wp_insert_post( $post_data );
        echo 'ok';
    }
    wp_die();
}




/**
 * cntools_remove_from_cart
 */
add_action( 'wp_ajax_remove_from_cart', 'cntools_remove_from_cart' );
add_action( 'wp_ajax_nopriv_remove_from_cart', 'cntools_remove_from_cart' );
function cntools_remove_from_cart() {
    $result = WC()->cart->remove_cart_item( $_POST['cart_item_id'] );

    echo json_encode([
        'status' => 'success',
        'result' => $result
    ], JSON_UNESCAPED_UNICODE);

    wp_die();
}


add_action( 'woocommerce_before_calculate_totals', 'rudr_custom_price_refresh' );

function rudr_custom_price_refresh( $cart_object ) {

    foreach ( $cart_object->get_cart() as $item ) {

        if( array_key_exists( 'offer_price', $item ) ) {
            $item[ 'data' ]->set_price( $item[ 'offer_price' ] );
        }

    }

}


/**
 * cntools_generate_payment
 */
add_action( 'wp_ajax_generate_payment', 'cntools_generate_payment' );
function cntools_generate_payment() {

    if(isset($_POST['target']))
    {

        global $wpdb;
        $table_name = $wpdb->prefix . 'supplier_orders';
        $sberform = $wpdb->get_row( "SELECT * FROM $table_name WHERE order_id = {$_POST['post_id']} LIMIT 1" );

        $url = $sberform->formUrl;

        if( in_array($_POST['target'], ['sb_online', 'send_invoice']))
        {
             $billing_email = get_post_meta($_POST['post_id'], 'billing_email', 1);

             $headers = array(
                 'From: [CNTOOLS.RU] <payment@cntools.ru>',
                 'content-type: text/html'
             );


             $out = wp_mail( $billing_email, '[CNTOOLS.RU] - Оплата заказа', 'Для оплататы заказа пройдите поссылке <a href="' . $url . '">Ссылка</a>', $headers );

             if($out)
             {

                $order = new WC_Order($_POST['post_id']);

                if (!empty($order)) {

                    $order->update_status( 'wc-pending' );

                }

                 echo '<span style="color: green;">Успешно отправлено!</span>';
             }
             else
             {
                 echo '<span style="color: red;">При отправке произошла ошибка!</span>';
             }
        }
    }

    wp_die();
}


/**
 * cntools_get_statistics
 */
add_action( 'wp_ajax_get_statistics', 'cntools_get_statistics' );
function cntools_get_statistics() {
    include get_template_directory() . '/blocks/statistics.php';
    wp_die();
}



/**
 * cntools_send_unpublish_all
 */
add_action( 'wp_ajax_send_unpublish_all', 'cntools_send_unpublish_all' );
function cntools_send_unpublish_all() {
    if(!empty($_POST['post_type'])) {
        $records = get_posts([
            'post_type' => $_POST['post_type'],
            'posts_per_page' => -1,
            'post_status' => 'publish'
        ]);

        if(!empty($records)) {
            foreach ($records as $item) {
                wp_update_post([
                    'ID' => $item->ID,
                    'post_status' => 'draft'
                ]);
            }
        }
    }

    wp_die();
}



/**
 * cntools_send_unpublish_all
 */
add_action( 'wp_ajax_send_trash_all', 'cntools_send_trash_all' );
function cntools_send_trash_all() {
    if(!empty($_POST['post_type'])) {
        $records = get_posts([
            'post_type' => $_POST['post_type'],
            'posts_per_page' => -1,
            'post_status' => 'publish'
        ]);

        if(!empty($records)) {
            foreach ($records as $item) {
                wp_update_post([
                    'ID' => $item->ID,
                    'post_status' => 'trash'
                ]);
            }
        }
    }

    wp_die();
}



/**
 * cntools_get_statistics
 */
add_action( 'wp_ajax_import_by_csv', 'cntools_import_by_csv' );
function cntools_import_by_csv() {
    $all_titles = get_product_properties_all_titles();

    foreach ($all_titles as $key => $value)
    {
        $all_keys[ trim($value) ] = trim($key);
    }

    $csvFile = file($_POST['file_url']);
    $count_prods = count($csvFile);
    $data = [];
    foreach ($csvFile as $line) {
        $data[] = str_getcsv($line);
    }

    $title = $data[0];
    unset($data[0]);
    $insert_args = [];
    $args = [];
    foreach ($data as $key => $arr)
    {
        foreach ($arr as $n => $item)
        {
            if(!empty($all_keys[$title[$n]])) {
                $field_key = $all_keys[$title[$n]];
                $field_value = $item;

                if($field_key == 'post_title')
                {
                    $args[$key]['product']['post_title'] = $field_value;
                }
                else if($field_key == 'post_excerpt')
                {
                    $args[$key]['product']['post_excerpt'] = $field_value;
                }

                else if($field_key == 'post_content')
                {
                    $args[$key]['product']['post_content'] = $field_value;
                }
                else
                {

                    $args[$key]['post_meta'][$field_key] = $field_value;
                }
            }

        }
    }

    $insert_data = $args;

//    dd($insert_data);

    if(!empty($insert_data)) {
          foreach ($insert_data as $data)
          {

              if(!empty($data['product']['post_content']) || !empty($data['product']['post_excerpt']))
              $post_data = array(
                  'post_title'    => sanitize_text_field( $data['product']['post_title']),
                  'post_content'  => $data['product']['post_content'],
                  'post_excerpt'  => $data['product']['post_excerpt'],
                  'post_status'   => 'publish',
                  'post_type'   => 'product',
//                  'post_category' => 18
              );

              $post_id = wp_insert_post( $post_data );

              if($post_id)
              {
                  foreach($data['post_meta'] as $meta_key => $meta_value)
                  {
                      add_post_meta($post_id, $meta_key, $meta_value);
                  }
              }
          }
    }



    echo $count_prods;
    wp_die();
}


/**
 *  cntools_parse_from_ocean_biz_ua
 */
add_action( 'wp_ajax_parse_from_ocean_biz_ua', 'cntools_parse_from_ocean_biz_ua' );
function cntools_parse_from_ocean_biz_ua() {

    if(empty($_POST['page_url'])) return false;
    $page_url = $_POST['page_url'];

    $option_key = 'cntools-options-product-specifications';
    $specifications = json_decode(get_option($option_key), true);
    $title = array_column($specifications, 'title');
    $keys = array_column($specifications, 'key');
    $title_keys = array_combine($title, $keys);
    $keys_title = array_combine($keys, $title);


    $result = parsePage($page_url);


    $post_data = array(
        'post_title'    => sanitize_text_field( $result['title'] ),
        'post_content'  => $result['description'],
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_type' => 'product',
    );

// Вставляем запись в базу данных
    $post_id = wp_insert_post( $post_data );

    wp_set_post_terms( $post_id, [18], 'product_cat', false );


    add_post_meta($post_id, '_sku', $result['prop_values'][1]);
    add_post_meta($post_id, '_price', 1);

    unset($result['prop_title'][1]);

    foreach ($result['prop_title'] as $n => $title) {

        $check_title = $title;
        if(!in_array($check_title, $keys_title)) {
            $filed_key = 'cn_' . codeGen(7);
            $specifications[] = [
                'title' =>  $check_title,
                'key' => $filed_key,
            ];
            update_option($option_key, json_encode($specifications, JSON_UNESCAPED_UNICODE));
        } else {
            $filed_key = $title_keys[$check_title];
        }

        # add meta
        add_post_meta($post_id, $filed_key, $result['prop_values'][$n]);

    }


    $file = file_get_contents($result['img']);

    $filename = wp_upload_dir()['path'] . '/cntools_' . bin2hex(random_bytes(10)) . '.jpg';

    file_put_contents($filename, $file);


// Check the post type that we use in the 'post_mime_type' field.
    $filetype = wp_check_filetype( basename( $filename ), null );

// Get the path to the uploads directory.
    $wp_upload_dir = wp_upload_dir();

// Prepare the array with the necessary data for the nesting.
    $attachment = array(
        'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ),
        'post_mime_type' => $filetype['type'],
        'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
        'post_content'   => $post_data['post_title'],
        'post_status'    => 'inherit'
    );

// Insert the attachment into the database.
    $attach_id = wp_insert_attachment( $attachment, $filename, $post_id );
    set_post_thumbnail($post_id, $attach_id);

    echo 'refresh';

    wp_die();
}

add_action( 'wp_ajax_remove_offer', 'cntools_remove_offer' );
function cntools_remove_offer() {
	global $wpdb;

	$req = sanitize_post($_POST, 'db');

	if ( ! isset( $req['offer'] ) || empty($req['offer']))  {
		return wp_send_json_error( 'Ошибка удаления' );
		wp_die();
	}

	$table_name = $wpdb->prefix . 'offers';

	$res = $wpdb->delete(
		$table_name,
		[
			'id' => (int) $req['offer']
		],
		'%d'
	);

	if (false === $res) {

		return wp_send_json_error( 'Ошибка удаления' );

	} elseif (0 === $res) {

		return wp_send_json_success( 'Элемент удален' );

	} else {

		return wp_send_json_success( 'Элемент удален' );
	}

	wp_die();
}


/**
 * cntools_add_supplier
 */
add_action( 'wp_ajax_add_supplier', 'cntools_add_supplier' );
function cntools_add_supplier() {
    global $wpdb;

    if ( ! isset( $_POST['product_id'] ) || empty($_POST['product_id']) || ! isset( $_POST['supplier_id'] ) || empty($_POST['supplier_id']))  {
		return wp_send_json_error( 'Ошибка добавления' );
		wp_die();
	}

    $table_name = $wpdb->prefix . 'offers';
    $res_add = $wpdb->insert(
        $table_name,
        [
            'product_id' => (int) $_POST['product_id'],
            'supplier_id' => (int) $_POST['supplier_id'],
            'price' => (int) $_POST['price'],
            'price_sale' => (int) $_POST['price_sale'],
            'quantity' => (int) $_POST['quantity'],
            'create_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s'),
        ],
		[
			'%d'
			, '%d'
			, '%f'
			, '%f'
			, '%d'
			, '%s'
			, '%s'
		]
    );

    //echo $out;

/*	$search = [
		'supplier_id'
		, 'post_title'
		, 'price'
		, 'price_sale'
		, 'quantity'
	];

	$search = array_map(function ($field) {
		return "%{$field}%";
	}, $search);*/

	ob_start(); ?>
	<tr id="offer-%id%">
		<td>%supplier_id% <input type="hidden" id="%supplier_id%" name="suppliers[%supplier_id%][id]" value="%supplier_id%"></td>
		<td><a href="post.php?post=%supplier_id%&action=edit" target="_blank">%post_title%</a></td>
		<td>
			<input type="text" name="suppliers[%supplier_id%][price]" value="%price%">
		</td>
		<td>
			<input type="text" name="suppliers[%supplier_id%][price_sale]" value="%price_sale%">
		</td>
		<td>
			<input type="text" name="suppliers[%supplier_id%][quantity]" value="%quantity%">
		</td>
		<td align="right">
			<button class="button restore-backup js-send-action" type="button" data-offer="%id%" data-action="remove_offer">Удалить</button>
		</td>
	</tr>
	<?php $data = ob_get_clean();

	if ($res_add) {

		$out = $wpdb->get_results(
			$wpdb->prepare( "
            SELECT * FROM {$table_name}
            WHERE id = %d
            ORDER BY id ASC
            LIMIT 1
            ;
            ",
				$wpdb->insert_id
			)
		);

		if ($out && isset($out[0])) {

			$str = [];
			$out_replace = [];

			foreach ($out as $el_key=>&$elem) {
				$supplier = get_post((int) $elem->supplier_id);
				$elem->post_title = $supplier->post_title;
				foreach ($elem as $key=>$val) {
					$out_replace[$el_key]['%' . $key . '%'] = $val;
				}
				$str[$el_key] = str_replace(array_keys($out_replace[$el_key]), array_values($out_replace[$el_key]), $data);
			}

			return wp_send_json_success( $str);

		} else {

			return wp_send_json_error( 'Ошибка добавления' );
		}

	} else {

		return wp_send_json_error( 'Ошибка добавления' );
	}


    wp_die();
}




/**
 * cntools_calc_self_service_price
 */
add_action( 'wp_ajax_calc_self_service_price', 'cntools_calc_self_service_price' );
add_action( 'wp_ajax_calc_self_service_price', 'cntools_calc_self_service_price' );

function cntools_calc_self_service_price() {
    $cdek = new Public\CdekConnection();
    $cart =  WC()->cart->get_cart();

    $prices_arr = [];
    foreach ($cart as $item) {

        $sender_post_code = get_post_meta($item['supplier_id'], 'cn_5405871', true);
        $receiver_post_code = $_POST['get_pcode'];
        $data= [
            'product_id' => $item['product_id'],
            'supplier_id' => $item['supplier_id'],
            'weight' => (float)str_replace(',', '.', get_post_meta($item['product_id'], 'cn_5038353', true)),
            'length' => (float)str_replace(',', '.', get_post_meta($item['product_id'], 'cn_1208621', true)),
            'width'  => (float)str_replace(',', '.', get_post_meta($item['product_id'], 'cn_1997515', true)),
            'height' => (float)str_replace(',', '.', get_post_meta($item['product_id'], 'cn_5725330', true)),
        ];

        $prices_arr[] = $cdek->getTarif($sender_post_code, $receiver_post_code, $data);
    }

    $delivery_info = [];
    foreach($prices_arr as $item){
        $city_id = get_post_meta($item['supplier_id'], 'supplier_city_id', 1);



        $delivery_info[] = [
            'product_title' => '<a href="' . get_the_permalink($item['product_id']) . '" target="_blank">' . get_the_title($item['product_id']) . '</a>',
            'start' =>  get_term( $city_id, 'cdek_pvz_cat' )->name,
            'end' => get_geo()['name'],
            'price' => $item['price'],
        ];
    }



    $result = [];
    if(!empty($prices_arr)) {
        $prices = array_column($prices_arr, 'price');
        $min = array_column($prices_arr, 'min');
        $max = array_column($prices_arr, 'max');
        $min_max = array_merge($min, $max);

        $result = [
            'delivery_info' => $delivery_info,
            'total_price' => array_sum($prices),
            'delivery_date' => date('d.m.Y', time() + max($min_max)*60*60*24),
        ];

    }

    echo json_encode($result, JSON_UNESCAPED_UNICODE);

    wp_die();
}