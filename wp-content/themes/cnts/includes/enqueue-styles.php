<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}



add_action( 'wp_enqueue_scripts', 'cntools_styles' );
function cntools_styles() {
    wp_enqueue_style( 'cntools-fontawesome-all', get_template_directory_uri() . '/assets/fontawesome/css/all.css', array(), _S_VERSION );
    wp_enqueue_style( 'cntools-bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css', array(), _S_VERSION );
    wp_enqueue_style('cntools-main-style', get_template_directory_uri() . '/assets/css2/main.css', array() , THEME_VER, 'all');
    wp_enqueue_style( 'goo-fancybox-style', get_template_directory_uri() . '/assets/libs/fancybox/fancybox.css', array(), THEME_VER, 'all' );
    wp_enqueue_style( 'goo-fancybox-panzoom-style', get_template_directory_uri() . '/assets/libs/fancybox/panzoom.css', array(), THEME_VER, 'all' );
    wp_enqueue_style( 'cntools-animate-style', get_template_directory_uri() . '/assets/css/animate.min.css', array(), THEME_VER, 'all' );
    wp_enqueue_style( 'cntools-bootstrap-datepicker', get_template_directory_uri() . '/assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.standalone.min.css', array(), THEME_VER, 'all' );
    wp_enqueue_style( 'cntools-Chart', get_template_directory_uri() . '/assets/libs/chart/Chart.bundle.js', array(), THEME_VER, 'all' );
	
//     wp_enqueue_style( 'cntools-bootstrap-theme', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap-theme.css', array(), _S_VERSION );

//    wp_enqueue_style( 'cntools-cntools-slider-style', get_template_directory_uri() . '/assets/css/cntools-slider.css', array(), _S_VERSION );
//    
//    wp_enqueue_style( 'cntools-cntools-owl-style', get_template_directory_uri() . '/assets/css/owl.theme.default.css', array(), _S_VERSION );

//    wp_enqueue_style( 'goo-owl-style', get_template_directory_uri() . '/assets/libs/OwlCarousel2/assets/owl.theme.default.css', array(), THEME_VER, 'all' );

//    wp_enqueue_style( 'cntools-shop-style', get_template_directory_uri() . '/assets/css/shop.css', array(), THEME_VER, 'all' );
//    
//    wp_enqueue_style( 'cntools-select2-style', get_template_directory_uri() . '/assets/libs/select2/css/select2.min.css', array(), THEME_VER, 'all' );
}