<?php

## Добавляем блоки в основную колонку на страницах постов и пост. страниц
add_action('add_meta_boxes_page', 'cntools_city_add_custom_box');
function cntools_city_add_custom_box()
{

    global $post;
    if(!empty($post))
    {
        $pageTemplate = get_post_meta($post->ID, '_wp_page_template', true);

        if($pageTemplate == 'templates/page-city.php' )
        {
            $screens = array( 'page');
            add_meta_box( 'cntools_city_id', 'Самовывоз:', 'cntools_city_meta_box_callback', $screens, 'advanced', 'high' );
        }
    }
}

// HTML код блока
function cntools_city_meta_box_callback( $post, $meta )
{
    $screens = $meta['args'];

    // Используем nonce для верификации
//    wp_nonce_field( plugin_basename(__FILE__), 'cntools_city_id' );

    $value = get_post_meta($post->ID, 'city', 1);
    $coords = !empty($value['coords']) ? $value['coords'] : '';

    if(empty($value['point']))
    {
        $value = [
            'point' => [
                'address' => [0 => '',],
                'coords'  => [0 => '',],
                'price'   => [0 => '',],
            ],
        ];
    }
    else
    {
        unset($value['point']['address'][0]);
        unset($value['point']['coords'][0]);
        unset($value['point']['price'][0]);
    }

    $point = !empty($value['point']) ? $value['point'] : '';



    ?>
    <div class="element-wrapper">
        <label for="coords">Коордынаты Центра города: <a href="https://yandex.ru/map-constructor/location-tool/?from=club" target="_blank">определене координат</a></label>
        <input type="text" id="coords" name="city[coords]" placeholder="Шырота, долгата" value="<?= $coords?>">
    </div>

    <table style="display: none">
        <tbody class="js-tr-line-block" >
            <tr>
            <td><input type="text" style="width: 100%;" id="" name="city[point][address][]" placeholder="Адрес" value=""></td>
            <td><input type="text" style="width: 100%;" id="" name="city[point][coords][]" placeholder="Шырота, долгата" value=""></td>
            <td><input type="text" style="width: 100%;" id="" name="city[point][price][]" placeholder="стоймость" value=""></td>
            <td style="width: 100px;"><a href="#delete">Удалить</a></td>
            </tr>
        </tbody>
    </table>

    <div class="address-wrapper" style="margin: 0px 15px 15px 15px;">
        <label for="">Адреса: </label>
        <?php if (!empty($point['address'])): ?>
        <table class="wp-list-table widefat fixed striped table-view-list pages">
            <tbody class="js-line-container">
            <?php foreach ($point['address'] as $key => $item): ?>
            <tr>
                <td><input type="text" style="width: 100%;" id="" name="city[point][address][]" placeholder="Адрес" value="<?= $item?>"></td>
                <td><input type="text" style="width: 100%;" id="" name="city[point][coords][]" placeholder="Шырота, долгата" value="<?= $point['coords'][$key]?>"></td>
                <td><input type="text" style="width: 100%;" id="" name="city[point][price][]" placeholder="стоймость" value="<?= $point['price'][$key]?>"></td>
                <td style="width: 100px;"><a href="#delete" class="js-delete-tr">Удалить</a></td>
            </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php endif; ?>

        <div class="button button-small button-secondary js-add-line" data-target=".js-line-container" data-line=".js-tr-line-block" style="float: right; margin: 15px 0 0 0">Добавить еще</div>
        <br>
        <br>
        <br>
    </div>

<?php

}

## Сохраняем данные, когда пост сохраняется
add_action( 'save_post', 'cntools_city_save_postdata' );
function cntools_city_save_postdata( $post_id ) {


    if(!empty($_POST))
    {
        foreach($_POST as $key => $value)
        {
//            // Убедимся что поле установлено.
//            if ( ! isset( $_POST[$key] ) )
//                return;
//
//            // проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
//            if ( ! wp_verify_nonce( $_POST[$key], plugin_basename(__FILE__) ) )
//                return;
//
//            // если это автосохранение ничего не делаем
//            if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
//                return;
//
//            // проверяем права юзера
//            if( ! current_user_can( 'edit_post', $post_id ) )
//                return;

            // Все ОК. Теперь, нужно найти и сохранить данные
            // Очищаем значение поля input.
            if(!empty($value))
            {
                $data = sanitize_text_field( $value );


                // Обновляем данные в базе данных.
                update_post_meta( $post_id, $key, $value );
            }
        }
    }


}