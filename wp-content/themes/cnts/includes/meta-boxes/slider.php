<?php

## Добавляем блоки в основную колонку на страницах постов и пост. страниц
add_action('add_meta_boxes', 'slider_add_custom_box');
function slider_add_custom_box()
{
    $screens = array( 'slider');
    add_meta_box( 'slider_id', 'Параметры слайда', 'slider_meta_box_callback', $screens, 'advanced', 'high' );
}

// HTML код блока
function slider_meta_box_callback( $post, $meta )
{
    $screens = $meta['args'];

    // Используем nonce для верификации
//    wp_nonce_field( plugin_basename(__FILE__), 'slider_id' );
    ;
?>
    <div class="element-wrapper">
        <input type="text" name="url" value="<?= get_post_meta($post->ID, 'url', true)?>" placeholder="Ссылка">
    </div>
    <div class="element-wrapper">
        <input type="text" name="line" value="<?= get_post_meta($post->ID, 'line', true)?>" placeholder="Первая линия текста">
    </div>

    <div class="element-wrapper">
        <input type="text" name="line2" value="<?= get_post_meta($post->ID, 'line2', true)?>" placeholder="Вторая линия текста">
    </div>

    <div class="element-wrapper">
        <input type="text" name="line3" value="<?= get_post_meta($post->ID, 'line3', true)?>" placeholder="Третья линия текста">
    </div>

    <?= get_post_meta($post->ID, 'url', true) == 'current_image' ? 'selected="selected"' : '' ?>
    <div class="element-wrapper">
        <select name="slider_theme">
            <option value="">&mdash; Выбрать стиль &mdash;</option>
            <option value="current_image" <?= get_post_meta($post->ID, 'slider_theme', true) == 'current_image' ? 'selected="selected"' : '' ?>>Только текущее изображеие</option>
            <option value="slide_blue" <?= get_post_meta($post->ID, 'slider_theme', true) == 'slide_blue' ? 'selected="selected"' : '' ?>>Синяя тема</option>
            <option value="slide_yellow" <?= get_post_meta($post->ID, 'slider_theme', true) == 'slide_yellow' ? 'selected="selected"' : '' ?>>Желтая тема</option>
            <option value="slide_grey" <?= get_post_meta($post->ID, 'slider_theme', true) == 'slide_grey' ? 'selected="selected"' : '' ?>>Серая тема</option>
        </select>
    </div>

<?php
}

## Сохраняем данные, когда пост сохраняется
add_action( 'save_post', 'slider_save_postdata' );
function slider_save_postdata( $post_id ) {

    foreach ($_POST as $key => $item) {
        // Убедимся что поле установлено.
        if ( ! isset( $_POST[$key] ) )
            return;

        // проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
//            if ( ! wp_verify_nonce( $_POST[$key], plugin_basename(__FILE__) ) )
//                return;
//
//            // если это автосохранение ничего не делаем
//            if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
//                return;
//
//            // проверяем права юзера
//            if( ! current_user_can( 'edit_post', $post_id ) )
//                return;

        // Все ОК. Теперь, нужно найти и сохранить данные
        // Очищаем значение поля input.
        $data = sanitize_text_field( $_POST[$key] );


        // Обновляем данные в базе данных.
        update_post_meta( $post_id, $key, $data );
    }

}