<?php

## Добавляем блоки в основную колонку на страницах постов и пост. страниц
add_action('add_meta_boxes', 'invoicing_add_custom_box');
function invoicing_add_custom_box()
{
    $screens = array( 'shop_order');
    add_meta_box( 'invoicing_id', 'Отпрака инвойса', 'invoicing_meta_box_callback', $screens, 'side', 'high' );
}

// HTML код блока
function invoicing_meta_box_callback( $post, $meta )
{
    $screens = $meta['args'];
    ?>
    <span class="js-msg-result"></span>
    <button type="button" class="button-pay js-send-payment" data-target="sb_online" data-id="<?=  $post->ID?>">Оплата через Сбербанк онлайн</button>
    <button type="button" class="button-pay js-send-payment" data-target="send_invoice" data-id="<?=  $post->ID?>">Выставить счет на перевод</button>
<?php
//
//    // Используем nonce для верификации
////    wp_nonce_field( plugin_basename(__FILE__), 'invoicing_id' );
//
//
//    $out = get_option('cntools-options-supplier-fields');
//    $result = !empty($out) ? json_decode($out, true) : false;
//
//    if($result)
//    {
//        foreach($result as $item)
//        {
//            // значение поля
//            $value = get_post_meta( $post->ID, $item['key'], 1 );
//
//            echo '<div class="element-wrapper">';
//            echo '<label for="' . $item['key'] . '">' . $item['title'] . ': </label> ';
//            echo '<input type="text" id="' . $item['key'] . '" name="' . $item['key'] . '" value="'. $value .'"/>';
//            echo '</div>';
//        }
//    }

}

## Сохраняем данные, когда пост сохраняется
add_action( 'save_post', 'invoicing_save_postdata' );
function invoicing_save_postdata( $post_id ) {

//    $out = get_option('cntools-options-supplier-fields');
//    $result = !empty($out) ? json_decode($out, true) : false;
//
//
//    if($result)
//    {
//        foreach($result as $item)
//        {
//            $key = $item['key'];
////            // Убедимся что поле установлено.
////            if ( ! isset( $_POST[$key] ) )
////                return;
////
////            // проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
////            if ( ! wp_verify_nonce( $_POST[$key], plugin_basename(__FILE__) ) )
////                return;
////
////            // если это автосохранение ничего не делаем
////            if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
////                return;
////
////            // проверяем права юзера
////            if( ! current_user_can( 'edit_post', $post_id ) )
////                return;
//
//            // Все ОК. Теперь, нужно найти и сохранить данные
//            // Очищаем значение поля input.
//            if(!empty($_POST[$key] ))
//            {
//                $data = sanitize_text_field( $_POST[$key] );
//
//
//                // Обновляем данные в базе данных.
//                update_post_meta( $post_id, $key, $data );
//            }
//
//        }
//    }


}