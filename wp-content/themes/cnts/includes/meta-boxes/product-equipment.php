<?php

## Добавляем блоки в основную колонку на страницах постов и пост. страниц
add_action('add_meta_boxes', 'product_equipment_add_custom_box');
function product_equipment_add_custom_box()
{
    $screens = array( 'product');
    add_meta_box( 'product_equipment_id', 'Комплектация', 'product_equipment_meta_box_callback', $screens, 'side', 'default' );
}

// HTML код блока
function product_equipment_meta_box_callback( $post, $meta )
{
    $screens = $meta['args'];

    // Используем nonce для верификации
//    wp_nonce_field( plugin_basename(__FILE__), 'product_equipment_id' );


    $out = get_option('cntools-options-product-equipment');
    $result = !empty($out) ? json_decode($out, true) : false;

    if($result)
    {
        foreach($result as $item)
        {
            // значение поля
            $value = get_post_meta( $post->ID, $item['key'], 1 );
            $checked = !empty($value) && $value == 1 ? 'checked="checked"' : '';
            echo '<div class="element-wrapper_">';
            echo '<input type="checkbox" id="' . $item['key'] . '" name="' . $item['key'] . '" value="1" '. $checked . '/> &mdash; '. $item['title'];
            echo '</div>';
        }
    }

}

## Сохраняем данные, когда пост сохраняется
add_action( 'save_post', 'product_equipment_save_postdata' );
function product_equipment_save_postdata( $post_id ) {

    $out = get_option('cntools-options-product-equipment');
    $result = !empty($out) ? json_decode($out, true) : false;


    if($result)
    {
        foreach($result as $item)
        {
            $key = $item['key'];
//            // Убедимся что поле установлено.
//            if ( ! isset( $_POST[$key] ) )
//                return;
//
//            // проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
//            if ( ! wp_verify_nonce( $_POST[$key], plugin_basename(__FILE__) ) )
//                return;
//
//            // если это автосохранение ничего не делаем
//            if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
//                return;
//
//            // проверяем права юзера
//            if( ! current_user_can( 'edit_post', $post_id ) )
//                return;

            // Все ОК. Теперь, нужно найти и сохранить данные
            // Очищаем значение поля input.
            if(!empty($_POST[$key] ))
            {
                $data = sanitize_text_field( $_POST[$key] );

                // Обновляем данные в базе данных.
                update_post_meta( $post_id, $key, $data );
            }
        }
    }


}