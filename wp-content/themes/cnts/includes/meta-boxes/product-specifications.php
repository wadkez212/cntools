<?php

## Добавляем блоки в основную колонку на страницах постов и пост. страниц
add_action('add_meta_boxes', 'product_specifications_add_custom_box');
function product_specifications_add_custom_box()
{
    $screens = array( 'product');
    add_meta_box( 'product_specifications_id', 'Технические характеристики', 'product_specifications_meta_box_callback', $screens, 'advanced', 'high' );
}

// HTML код блока
function product_specifications_meta_box_callback( $post, $meta )
{
    $screens = $meta['args'];

    // Используем nonce для верификации
//    wp_nonce_field( plugin_basename(__FILE__), 'product_specifications_id' );


    $out = get_option('cntools-options-product-specifications');
    $result = !empty($out) ? json_decode($out, true) : false;

    if($result)
    {
        foreach($result as $item)
        {
            // значение поля
            $value = get_post_meta( $post->ID, $item['key'], 1 );

            echo '<div class="element-wrapper">';
            echo '<label for="' . $item['key'] . '">' . $item['title'] . ': </label> ';
            echo '<input type="text" id="' . $item['key'] . '" name="' . $item['key'] . '" value="'. $value .'"/>';
            echo '</div>';
        }
    }

}

## Сохраняем данные, когда пост сохраняется
add_action( 'save_post', 'product_specifications_save_postdata' );
function product_specifications_save_postdata( $post_id ) {

    $out = get_option('cntools-options-product-specifications');
    $result = !empty($out) ? json_decode($out, true) : false;


    if($result)
    {
        foreach($result as $item)
        {
            $key = $item['key'];
//            // Убедимся что поле установлено.
//            if ( ! isset( $_POST[$key] ) )
//                return;
//
//            // проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
//            if ( ! wp_verify_nonce( $_POST[$key], plugin_basename(__FILE__) ) )
//                return;
//
//            // если это автосохранение ничего не делаем
//            if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
//                return;
//
//            // проверяем права юзера
//            if( ! current_user_can( 'edit_post', $post_id ) )
//                return;

            // Все ОК. Теперь, нужно найти и сохранить данные
            // Очищаем значение поля input.
            if(!empty($_POST[$key] ))
            {
                $data = sanitize_text_field( $_POST[$key] );


                // Обновляем данные в базе данных.
                update_post_meta( $post_id, $key, $data );
            }
        }
    }


}