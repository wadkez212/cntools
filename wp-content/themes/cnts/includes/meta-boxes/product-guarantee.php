<?php

## Добавляем блоки в основную колонку на страницах постов и пост. страниц
add_action('add_meta_boxes', 'product_guarantee_add_custom_box');
function product_guarantee_add_custom_box()
{
    $screens = array( 'product');
    add_meta_box( 'product_guarantee_id', 'Гарантия в месяцах', 'product_guarantee_meta_box_callback', $screens, 'side', 'high' );
}

// HTML код блока
function product_guarantee_meta_box_callback( $post, $meta )
{
    $screens = $meta['args'];

    // Используем nonce для верификации
//    wp_nonce_field( plugin_basename(__FILE__), 'product_guarantee_id' );

    $out = get_option('cntools-common-settings');
    $result = !empty($out) ? json_decode($out, true) : false;

    $value = get_post_meta( $post->ID, 'guarantee', 1 );

    echo '<div class="element-wrapper">';
    echo '<select name="guarantee" style="width: 100%; ">';
    echo '<option value="">&mdash; Выбрать &mdash;</option>';
    for($i = 1; $i <= $result['guarantee']; $i++)
    {

        $selected = !empty($value) && $value == $i ? 'selected="selected"' : '';
        echo '<option ' . $selected . '  value="' . $i . '">' . $i . '</option>';
    }
    echo '</select>';
    echo '</div>';


}

## Сохраняем данные, когда пост сохраняется
add_action( 'save_post', 'product_guarantee_save_postdata' );
function product_guarantee_save_postdata( $post_id ) {
    $key = 'guarantee';
    // Убедимся что поле установлено.
            if ( ! isset( $_POST[$key] ) )
                return;

            // проверяем nonce нашей страницы, потому что save_post может быть вызван с другого места.
//            if ( ! wp_verify_nonce( $_POST[$key], plugin_basename(__FILE__) ) )
//                return;
//
//            // если это автосохранение ничего не делаем
//            if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
//                return;
//
//            // проверяем права юзера
//            if( ! current_user_can( 'edit_post', $post_id ) )
//                return;

            // Все ОК. Теперь, нужно найти и сохранить данные
            // Очищаем значение поля input.
            $data = sanitize_text_field( $_POST[$key] );


            // Обновляем данные в базе данных.
            update_post_meta( $post_id, $key, $data );
}