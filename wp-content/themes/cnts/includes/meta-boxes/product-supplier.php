<?php

## Добавляем блоки в основную колонку на страницах постов и пост. страниц
add_action('add_meta_boxes', 'product_supplier_add_custom_box');
function product_supplier_add_custom_box()
{
    $screens = array( 'product');
    add_meta_box( 'product_supplier_id', 'Предложения по товару', 'product_supplier_meta_box_callback', $screens, 'advanced', 'high' );
}

// HTML код блока
function product_supplier_meta_box_callback( $post, $meta )
{
    $screens = $meta['args'];

    // Используем nonce для верификации
//    wp_nonce_field( plugin_basename(__FILE__), 'product_supplier_id' );

    $suppliers = get_posts([
       'post_type' => 'suppliers'
    ]);

    global $wpdb;
    $table_name = $wpdb->prefix . "offers";
    $offers = $wpdb->get_results( "SELECT * FROM $table_name WHERE product_id = {$post->ID}" );

    $suppliers_arr = [];
    foreach ($suppliers as $supplier) {
        $suppliers_arr[$supplier->ID] = $supplier;
    }
    ?>
    <table class="wp-list-table widefat striped table-view-list">
        <thead>
            <tr>
                <th>ID</th>
                <th>Поставщик</th>
                <th>Цена</th>
                <th>Цена со скидкой</th>
                <th>Наличие</th>
                <th></th>
            </tr>
        </thead>
        <tbody id="js-supplier-container">
		<?php if (!empty($offers)) { ?>
			<?php foreach ($offers as $item) { ?>
				<?php if (isset($item->supplier_id) && 0 !== (int)$item->supplier_id) { ?>
					<tr id="offer-<?= $item->id ;?>">
						<td><?= $item->supplier_id; ?> <input type="hidden" id="<?= $item->supplier_id; ?>" name="suppliers[<?= $item->supplier_id; ?>][id]" value="<?= $item->supplier_id; ?>"></td>
						<td><a href="post.php?post=<?= $item->supplier_id; ?>&action=edit" target="_blank"><?= $suppliers_arr[$item->supplier_id]->post_title; ?></a></td>
						<td>
							<input type="text" name="suppliers[<?= $item->supplier_id ?>][price]" value="<?= $item->price ?>">
						</td>
						<td>
							<input type="text" name="suppliers[<?= $item->supplier_id ?>][price_sale]" value="<?= $item->price_sale ?>">
						</td>
						<td>
							<input type="text" name="suppliers[<?= $item->supplier_id ?>][quantity]" value="<?= $item->quantity ?>">
						</td>
						<td align="right">
							<button class="button restore-backup js-send-action" type="button" data-offer="<?= $item->id ;?>" data-action="remove_offer">Удалить</button>
						</td>
					</tr>
				<?php } else { ?>
					<tr>
						<td colspan="5">Ошибки в предложении с id <?= $item->id; ?>. Пропускаем</td>
						<td align="right">
							<button class="button restore-backup js-send-action" type="button" data-offer="<?= $item->id ;?>" data-action="remove_offer">Удалить</button>
						</td>
					</tr>
				<?php } ?>
			<?php } ?>
		<?php } else { ?>
			<tr>
				<td colspan="6" style="text-align:  center; color: red; font-size: 14px;">Пока нет предложений по
					товару
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
	<table>
		<tbody>
        <tr>
            <td colspan="6">
                <input type="hidden" name="product_id" class="js-supplier-field" value="<?= get_the_ID()?>">
                <div class="element-wrapper">
                    <label for="cn_7124320">Поставщик: </label>
                    <select name="supplier_id" class="js-supplier-field">
                        <?php
                        $suppliers = get_posts([
                            'post_type' => 'suppliers'
                        ]);
                        ?>
                        <option value="">&mdash; Выбрать &mdash;</option>
                        <?php foreach ($suppliers as $supplier) { ?>
							<?php if (isset($supplier->ID) && $supplier->post_title) { ?>
                            <option value="<?= $supplier->ID ;?>"><?= $supplier->post_title ;?></option>
							<?php } ?>
                        <?php };?>
                    </select>
                </div>
                <div class="element-wrapper"><label for="price">Цена: </label>
					<input type="text" id="price" name="price" class="js-supplier-field" value="">
				</div>
                <div class="element-wrapper"><label for="price_sale">Цена со скидкой: </label>
					<input type="text" id="price_sale" class="js-supplier-field" name="price_sale" value="">
				</div>
                <div class="element-wrapper"><label for="quantity">Наличие: </label>
					<input type="text" id="quantity" name="quantity"class="js-supplier-field" value="">
				</div>
                <div class="element-wrapper">
                    <button type="button" class="button restore-backup js-send-action" data-action="add_supplier" data-serialize=".js-supplier-field" data-rescontainer="#js-supplier-container">Сохранить предложение</button>
                </div>
            </td>
        </tr>
        </tbody>
    </table>

<?php


}

## Сохраняем данные, когда пост сохраняется
add_action( 'save_post', 'product_supplier_save_postdata' );
function product_supplier_save_postdata( $post_id ) {
    $key = 'suppliers';
    global $wpdb;

    if ( ! isset( $_POST[$key] ) )  return;

    $table_name = $wpdb->prefix . 'offers';

    foreach ($_POST[$key] as $id => $item)
    {
        if(!empty($item['price'])) {

            $result = $wpdb->update($table_name, [
                'price' => $item['price'],
                'price_sale' => $item['price_sale'],
                'quantity' => $item['quantity'],
                'update_at' => date('Y-m-d H:i:s'),
            ], [
                'product_id' => $post_id,
                'supplier_id' => $item['id'],
            ]);

            if ($result === false || $result < 1) {
                $wpdb->insert(
                    $table_name,
                    [
                        'product_id' => $post_id,
                        'supplier_id' => $item['id'],
                        'price' => $item['price'],
                        'price_sale' => $item['price_sale'],
                        'quantity' => $item['quantity'],
                        'create_at' => date('Y-m-d H:i:s'),
                        'update_at' => date('Y-m-d H:i:s'),
                    ]
                );
            }

//            WC()->cart->add_to_cart( 14, 1, 0, array(), array( 'misha_custom_price' => 1000 ) );



//            update_post_meta( $post_id, 'supplier_id_' . $id, $id );
//            update_post_meta( $post_id, 'supplier_price_'. $id, $item['price'] );
//            update_post_meta( $post_id, 'supplier_price_sale_' . $id, $item['price_sale'] );
        }
    }



}