<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// This theme uses wp_nav_menu() in one location.
register_nav_menus(
    array(
        'menu-1' => esc_html__( 'Primary', 'cntools' ),
        'menu-2' => esc_html__( 'Top-menu', 'cntools' ),
        'footer-menu-1' => esc_html__( 'О компании', 'cntools' ),
        'footer-menu-2' => esc_html__( 'Акции', 'cntools' ),
        'footer-menu-3' => esc_html__( 'Получение и оплата', 'cntools' ),
        'footer-menu-4' => esc_html__( 'Сервис поддержка', 'cntools' ),
    )
);