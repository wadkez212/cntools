<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function cntools_cats_on_main()
{
    $cntools_option_key = 'cntools-options-cats-on-main';
    $out = get_option($cntools_option_key);
    $result = !empty($out) ? json_decode($out, true): false;

    if(!empty($result)){
        echo '<ul class="fast-url">';
        foreach ($result as  $item)
        {
            ?>

            <li><a href="<?= get_category_link( $item['cat_id'] )?>" style="background: url('<?= $item['icon']?>') left center no-repeat;"><span><?= get_product_cat_name($item['cat_id']) ?></span></a></li>
<?php
        }
        echo '</ul>';
    }
}


function get_product_cat_name( $cat_id ) {
    $cat_id   = (int) $cat_id;
    $category = get_term( $cat_id, 'product_cat' );

    if ( ! $category || is_wp_error( $category ) ) {
        return '';
    }

    return $category->name;
}


/**
 * getCntoolsModuleOption
 * @param $option_key
 * @return false|mixed
 */
function getCntoolsModuleOption($option_key) {
    $out = get_option($option_key);
    return !empty($out) ? json_decode($out, true) : false;
}


/**
 * getDefaultOptionValue
 * @param $data
 * @return mixed|void
 */
function getDefaultOptionValue($data = false)
{
    foreach ($data as $key => $value)
    {
        if($key == 'default') {
            return $value;
            break;
        }
    }
}




function my_admin_footer_function() {
 ?>
<!--    <div class="sounds-wrapper" style="display: none;">-->
<!--        <audio id="new-order"><source src="--><?//= get_template_directory_uri()?><!--/assets/sounds/new_order.mp3" type="audio/mpeg"></audio>-->
<!--        <audio id="new-message"><source src="--><?//= get_template_directory_uri()?><!--/assets/sounds/new_message.mp3" type="audio/mpeg"></audio>-->
<!--        <audio id="new-message-2"><source src="--><?//= get_template_directory_uri()?><!--/assets/sounds/new_message_2.mp3" type="audio/mpeg"></audio>-->
<!--    </div>-->
    <?php
}
add_action('admin_footer', 'my_admin_footer_function');



add_action('admin_head-edit.php','cn_additional_actions');
function cn_additional_actions() {
    global $current_screen;
    if ('product' != $current_screen->post_type) return;
    ?>
    <script type="text/javascript">
        jQuery(document).ready( function()
        {
            jQuery(".wrap .subsubsub").append('<li class="byorder"> | <a href="#upublish" class="js-send-action" data-action="send_unpublish_all" data-confirmation="Вы уверены что хотите продолжить?" data-type="<?= $current_screen->post_type?>" style="color: #c74545;">Отправить все товары в черновики</a></li>');
            let Block = `<div class="parse_ocaen_block" style="padding: 10px; background: #d4d4d7; margin-bottom: 5px;">
                            <input type="text" name="page_url"  class="page_url" placeholder="URL страницы товара">
                            <div class="button js-send-action" data-action="parse_from_ocean_biz_ua" data-serialize=".page_url" data-refresh="back">Парсить товар с ocean.biz.ua</a>
                        </div>`;
            jQuery(".wp-header-end").after(Block);

        });
    </script>
    <?php
}


add_action('admin_head-edit.php','cn_additional_actions_2');
function cn_additional_actions_2() {
    global $current_screen;
    if ('cdek_pvz' != $current_screen->post_type) return;
    ?>
    <script type="text/javascript">
        jQuery(document).ready( function()
        {
            jQuery(".wrap .subsubsub").append('<li class="byorder"> | <a href="#upublish" class="js-send-action" data-action="send_trash_all" data-confirmation="Вы уверены что хотите продолжить?" data-type="<?= $current_screen->post_type?>" style="color: #c74545;">Отправить все в корзину</a></li>');

        });
    </script>
    <?php
}


/**
 * get_geo
 * @return void
 */
function get_geo() {
    $current_city_id = !empty($_COOKIE['cntools_current_geo']) ? $_COOKIE['cntools_current_geo'] : false;
    $result = getCntoolsModuleOption('cntools-options-producer-working-info');

    $default_arr = array_flip(array_filter(array_combine(array_keys($result), array_column($result, 'default'))));
    $default_key = $default_arr['yes'];
    $default = $result[$default_key];
    $term = get_term($default['city_id']);
    $default['city_code'] = $term->slug;
    $default['name'] = $term->name;



    if(!empty($current_city_id)) {
        $city_ids_arr = array_flip(array_filter(array_combine(array_keys($result), array_column($result, 'city_id'))));
        $geo = !empty($city_ids_arr[$current_city_id]) && !empty($result[$city_ids_arr[$current_city_id]]) ? $result[$city_ids_arr[$current_city_id]] : false;


        if(!$geo && get_term($current_city_id)) {
            $term = get_term($current_city_id);
            $geo = [
                'city_id' => $term->term_id,
                'name' => $term->name,
                'city_code' => $term->slug,
                'count' => $term->count,
                'default' => 'no',
                'slogan' => $default['slogan'],
                'address' => $default['address'],
                'hours' => $default['hours'],
                'phone' => $default['phone'],
                'phone_additional' => $default['phone_additional'],
                'coords' => $default['coords'],
                'pcode' => get_post_meta($current_city_id, 'PostalCode', 1),
            ];


        }

        if($geo) {
            $term = get_term($current_city_id);
            $geo['city_code'] = $term->slug;
            $geo['name'] = $term->name;
        }
    }

    $geo = empty($geo) ? $default : $geo;

    $geo['slogan'] = str_replace('{city}',  get_term($geo['city_id'])->name, $geo['slogan']);

    return $geo;
}


/**
 * @return int[]|WP_Post[]
 */
function getPvzs() {
    $result = get_posts([
        'post_type'   => 'cdek_pvz',
        'numberposts' => -1,
        'tax_query' =>
            array (
                0 =>
                    array (
                        'taxonomy' => 'cdek_pvz_cat',
                        'field' => 'term_taxonomy_id',
                        'terms' => get_geo()['city_id'],
                    ),
            ),
    ]);

    return $result;
}

