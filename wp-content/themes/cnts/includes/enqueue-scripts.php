<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


add_action( 'wp_enqueue_scripts', 'cntools_scripts' );
function cntools_scripts() {

    wp_localize_script( 'cnts-frontajax', 'frontajax', [ 'ajaxurl' => admin_url( 'admin-ajax.php' ) ]);

   // wp_enqueue_script( 'goonline-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), THEME_VER, true );
//    wp_localize_script( 'ajax-script', 'cntools_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

    wp_enqueue_script( 'cntools-bootstrap', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js', array(), THEME_VER, true );
    wp_enqueue_script( 'cntools-app', get_template_directory_uri() . '/assets/js/app.js', array(), THEME_VER, true );
//    wp_enqueue_script( 'cntools-owl', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array(), THEME_VER, true );
    wp_enqueue_script( 'cntools-fancybox', get_template_directory_uri() . '/assets/libs/fancybox/fancybox.umd.js', array('jquery'), THEME_VER, true );
//    wp_enqueue_script( 'cntools-owl', get_template_directory_uri() . '/assets/libs/OwlCarousel2/owl.carousel.min.js', array('jquery'), THEME_VER, true );
//    wp_enqueue_script( 'cntools-select2', get_template_directory_uri() . '/assets/libs/select2/js/select2.full.min.js', array('jquery'), THEME_VER, true );
    wp_enqueue_script( 'cntools-bootstrap-datepicker', get_template_directory_uri() . '/assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js', array('jquery'), THEME_VER, true );
    wp_enqueue_script( 'cntools-JsLocalSearch', get_template_directory_uri() . '/assets/libs/JsLocalSearch.js', array('jquery'), THEME_VER, true );




}