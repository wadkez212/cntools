<?php

function custom_post_type_cdek_pvz() {
    
    $labels = array(
        'name'                => _x( 'Управление СДЕК ПВЗ', 'Post Type General Name', 'cntools' ),
        'singular_name'       => _x( 'СДЕК ПВЗ', 'Post Type Singular Name', 'cntools' ),
        'menu_name'           => __( 'СДЕК ПВЗ', 'cntools' ),
        'parent_item_colon'   => __( 'Parent ', 'cntools' ),
        'all_items'           => __( 'Все ПВЗ', 'cntools' ),
        'view_item'           => __( 'Просмотр', 'cntools' ),
        'add_new_item'        => __( 'Добавить новую', 'cntools' ),
        'add_new'             => __( 'Добавить', 'cntools' ),
        'edit_item'           => __( 'Редактировать', 'cntools' ),
        'update_item'         => __( 'Обновить', 'cntools' ),
        'search_items'        => __( 'Поиск', 'cntools' ),
        'not_found'           => __( 'Не смогли найти', 'cntools' ),
        'not_found_in_trash'  => __( 'Не смогли найти', 'cntools' ),
    );

// Set other options for Custom Post Type

    $args = array(
        'label'               => __( 'СДЕК ПВЗ', 'cntools' ),
        'description'         => __( 'СДЕК ПВЗ', 'cntools' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'thumbnail','templates', 'custom-fields' ), // 'excerpt'  // 'excerpt', //'custom-fields', //  'author', // 'editor',  'revisions',
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'cdek_pvz_cat' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 6,
        'menu_icon'           => 'dashicons-store',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,

    );

    // Registering your Custom Post Type
    register_post_type( 'cdek_pvz', $args );

}

add_action( 'init', 'custom_post_type_cdek_pvz', 0 );


add_action( 'init', 'create_cdek_pvz_cat_hierarchical_taxonomy', 0 );
function create_cdek_pvz_cat_hierarchical_taxonomy() {

    $labels = array(
        'name' => _x( 'Регионы и города', 'taxonomy general name' ),
        'singular_name' => _x( 'Регионы и города', 'taxonomy singular name' ),
        'search_items' =>  __( 'Пойск' ),
        'all_items' => __( 'Все' ),
//        'parent_item' => __( 'მშობელი კატეგორია' ),
//        'parent_item_colon' => __( 'მშობელი კატეგორია:' ),
        'edit_item' => __( 'Редактировать' ),
        'update_item' => __( 'Обновить' ),
        'add_new_item' => __( 'Новая' ),
        'new_item_name' => __( 'Название новой записи' ),
        'menu_name' => __( 'Регионы и города' ),
    );

    register_taxonomy('cdek_pvz_cat', array('cdek_pvz'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_in_rest' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'cdek_pvz_cat' ),
    ));

}