<?php

/**
 * @return string[]
 */
function get_product_properties_all_titles()
{
    $product_options_keys = [
        'cntools-options-product-specifications',
        'cntools-options-product-equipment',
        'cntools-options-product-product-packing-info',
        'cntools-options-product-benefits-list',
    ];

    $out = [];
    $title = [
        '_sku' => 'Артикул',
        'post_title' => 'Имя',
         'post_excerpt' => 'Краткое описание',
         'post_content' => 'Описание',
         '_stock' => 'Запасы',
        '_price' => 'Цена',
        '_sale_price' => 'Цена после скидки',
        'Категории',
        'supplier' => 'Поставшик',
        'country' => 'Страна производитель',
        'guarantee' => 'Гарантия в месяцах',
        '_thumbnail_id' => 'Ссылки на изображения'
    ];
    foreach ($product_options_keys as $option_key)
    {
        $result = getCntoolsModuleOption($option_key);
        $titles_arr = array_column($result, 'title');
        $keys_arr = array_column($result, 'key');
        $combine = array_combine($keys_arr, $titles_arr);

        foreach($combine as $key => $item)
        {
//            array_push($title, $item);
            $title[$key] = $item;
        }

    }

    return $title;
}


/**
 * @param $list
 * @return void
 */
function create_csv($list = [], $title = false)
{

    $fp = fopen(wp_get_upload_dir()['path'] . '/' . $title . '.csv', 'w'); // wp-content/uploads/2022/09/


    foreach ($list as $fields)
    {
        fputcsv($fp, $fields);
    }

    fclose($fp);
}