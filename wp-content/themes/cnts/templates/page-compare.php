<?php
/**
 *  Template Name: Страница сравнения
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CNTOOLS
 */


get_header();
?>

    <main class="container">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<nav class="woocommerce-breadcrumb">','</nav>' );
        }
        ?>
        <?php the_title( '<h1 class="cntools-entry-title">', '</h1>' ); ?>
        <div class="card card-shadow">
            <div class="card-body">
        <?php if(!empty($_COOKIE['cntools_compare'])): ?>
        <?php
            global $wpdb;
            $table_name = $wpdb->prefix . "offers";

        $option_key = 'cntools-options-product-specifications';
        $out = get_option($option_key);
        $options_arr = !empty($out) ? json_decode($out, true): false;
        $product_ids_arr = explode(',', $_COOKIE['cntools_compare']);

        ?>

        <table class="table table-sm table-hover table-borderless table-striped">
            <tbody>
            <tr>
                <td style=" font-weight: 700">Изображение</td>
                <?php foreach ($product_ids_arr as $id): ?>
                    <td>
                        <img src="<?= get_the_post_thumbnail_url($id)?>" class="img-fluid" style="width: 100px;" alt="<?= get_the_title($id)?>">
                    </td>
                <?php endforeach; ?>
            </tr>
            <tr>
                <td style=" font-weight: 700">Название</td>
                <?php foreach ($product_ids_arr as $id): ?>
                    <td style=" font-weight: 700"><a href="<?= get_permalink($id);?>"><?=get_the_title($id)?></a></td>
                <?php endforeach; ?>
            </tr>
            <tr>
                <td style=" font-weight: 700">Предложении</td>
                <?php foreach ($product_ids_arr as $id): ?>
                    <?php $offer_info = $wpdb->get_results( "SELECT count('ID') as quan FROM $table_name WHERE product_id = $id LIMIT 1" );?>
                    <td><?= $offer_info[0]->quan?></td>
                <?php endforeach; ?>
            </tr>
            <tr>
                <td style=" font-weight: 700">Цена от</td>
                <?php foreach ($product_ids_arr as $id): ?>
                    <?php $offer_info = $wpdb->get_results( "SELECT MIN(price) as price FROM $table_name WHERE product_id = $id LIMIT 1" );?>
                    <td><?= empty($offer_info[0]->price) ? '&mdash;' : $offer_info[0]->price?></td>
                <?php endforeach; ?>
            </tr>
                <?php
                    foreach ($options_arr as $item):
                    $title = $item['title'];
                    $key = $item['key'];
                ?>
                <tr>
                    <td style=" font-weight: 700"><?= $title?></td>
                    <?php foreach ($product_ids_arr as $id): ?>
                    <?php  $value = get_post_meta($id, $key, true); ?>
                        <td><?= empty($value) ? '&mdash;' : $value?></td>
                    <?php endforeach; ?>
                </tr>
                    <?php endforeach; ?>

            </tbody>
        </table>
        <?php else: ?>
        <p>В списке на сравненине нет товаров</p>
        <?php endif; ?>
            </div>
        </div>
        <br>
        <br>
        <br>
    </main><!-- #main -->

<?php
get_footer();