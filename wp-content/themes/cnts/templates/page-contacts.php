<?php
/**
 *  Template Name: Страница Контактов
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CNTOOLS
 */


get_header();
$geo = get_geo();
?>

    <main class="container">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<nav class="woocommerce-breadcrumb">','</nav>' );
        }
        ?>

        <?php the_title( '<h1 class="cntools-entry-title">', '</h1>' ); ?>
        <div class="cnts-entry" style="margin-bottom: 60px;">
            <div class="row properties">
                <div class="col-md-12">
                    <div class="card card-shadow" style="margin-bottom:20px;">
                        <div class="card-body">
                            <ul class="options">
                                <li class="width100prc"><span class="title">Телефон:</span><span class="value"><?= $geo['phone']?></span></li>
                                <li class="width100prc"><span class="title">Аддрес:</span><span class="value"><?= $geo['address']?></span></li>
                                <li class="width100prc"><span class="title">Часы работы:</span><span class="value"><?= $geo['hours']?></span></li>
                            </ul>
                        </div>
                    </div>

                </div>
                <div class="col-md-12">
                    <br>
                    <h2>Пункты выдачи заказов</h2>
                    <?php

                    $pvzs = get_posts([
                        'post_type'   => 'cdek_pvz',
                        'numberposts' => -1,
                        'tax_query' =>
                            array (
                                0 =>
                                    array (
                                        'taxonomy' => 'cdek_pvz_cat',
                                        'field' => 'term_taxonomy_id',
                                        'terms' => $geo['city_id'],
                                    ),
                            ),
                    ]);

                    ?>
                    <div class="row">
                        <?php foreach ($pvzs as $item): ?>
                            <div class="col-xxl-<?= !empty($geo['count']) && $geo['count'] == 1 ? '12' : '6'?>">
                                <div class="card card-shadow" style="margin-bottom:20px;">
                                    <div class="card-body">
                                        <strong><a target="_blank" href="<?= get_permalink($item->ID)?>"><?= $item->post_title?></a> <i class="text text-muted small-text fas fa-external-link-alt"></i></strong>
                                        <ul class="options">
                                            <li class="width100prc"><span class="title">Телефон:</span><span class="value"><?= get_post_meta($item->ID, 'Phone', 1)?></span></li>
                                            <li class="width100prc"><span class="title">Эл.почта:</span><span class="value"><?= get_post_meta($item->ID, 'Email', 1)?></span></li>
                                            <li class="width100prc"><span class="title">Аддрес:</span><span class="value"><?= get_post_meta($item->ID, 'Address', 1)?></span></li>
                                            <li class="width100prc"><span class="title">Часы работы:</span><span class="value"><?= get_post_meta($item->ID, 'WorkTime', 1)?></span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </main><!-- #main -->

<?php
get_footer();