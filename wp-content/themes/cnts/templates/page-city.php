<?php
/**
 *  Template Name: Страница города
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CNTOOLS
 */


get_header();
?>

    <main class="container">

        <?php
        while ( have_posts() ) :
            the_post();

            get_template_part( 'template-parts/content', 'page' );

        endwhile; // End of the loop.
        ?>

    </main><!-- #main -->

<?php
get_footer();