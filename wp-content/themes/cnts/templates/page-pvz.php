<?php
/**
 *  Template Name: Страница пункты выдачи заказов
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CNTOOLS
 */


get_header();
?>
<div class="container">
<?php
if ( function_exists('yoast_breadcrumb') ) {
    yoast_breadcrumb( '<nav class="woocommerce-breadcrumb">','</nav>' );
}
?>

    <?php the_title( '<h1 class="cntools-entry-title">', '</h1>' ); ?>

    <div class="cnts-entry" style="margin-bottom: 60px;">
        <?php the_content();?>

        <?php
           $geo = get_geo();
           $pvzs = get_posts([
               'post_type'   => 'cdek_pvz',
               'numberposts' => -1,
               'tax_query' =>
                   array (
                       0 =>
                           array (
                               'taxonomy' => 'cdek_pvz_cat',
                               'field' => 'term_taxonomy_id',
                               'terms' => $geo['city_id'],
                           ),
                   ),
           ]);

        ?>
        <div class="row properties">
            <?php foreach ($pvzs as $item): ?>
            <div class="col-xxl-<?= !empty($geo['count']) && $geo['count'] == 1 ? '12' : '6'?>">
                <div class="card card-shadow" style="margin-bottom:20px;">
                    <div class="card-body">
                        <h2>ПВЗ: <?= $item->post_title?></h2>
                        <ul class="options">
                            <li class="width100prc"><span class="title">Телефон:</span><span class="value"><?= get_post_meta($item->ID, 'Phone', 1)?></span></li>
                            <li class="width100prc"><span class="title">Эл.почта:</span><span class="value"><?= get_post_meta($item->ID, 'Email', 1)?></span></li>
                            <li class="width100prc"><span class="title">Аддрес:</span><span class="value"><?= get_post_meta($item->ID, 'Address', 1)?></span></li>
                            <li class="width100prc"><span class="title">Часы работы:</span><span class="value"><?= get_post_meta($item->ID, 'WorkTime', 1)?></span></li>
                        </ul>
                        <a href="<?= get_permalink($item->ID)?>" class="btn btn-white" style="margin-top: 10px; float: right">Подробнее</a>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>

    </div>


</div>
<?php
get_footer();