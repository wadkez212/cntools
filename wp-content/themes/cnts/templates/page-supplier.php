<?php
/**
 *  Template Name: Страница Поставика
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CNTOOLS
 */


get_header();
?>
<script>
    window.onload = function () {

        var options = {
            animationEnabled: true,
            theme: "light2",
            title:{
                text: "Стастистика по продажам"
            },
            axisX:{
                valueFormatString: "DD MMM"
            },
            axisY: {
                title: "Кол-во продаж",
                suffix: "K",
                minimum: 30
            },
            toolTip:{
                shared:true
            },
            legend:{
                cursor:"pointer",
                verticalAlign: "bottom",
                horizontalAlign: "left",
                dockInsidePlotArea: true,
                itemclick: toogleDataSeries
            },
            data: [{
                type: "line",
                showInLegend: true,
                name: "Просмотры товаров",
                markerType: "square",
                xValueFormatString: "DD MMM, YYYY",
                color: "#F08080",
                yValueFormatString: "#,##0K",
                dataPoints: [
                    { x: new Date(2022, 08, 1), y: 63 },
                    { x: new Date(2022, 08, 2), y: 69 },
                    { x: new Date(2022, 08, 3), y: 65 },
                    { x: new Date(2022, 08, 4), y: 70 },
                    { x: new Date(2022, 08, 5), y: 71 },
                    { x: new Date(2022, 08, 6), y: 65 },
                    { x: new Date(2022, 08, 7), y: 73 },
                    { x: new Date(2022, 08, 8), y: 96 },
                    { x: new Date(2022, 08, 9), y: 84 },
                    { x: new Date(2022, 08, 10), y: 85 },
                    { x: new Date(2022, 08, 11), y: 86 },
                    { x: new Date(2022, 08, 12), y: 94 },
                    { x: new Date(2022, 08, 13), y: 97 },
                    { x: new Date(2022, 08, 14), y: 86 },
                    { x: new Date(2022, 08, 15), y: 89 }
                ]
            },
                {
                    type: "line",
                    showInLegend: true,
                    name: "Завершенные продажы",
                    lineDashType: "dash",
                    yValueFormatString: "#,##0K",
                    dataPoints: [
                        { x: new Date(2022, 08, 1), y: 60 },
                        { x: new Date(2022, 08, 2), y: 57 },
                        { x: new Date(2022, 08, 3), y: 51 },
                        { x: new Date(2022, 08, 4), y: 56 },
                        { x: new Date(2022, 08, 5), y: 54 },
                        { x: new Date(2022, 08, 6), y: 55 },
                        { x: new Date(2022, 08, 7), y: 54 },
                        { x: new Date(2022, 08, 8), y: 69 },
                        { x: new Date(2022, 08, 9), y: 65 },
                        { x: new Date(2022, 08, 10), y: 66 },
                        { x: new Date(2022, 08, 11), y: 63 },
                        { x: new Date(2022, 08, 12), y: 67 },
                        { x: new Date(2022, 08, 13), y: 66 },
                        { x: new Date(2022, 08, 14), y: 56 },
                        { x: new Date(2022, 08, 15), y: 64 }
                    ]
                }]
        };
        $("#chartContainer").CanvasJSChart(options);

        function toogleDataSeries(e){
            if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                e.dataSeries.visible = false;
            } else{
                e.dataSeries.visible = true;
            }
            e.chart.render();
        }

    }
</script>
    <main class="container">
        <link href="https://canvasjs.com/assets/css/jquery-ui.1.11.2.min.css" rel="stylesheet" />

        <?php
        while ( have_posts() ) :
            the_post();

            get_template_part( 'template-parts/content', 'page' );

        endwhile; // End of the loop.
        ?>

        <div class=" card">
            <ul class="supplier-menu">
                <li><a href="#"><i class="fa fa-user-alt"></i> Профиль</a></li>
                <li class="active"><a href="#"><i class="fa fa-chart-pie"></i> Статистика</a></li>
                <li><a href="#"><i class="fa fa-boxes"></i>Товары</a></li>
                <li><a href="#"><i class="fa fa-book"></i> Документация</a></li>
            </ul>
            <br><br>
            <div class="flex-inner">
                <div id="chartContainer" style="height: 370px; width: 100%;"></div>
            </div>


            <br>
        </div>


    </main><!-- #main -->
<br>
<br>
<br>
<br>
<br>

<?php
get_footer();


