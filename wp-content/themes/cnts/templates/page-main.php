<?php
/**
 *  Template Name: Главная страница
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CNTOOLS
 */
global $wpdb;
get_header();
?>
<!-- main-top  -->
<section class="front-page-top">
    <div class="container">
        <div class="row">
            <div class="col-md-0  col-lg-3 col-xl-3 col-xxl-3 bg-grey">
                <?php cntools_cats_on_main()?>
            </div>
            <div class="col-md-12  col-lg-9 col-xl-9 col-xxl-9">
                <div class="row">
                    <div class="col-12">
                        <?php get_template_part( 'template-parts/front-page-slider'); ?>
                    </div>
                </div>
                <?php get_template_part( 'template-parts/front-page-mini-sliders'); ?>
            </div>
        </div>
    </div>
</section>

<?php
$out = get_option('cntools-options-main-page-settings');
$result = !empty($out) ? json_decode($out, true): false;
//    dd($result);
?>
<?php if(!empty($result)): ?>
    <?php foreach ($result as $block): ?>
        <?php get_template_part( $block['block'], null, ['title' => $block['title'], 'text' => $block['text']]); ?>
    <?php endforeach;?>
<?php endif; ?>

<?php get_template_part( 'template-parts/front-page-categories'); ?>
<?php get_template_part( 'template-parts/front-page-popular-products'); ?>
<?php get_template_part( 'template-parts/front-page-company-info'); ?>
<?php get_template_part( 'template-parts/front-page-faq'); ?>
<?php get_template_part( 'template-parts/front-page-become-partner'); ?>


<?php
get_footer();
