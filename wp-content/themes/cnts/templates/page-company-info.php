<?php
/**
 *  Template Name: Страница реквизитов
 *
 */


get_header();
?>

    <main class="container">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<nav class="woocommerce-breadcrumb">','</nav>' );
        }
        ?>
        <?php the_title( '<h1 class="cntools-entry-title">', '</h1>' ); ?>

        <?php the_content(); ?>


    </main><!-- #main -->

<?php
get_footer();