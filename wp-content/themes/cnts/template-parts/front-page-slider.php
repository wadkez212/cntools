<?php
$sliders = get_posts( array(
    'numberposts' => 5,
//    'category'    => 45,
    'orderby'     => 'date',
    'order'       => 'DESC',
    'include'     => array(),
    'exclude'     => array(),
    'meta_key'    => '',
    'meta_value'  =>'',
    'post_type'   => 'slider',
    'suppress_filters' => true,
) );

?>


<div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel" style="margin-top: 12px;">
    <?php
    echo do_shortcode('[smartslider3 slider="1"]');
    ?>
</div>