<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CNTOOLS
 */

?>
<?php



if(!is_product()):
    if ( is_singular() ) :
        the_title( '<h1 class="entry-title">', '</h1>' );
    else :
        the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
    endif;
endif;
?>

<?php
the_content(
    sprintf(
        wp_kses(
        /* translators: %s: Name of current post. Only visible to screen readers */
            __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'cntools' ),
            array(
                'span' => array(
                    'class' => array(),
                ),
            )
        ),
        wp_kses_post( get_the_title() )
    )
);

?>

<!-- #post-<?php the_ID(); ?> -->
