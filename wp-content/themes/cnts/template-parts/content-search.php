<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CNTOOLS
 */

?>



    <div class="card card-shadow offers-list-card" style="margin-bottom: 20px;">
        <div class="card-body">
            <?php the_title( sprintf( '<a href="%s" rel="bookmark"><strong>', esc_url( get_permalink() ) ), '</strong></a>' ); ?>
            <span class="text text-muted"><?php the_excerpt(); ?></span>
        </div>
    </div>

