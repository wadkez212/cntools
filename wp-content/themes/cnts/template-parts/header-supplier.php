<header>
    <!--  Top  -->
    <section class="top hide-500">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-lg-6 col-md-10">
                </div>
            </div>
        </div>
    </section>
    <!--  Middle  -->
    <section class="container middle" style="border-bottom: solid 10px var(--bs-blue)">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-xl-8 col-xxl-8">
                <div class="flex-inline logo-wrapper">
                    <div class="blue-butter js-toggle-menu" style="display: none;" data-target=".right-options">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <a href="https://cntools.ru" class="logo" style="width: 150px;">
                        <img src="https://cntools.ru/wp-content/themes/cnts/assets/images/logo.png">
                    </a>
                </div>
            </div>
            <div class="col-sm-4 col-md-4  col-lg-4 col-xl-4 col-xxl-4 hide-500">
                <ul class="checker-and-gate">
                    <?php if(is_user_logged_in()): ?>
                        <li><a href="/my-account"><i class="fas fa-user-circle"></i> <span><?php echo wp_get_current_user()->display_name; ?></span></a>
                        </li>
                    <?php else: ?>
                        <li><a href="/my-account"><i class="fas fa-user-circle"></i> <span>Авторизация</span></a></li>
                    <?php endif; ?>
                </ul>
            </div>

        </div>
    </section>


    <!--  Navigation   -->

</header>
