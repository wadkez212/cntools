<?php
$categories = get_categories( [
    'taxonomy'     => 'product_cat',
    'child_of'     => 0,
    'parent'       => '0',
    'orderby'      => 'term_ID',
    'order'        => 'ASC',
    'hide_empty'   => 1,
    'hierarchical' => 1,
    'exclude'      => [23],
//    'number'       => 0,
//    'pad_counts'   => false,
] );

//dd($categories);

?>


       <div class="row">
               <?php foreach ($categories as $item): ?>
                   <?php $thumbnail_id = get_term_meta( $item->term_id, 'thumbnail_id', true );?>
                   <div class="col-md-4">
                       <a class="catalog-item" href="<?= get_category_link( $item->term_id )?>">
                           <strong><?= $item->name?></strong> (<?= $item->count?>)
                       </a>
                       <?php
                       $childes = get_categories( [
                           'taxonomy'     => 'product_cat',
                           'child_of'     => $item->term_id,
                           'orderby'      => 'term_ID',
                           'order'        => 'ASC',
                           'hide_empty'   => 1,
                           'hierarchical' => 1,
                           'exclude'      => [23],
                       ] );

                       foreach ($childes as $subitem):
                       ?>
                           <a class="catalog-sub-item" href="<?= get_category_link( $subitem->term_id )?>">
                               <strong><?= $subitem->name?></strong> (<?= $subitem->count?>)
                           </a>
                        <?php endforeach;?>
                       <br>
                   </div>
               <?php endforeach; ?>
       </div>







