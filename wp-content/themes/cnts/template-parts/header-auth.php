<ul class="checker-and-gate">
    <li><a href="/status-zakaza"><i class="far fa-clock "></i> <span>Статус заказа</span></a></li>
    <?php if(is_user_logged_in()): ?>
        <li><a href="/my-account"><i class="fas fa-user-circle"></i> <span><?php echo wp_get_current_user()->display_name; ?></span></a>
            <!--                            <ul class="user-urls">-->
            <!--                                <li><a href="">--><?php //echo wp_get_current_user()->display_name; ?><!--</a></li>-->
            <!--                                <li><a href="">Мой кабинет</a></li>-->
            <!--                                <li><a href="">Личные данные</a></li>-->
            <!--                                <li><a href="">Мой заказы</a></li>-->
            <!--                                <li><a href="">Избранное</a></li>-->
            <!--                                <li><a href="/my-account/customer-logout/?_wpnonce=098a42b5ef">Выход</a></li>-->
            <!--                            </ul>-->
        </li>
    <?php else: ?>
        <li><a href="/my-account"><i class="fas fa-user-circle"></i> <span>Авторизация</span></a></li>
    <?php endif; ?>
</ul>