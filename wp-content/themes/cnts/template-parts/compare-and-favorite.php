<ul class="compare-and-favorite">
    <li><a href="/spisok-tovarov-na-sravnenie"><i class="fa fa-balance-scale"></i> <span>Сравненние</span></a></li>
    <li><a href="/spisok-izbrannyh-tovarov"><i class="fa fa-heart"></i> <span>Избранное</span></a></li>
    <?php if(WC()->cart->get_cart_contents_count() > 0): ?>
        <li class="cart"><a href="/cart/"><i class="fa fa-shopping-cart"></i><sup><?= WC()->cart->get_cart_contents_count()?></sup> <span><?= WC()->cart->get_cart_subtotal()?></span></a></li>
    <?php else: ?>
    <li class="cart"><a href="/cart/"><i class="fa fa-shopping-cart"></i> <span class="header-cart-price">0.00 <span class="woocommerce-Price-currencySymbol">₽</span></span></a></li>
    <?php endif;?>
</ul>