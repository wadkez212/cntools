<?php if(!empty($_POST['category_id'])): ?>
<?php

    global $wpdb;
    $table_name = $wpdb->prefix . 'offers';

    $products = get_posts([
        'post_type'   => 'product',
        'numberposts' => -1,
        'tax_query' =>
            array (
                0 =>
                    array (
                        'taxonomy' => 'product_cat',
                        'field' => 'term_taxonomy_id',
                        'terms' => $_POST['category_id'],
                    ),
            ),
    ]);

    if(!empty($products)){
        foreach ($products as $item) {
            $out = $wpdb->insert( 
                $table_name,
                [
                    'product_id' => $item->ID,
                    'supplier_id' => $_POST['supplier_id'],
                    'create_at' => date('Y-m-d H:i:s'),
                    'update_at' => date('Y-m-d H:i:s'),
                ]
            );
        }
    }


?>
<?php endif; ?>


