<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CNTOOLS
 */
$current_page = sanitize_post( $GLOBALS['wp_the_query']->get_queried_object() );
?>
<?php
if ( function_exists('yoast_breadcrumb') ) {
    yoast_breadcrumb( '<nav class="woocommerce-breadcrumb">','</nav>' );
}
?>
<?php if (!in_array($current_page->post_name, ['my-account'])): ?>
    <?php the_title( '<h1 class="cntools-entry-title">', '</h1>' ); ?>
<?php endif;?>
<div class="cnts-entry">
<?php the_content();?>
</div>

