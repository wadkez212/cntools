<header>
    <!--  Top  -->
    <section class="top hide-500">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-lg-6 col-md-10  col-xxl-5">
                    <?php get_template_part( 'template-parts/geo'); ?>
                </div>
                <div class="col-sm-2 col-lg-6 col-md-2 col-xxl-7 pos-rel">
                    <div class="blue-butter js-toggle-menu" data-target=".right-options" style="margin-top: 15px;">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>

                    <?php
                    wp_nav_menu(
                        array(
                            'container'  => '',
                            'menu_class' => 'options right-options',
                            'theme_location' => 'menu-2',
                        )
                    );
                    ?>
                </div>
            </div>
        </div>
    </section>

    <!--  Middle  -->
    <section class="container middle">
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-4 col-lg-3 col-xl-5 col-xxl-5">
                <div class="flex-inline logo-wrapper">
                    <div class="blue-butter js-toggle-menu" style="display: none;" data-target=".right-options">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <a href="https://cntools.ru" class="logo">
                        <img src="https://cntools.ru/wp-content/themes/cnts/assets/images/logo.png">
                    </a>
                    <?php //if ( !empty(get_geo()['slogan']) ): ?>
                        <div class="slogan"><?php //get_geo()['slogan']?></div>
                    <?php //endif;?>

                </div>
            </div>
            <div class="col-sm-5 col-md-4  col-lg-3 col-xl-2 col-xxl-2 hide-500">
                <div class="phones">
                    <?php //if ( !empty(get_geo()['phone']) ): ?>
                        <div><?php //get_geo()['phone']?></div>
                    <?php //endif;?>

                    <?php //if ( !empty(get_geo()['phone_additional']) ): ?>
                        <div><?php //get_geo()['phone_additional']?></div>
                    <?php //endif;?>
                </div>

            </div>
            <div class="col-sm-2 col-md-2  col-lg-2 col-xl-2 col-xxl-2 hide-500 ">
                <div class="flex-vertical">
                    <button type="button" class="btn btn-primary btn-order-call js-callback-order"  data-title="Заказать звонок" data-bs-toggle="modal" data-bs-target="#callback_button">
                        <i class="fa fa-phone-volume radial-pulse"></i> <span>Заказать звонок</span>
                    </button>
                </div>
            </div>
            <div class="col-sm-2 col-md-2  col-lg-4 col-xl-3 col-xxl-3 hide-500">
                <?php get_template_part( 'template-parts/header-auth'); ?>
            </div>

        </div>
    </section>


    <!--  Navigation   -->

    <nav class="bottom hide-500">
        <div class="container relative">
            <div class="row">
                <div class="col-sm-2 col-md-1  col-lg-1 col-xl-3 col-xxl-3 bg-blue" style="padding: 0;">
                    <h3 class="catalog-menu-title js-display-catalog" data-title="Каталог продукции">
                            <span class="butter-button">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        <span class="cat-text">Каталог продукции</span>
                    </h3>
                </div>
                <div class="col-sm-6 col-md-8  col-lg-8 col-xl-5 col-xxl-5">
                    <?php get_template_part( 'template-parts/header-search'); ?>
                </div>
                <div class="col-sm-4 col-md-2  col-lg-3 col-xl-4 col-xxl-4">
                    <div class="flex-vertical">
                        <?php get_template_part( 'template-parts/compare-and-favorite'); ?>
                    </div>

                </div>
            </div>
            <div class="global-dropdown"></div>
        </div>
    </nav>

    <!--  Mobile  -->
    <div class="container">
        <div class="row mob-search-block">
            <div class="col-7">
            </div>
            <div class="col-5">
            </div>
        </div>
    </div>

</header>
