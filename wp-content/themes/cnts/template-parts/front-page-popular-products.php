<?php
global $wpdb;
$results = $wpdb->get_results( "SELECT * FROM $wpdb->posts WHERE post_type='product' AND post_status='publish' LIMIT 5");
?>
<h2 class="front-section-title"><?php echo !empty($args['title']) ? $args['title'] : 'Популярные товары' ;?></h2>
<section class="container">
    <div class="row">
        <?= do_shortcode('[products limit="4" columns="4" orderby="popularity"  on_sale="false" ]') ?>
    </div>
</section>

