<div class="be-partner">
    <section class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo !empty($args['title']) ? $args['title'] : 'Хотите стать <span>партнерем?</span>' ;?></h2>
                <?php echo !empty($args['text']) ? '<p>' . $args['text'] . '</p>' : '' ;?>
                <div class="callback-form">
                    <input class="form-control" type="text" name="name" placeholder="Ваще имя">
                    <input class="form-control" type="text" name="phone" placeholder="+7 (999) 99-99-99">
                    <div class="form-footer row">
                        <div class="col-xxl-6">
                            <button class="btn btn-yellow js-send-request-be-partner" data-serialize=".callback-form input">Оставить заявку</button>
                        </div>
                        <div class="col-xxl-6">
                            <div class="conf-policy">Нажимая кнопку отправить вы соглашаетесь с <a href="<?= privacy_policy()?>" target="_blank">политикой конфиденциальности</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>