<div class="alert alert-info">
    Здесь вы сможете выбрать нужные категории с товарами и добавить их в свой список товаров. <br> Далее вы сможете добавить для них свой прайс.
<!--    Здесь вы можете сгенерировать файл загрузки товаров на базе уже существующих товаров и категории. Если не найдете нужную категорию  Или товар напишите  нашим менеджерам-->
</div>

<?php
$categories = get_categories( [
    'taxonomy'     => 'product_cat',
    'child_of'     => 0,
    'parent'       => '0',
    'orderby'      => 'term_ID',
    'order'        => 'ASC',
    'hide_empty'   => 1,
    'hierarchical' => 1,
    'exclude'      => [23],
//    'number'       => 0,
//    'pad_counts'   => false,
] );
?>


<div class="row cat-list-inputs-wrapper">
    <input type="hidden" name="supplier_id" value="<?= $_POST['id']?>">
    <?php foreach ($categories as $item): ?>
        <?php $thumbnail_id = get_term_meta( $item->term_id, 'thumbnail_id', true );?>
        <div class="col-md-4">
            <div class="form-check" style="border-bottom: dotted 1px #aaa;">
                <input class="form-check-input js-check-sub" data-target=".cat-<?= $item->term_id?>" type="checkbox" name="category_id[]" value="<?= $item->term_id?>" id="cat-<?= $item->term_id?>">
                <label class="form-check-label" for="cat-<?= $item->term_id?>" >
                    <strong><?= $item->name?></strong> (<?= $item->count?>)
                </label>
            </div>
            <?php
            $childes = get_categories( [
                'taxonomy'     => 'product_cat',
                'child_of'     => $item->term_id,
                'orderby'      => 'term_ID',
                'order'        => 'ASC',
                'hide_empty'   => 1,
                'hierarchical' => 1,
                'exclude'      => [23],
            ] );

            foreach ($childes as $subitem):
                ?>
                <div class="form-check">
                    <input class="form-check-input cat-<?= $item->term_id?>" type="checkbox" name="category_id[]" value="<?= $subitem->term_id?>" id="cat-<?= $subitem->term_id?>">
                    <label class="form-check-label" for="cat-<?= $subitem->term_id?>" style="font-size: 11px;">
                        <strong><?= $subitem->name?></strong> (<?= $subitem->count?>)
                    </label>
                </div>
            <?php endforeach;?>
            <br>
        </div>
    <?php endforeach; ?>
</div>