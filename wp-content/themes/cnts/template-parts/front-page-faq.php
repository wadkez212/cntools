<?php
$results = $wpdb->get_results( "SELECT ID, post_title, post_content FROM $wpdb->posts  WHERE post_type='faq' AND post_status='publish'");
?>

<h2 class="front-section-title"><?php echo !empty($args['title']) ? $args['title'] : 'Это будет полезно' ;?></h2>

<section class="container faq">
    <div class="row">
        <div class="col-md-12 col-lg-6 col-xxl-6 left-block">
            <h2>Часто задаваемые вопросы</h2>
            <ul class="faq-list">
                <?php foreach ($results as $item): ?>
                    <li class="js-faq-toggle">
                        <div class="faq-question"><?= $item->post_title?></div>
                        <div class="faq-answer">
                            <?= $item->post_content?>
                        </div>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
        <div class="col-6 col-md-6 right-block">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/man.png">
        </div>
    </div>

</section>