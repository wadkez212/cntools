<?php
global $wpdb;
$table_name = $wpdb->prefix . 'offers';
$products = $wpdb->get_results( "SELECT * FROM $table_name WHERE supplier_id = {$Request->id} ORDER BY price ASC" );


$tmp = [];
$tmp[] = [
   'ID', 'Имя', 'Артикул', 'Бренд',  'Запасы',  'Цена', 'Цена со скидкой', 'Единица товара', 'Вес (кг)', 'Длина (мм)', 'Ширина (мм)', 'Высота (мм)'
];


if(!empty($products)) {
    foreach($products as $item) {
        $tmp[] = [
            $item->id,
            get_the_title($item->product_id),
            (string)get_post_meta($item->product_id, '_sku', 1),
            $item->brand,
            $item->quantity,
            $item->price,
            $item->price_sale,
            'Штука',
             0, 0, 0, 0
        ];
    }
}


$csv_file_name = wp_get_upload_dir()['path'] . '/' . $Request->id . '-importer.csv';
file_put_contents($csv_file_name, '');
create_csv($tmp, $Request->id . '-importer');
$url = wp_upload_dir()['url'] . '/' . $Request->id . '-importer.csv';
echo '<div class="alert alert-success">Файл успешно сгенерирован - <a href="' . $url .  '"><strong>Скачать</strong></a></div>';


