<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CNTOOLS
 */
$current_page = sanitize_post( $GLOBALS['wp_the_query']->get_queried_object() );
?>
<nav class="woocommerce-breadcrumb"><span><span><a href="/">Главная</a> / <a href="/punkty-vydachi-zakazov/">Пункты выдачи заказов</a> / <span class="breadcrumb_last" aria-current="page"><?= get_the_title()?></span></span></span></nav>

<?php the_title( '<h1 class="cntools-entry-title">', '</h1>' ); ?>

<div class="cnts-entry properties">
    <ul class="options">
        <li class="width100prc"><span class="title">Телефон:</span><span class="value"><?= get_post_meta(get_the_ID(), 'Phone', 1)?></span></li>
        <li class="width100prc"><span class="title">Эл.почта:</span><span class="value"><?= get_post_meta(get_the_ID(), 'Email', 1)?></span></li>
        <li class="width100prc"><span class="title">Аддрес:</span><span class="value"><?= get_post_meta(get_the_ID(), 'FullAddress', 1)?></span></li>
        <li class="width100prc"><span class="title">Часы работы:</span><span class="value"><?= get_post_meta(get_the_ID(), 'WorkTime', 1)?></span></li>
    </ul>
    <br>
    <br>
    <h2>Как добратся</h2>
    <?php the_content();?>
    <?php
        $images = json_decode(get_post_meta(get_the_ID(), 'images', 1), true);
    ?>
    <div class="row">
        <?php $i = 0; foreach ($images as $img): ?>
        <?php if($i == 4) break; ?>
        <div class="col-md-3" style="margin-bottom: 20px;">
            <div class="main-image" style="background: #f0f0f0;">
            <a data-fancybox="gallery" href="<?= $img?>">
                <div class="image-wrapper">
                <img loading="lazy" class="rounded img-fluid"  src="<?= $img?>" alt="">
                </div>
            </a>
            </div>
        </div>
        <?php $i++;  endforeach; ?>
        <div class="col-md-12">
            <div class="ya-map" id="map" style="min-height: 450px; margin-top: 10px; background: #f0f0f0; border: solid 1px #e1e1e1;"></div>
        </div>
    </div>

<?php
//    dd(get_post_meta(get_the_ID()));
?>

</div>

<script  type="text/javascript">
    ymaps.ready(init);

    function init() {
        var myMap = new ymaps.Map("map", {
                center: [<?= get_post_meta(get_the_ID(), 'coordY', 1)?>, <?= get_post_meta(get_the_ID(), 'coordX', 1)?>],
                zoom: 14
            });

        myMap.geoObjects
            .add(new ymaps.Placemark([<?= get_post_meta(get_the_ID(), 'coordY', 1)?>, <?= get_post_meta(get_the_ID(), 'coordX', 1)?>], {
                balloonContent: '<?= get_post_meta(get_the_ID(), 'FullAddress', 1)?>'
            }, {
                preset: 'islands#icon',
                iconColor: '#0095b6'
            }));
    }

</script>
