<?php
$result = getCntoolsModuleOption('cntools-options-out-company');
?>
<section class="our-company">
    <div class="container">
        <div class="row">
            <div class="col-xxl-4 under-logo-text">
                <div class="company-logo">
                    <img src="<?= $result['logo']?>">
                </div>
                <p><?= $result['text_under_logo']?></p>
                <p><?= $result['text_under_text']?></p>
            </div>
            <div class="col-xxl-8 border-left">
                <?= $result['description']?>
            </div>
        </div>

        <div class="row">
            <div class="col-xxl-12">
                <?php if($result): ?>
                <div class="our-company-info-blocks">
                    <?php foreach($result as $item):?>
                        <?php if(is_array($item)): ?>
                            <div class="flex-inner" style="background: white">
                                <div class="img-wrapper">
                                    <img src="<?=$item['icon']?>"<?=(!empty($item['name'])) ? "alt=\"{$item["name"]}\"" : '' ;?>>
                                </div>
                                <?php if (is_numeric($item['line'])): ?>
                                    <h3><?=number_format($item['line'], 0, ' ', ' ')?></h3>
                                <?php else: ?>
                                    <h3><?= $item['line']?></h3>
                                <?php endif; ?>
                                <p><?=$item['line2']?></p>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>

    </div>
</section>
