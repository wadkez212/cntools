<?php
$categories = get_categories( [
    'taxonomy'     => 'product_cat',
    'parent'       => '0',
    'orderby'      => 'term_ID',
    'order'        => 'ASC',
    'hide_empty'   => 0,
//    'hierarchical' => 1,
    'exclude'      => [22, 23],
] );

?>

<h2 class="front-section-title"><?php echo !empty($args['title']) ? $args['title'] : 'Выберите нужную категорию' ;?></h2>

<section class="container product-categories">
    <ul class="categories-list">
        <?php foreach ($categories as $item): ?>
            <?php $thumbnail_id = get_term_meta( $item->term_id, 'thumbnail_id', true );?>
            <li>
                <a href="<?= get_category_link( $item->term_id )?>">
                    <div class="img">
                        <img src="<?= wp_get_attachment_url( $thumbnail_id )?>">
                    </div>
                    <strong><?= $item->name?></strong>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</section>

