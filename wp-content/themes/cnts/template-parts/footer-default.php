<footer>
    <?php get_template_part( 'template-parts/footer-subscribe'); ?>
    <div class="container footer-menus">
        <div class="row">
            <div class="col-md-12 col-lg-3 col-xxl-3">
                <h3>О компании</h3>
                <?php
                wp_nav_menu(
                    array(
                        'container'  => '',
                        'menu_class' => 'footer-menu-list',
                        'theme_location' => 'footer-menu-1',
                    )
                );
                ?>
            </div>
            <div class="col-md-12 col-lg-3 col-xxl-3">
                <h3>Получение и оплата</h3>
                <?php
                wp_nav_menu(
                    array(
                        'container'  => '',
                        'menu_class' => 'footer-menu-list',
                        'theme_location' => 'footer-menu-3',
                    )
                );
                ?>
            </div>
            <div class="col-md-12 col-lg-3 col-xxl-3">
                <h3>Сервис поддержка</h3>
                <?php
                wp_nav_menu(
                    array(
                        'container'  => '',
                        'menu_class' => 'footer-menu-list',
                        'theme_location' => 'footer-menu-4',
                    )
                );
                ?>
            </div>
            <div class="col-md-12 col-lg-3 col-xxl-3">
                <h3>Наши соцсети</h3>
                <?php
                $out = get_option('cntools-options-snetwork');
                $result = !empty($out) ? json_decode($out, true) : false;
                ?>
                <?php if($result): ?>
                    <ul class="social-media-list">
                        <?php foreach($result as $item):?>
                            <li><a href="<?=$item['url']?>">
                                    <img src="<?=$item['icon']?>" alt="<?=$item['name']?>">
                                </a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="col-12">
                <?php
                $cntools_option_key = 'cntools-options-payment-system-list';
                $out = get_option($cntools_option_key);
                $result = !empty($out) ? json_decode($out, true): false;
                ?>
                <ul class="payment-icon-list payment-icon-list-footer">
                    <?php foreach ($result as $item): ?>
                        <li><img src="<?= $item['icon']?>" title="<?= $item['line']?>"></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-12">
                <p style="color: white" class="copyright">
                    &copy; 2022 "CNTOOLS" &mdash; Профессиональные инструменты
                </p>
            </div>
        </div>
    </div>
</footer>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
<?php wp_footer(); ?>




<div class="modal fade" id="callback_button" tabindex="-1" aria-labelledby="callback_button" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-5"><button type="button" class="btn btn-primary"></button></div>
                    <div class="col-md-7">
                        <div class="text text-muted small">Нажимая кнопку вы соглашаетесь с <a class="underline" href="<?= privacy_policy()?>" target="_blank">политикой конфиденциальности</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>