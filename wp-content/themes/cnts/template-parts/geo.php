<?php
$geo = get_geo();

dd($geo);
?>

<ul class="options left-options">
    <li class="selected-geo">
        <a href="#"><i class="fas fa-map-marker-alt"></i> <strong>Ваш город:</strong>
            <span class="js-change-geo"
                  data-action="get_city_list"
                  data-title="Список городов"
                  data-action="get_city_list"
                  data-bs-toggle="modal"
                  data-bs-target="#callback_button"
                  data-city="<?= $geo['city_id']?>"><?= get_term($geo['city_id'])->name?></span></a>
        <?php if( empty($_COOKIE['cntools_current_geo']) ): ?>
        <ul class="geo-selector js-confirm-geo">
            <li>
                <p class="confirm-city-text">Ваш город: <span class="selected-city"><?= get_term($geo['city_id'])->name?>?</span></p>
                <div style="text-align: center">
                    <button class="btn btn-white btn-sm js-change" data-title="Список городов" data-action="get_city_list" data-bs-toggle="modal" data-bs-target="#callback_button" data-city="<?= $geo['city_id']?>"  style="min-width: 70px;">Нет</button>
                    <button class="btn btn-primary btn-sm js-yes" data-city="<?= $geo['city_id']?>"  style="min-width: 70px;">Да</button>
                </div>
            </li>
        </ul>
        <?php endif; ?>
    </li>
    <?php if(!empty($geo['count'])):?>
        <li><a href="/punkty-vydachi-zakazov/"><i class="fas fa-boxes"></i> Пункты выдачи</a></li>
    <?php else: ?>
    <li><a href="#"><i class="fas fa-map-marked-alt"></i> <strong>Офис:</strong> <span><?= $geo['address']?></span></a></li>
    <li><i class="far fa-clock"></i> <?= $geo['hours']?></li>
    <?php endif; ?>
</ul>

