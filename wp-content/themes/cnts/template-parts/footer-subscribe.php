<section class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-7 col-xxl-7">

                    <div class="subscribe-text">
                        <span class="fa fa-envelope"></span> Подпишитесь на рассылку и будьте в курсе! Акции, скидки и спецпредложения ждут вас!
                    </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-5 col-xxl-5">

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 col-xxl-8">
                            <input type="email" class="form-control inp-subscribe" id="email" name="email" placeholder="Введите E-mail">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 col-xxl-4">
                            <button type="submit" class="btn btn-yellow js-subscribe" data-action="subscribe_email" data-serialize=".inp-subscribe">Подписаться</button>
                        </div>
                    </div>

            </div>
        </div>
    </div>
</section>