<?php
$citys = json_decode(get_option('cntools-options-producer-working-info'));
$categories = get_categories( [
    'taxonomy'     => 'cdek_pvz_cat',
    'child_of'     => 0,
    'parent'       => '0',
    'orderby'      => 'name',
    'order'        => 'ASC',
    'hide_empty'   => true,
    'hierarchical' => 0,
] );
?>


<div class="row geo-items-list">
    <div class="col-12">
        <div class="mb-3">
            <input type="text" name="search_city" class="form-control" placeholder="Название населенного пункта...">
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-5 geo-item-wrapper">
        <?php foreach ($citys as $city):  ?>
            <a href="#city-<?= $city->city_id?>" style="font-weight: 700;" class="js-define-geo" data-city="<?= $city->city_id?>"><i class="fas fa-map-marker-alt"></i> <?= get_term($city->city_id)->name?></a>
        <?php  endforeach; ?>
        </div>
    </div>
    <div class="col-12 geo-display-block"></div>
    <?php foreach ($categories as $item): ?>
    <?php $test[] = $item->slug?>
    <div class="col-md-3 geo-item-wrapper">
        <div class="geo-item-title"><?= $item->name?></div>
        <?php
        $sub_categories = get_categories( [
            'taxonomy'     => 'cdek_pvz_cat',
            'child_of'     => $item->term_id,
        ] );
        ?>
        <?php foreach ($sub_categories as $sub_item): ?>
        <a href="#city-<?= $sub_item->cat_ID?>" class="js-define-geo" data-city="<?= $sub_item->cat_ID?>"><i class="fas fa-map-marker-alt"></i> <?= $sub_item->name?></a>
        <?php endforeach;?>
    </div>
    <?php endforeach;?>
</div>
<?php


