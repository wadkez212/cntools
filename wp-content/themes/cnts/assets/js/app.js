/**
 *
 * @param config
 * @constructor
 */

function setPickupPoint() {
    var baloon = document.querySelector('.js-point-select');
    var baloonHeader = baloon.innerText;
    document.querySelector('#shipping_address').value = baloonHeader;
}

function JSCntoolsFront(config) {
    this.config = config || {};
    this.params = {};
    this.init();
}

JSCntoolsFront.prototype = {
    init: function () {
        console.log('init front');
        $ = jQuery;
        this.unbind();
        this.bindEvents();
        _this = this;
        window.commonModal = new bootstrap.Modal('#callback_button');
        // window.supplierModal = new bootstrap.Modal('#supplier_modal');
        $('.date-picker').datepicker({
            format: "dd.mm.yyyy",
            language: "ru"
        });
    },
    unbind: function () {
    },
    bindEvents: function () {
        $(document).on('click touch', '.js-send-action', this.sendAction);
        $(document).on('click touch', '.js-confirm-geo .js-yes', this.defineGeo);
        $(document).on('click touch', '.js-define-geo', this.defineGeo);
        $(document).on('click touch', '.js-confirm-geo .js-change', this.changeGeo);
        $(document).on('click touch', '.js-change-geo', this.changeGeo);
        $(document).on('click touch', '.js-callback-order', this.callbackOrder);
        $(document).on('click touch', '.js-send-callback-order', this.sendCallbackOrder);
        $(document).on('click touch', '.js-subscribe', this.emailSubscribe);
        $(document).on('click touch', '.js-view-offers-list', this.viewOffersList);
        $(document).on('click touch', '.js-subscribe-on-notification', this.subscribeOnNotification);
        $(document).on('click touch', '.js-send-subscribe-notification', this.sendSubscribeNotification);
        $(document).on('click touch', '.js-add-to', this.addTo);
        $(document).on('click touch', '.js-send-request-be-partner', this.sendRequestBePartner);
        $(document).on('click touch', '.js-display-catalog', this.viewCatalogMenu);
        $(document).on('click touch', '.js-steps-nav', this.stepLocation);
        $(document).on('change', '[name="self_service"]', this.calcSelfService);
        $(document).on('click touch', '.js-active-offers-tab', this.activeOffersTab);
        $(document).on('click touch change', '.js-billing-user-type input', this.billingUserType);
        $(document).on('click touch', '.js-gen-product-template', this.genProductTemplate);
        $(document).on('click touch', '.js-add-all-products-from-cats', this.addToSupplierListSelectedProducts);
        $(document).on('click touch', '.js-get-update-csv', this.getUpdateCsv);
        $(document).on('click touch', '.js-upload-and-update-csv', this.uploadAndUpdateCsv);
        $(document).on('click touch', '.js-update-by-csv', this.uploadCsv);

    },
    modal: {
        init: function () {
            $('.modal-body .alert').remove();
            $('.modal-dialog').removeClass('modal-xl');
            $('.modal-footer').hide();
        },
        removeAlert: function () {
            $('.modal-body .alert').remove()
        },
        titleText: function (text) {
            $('.modal-title').text(text)
        },
        footerHide: function () {
            $('.modal-footer').hide()
        },
        footerShow: function () {
            $('.modal-footer').show()
        },
        bodyText: function (text) {
            $('.modal-body').text(text)
        },
        bodyHtml: function (html) {
            $('.modal-body').html(html)
        },
        bodyPrepend: function (html) {
            $('.modal-body').prepend(html)
        },
        btnText: function (text) {
            $('.modal-footer .btn').text(text)
        },
        btnClass: function (selector) {
            $('.modal-footer .btn').addClass(selector)
        },
        xl: function () {
            $('.modal-dialog').addClass('modal-xl')
        },
        btnAttr: function (name, value) {
            $('.modal-footer .btn').attr(name, value)
        }
    },
    inArray: function (needle, haystack) {
        var length = haystack.length;
        for (var i = 0; i < length; i++) {
            if (haystack[i] == needle) return true;
        }
        return false;
    },
    toObject: function (arr) {
        var obj = {};
        $.each(arr, function (i, o) {
            var n = o.name,
                v = o.value;

            obj[n] = obj[n] === undefined ? v
                : Array.isArray(obj[n]) ? obj[n].concat(v)
                    : [obj[n], v];
        });

        return obj;
    },
    setCookie: function (name, value, days) {
        var expires = "";
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toUTCString();
        }
        document.cookie = name + "=" + (value || "") + expires + "; path=/";
    },
    getCookie: function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    },
    eraseCookie: function (name) {
        document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    },
    defineGeo: function () {
        var $self = $(this), $data = $self.data(), citi_id = $data.city;
        _this.setCookie('cntools_current_geo', citi_id, 120);
        location.reload(true);
    },
    changeGeo: function () {
        var $self = $(this), $data = $self.data()

        _this.modal.init();
        _this.modal.xl();
        _this.modal.titleText($data.title);

        _this.ajaxRequest($data.action, 'city_id=' + $data.city,
            function () {
                _this.modal.bodyHtml('Загрузка...');
            },
            function (response) {
                _this.modal.bodyHtml(response);
            }
        );
    },
    callbackOrder: function () {
        var $self = $(this), $data = $self.data();
        _this.modal.init();
        _this.modal.footerShow();
        _this.modal.titleText($data.title);
        _this.modal.btnText('Заказать звонок')
        _this.modal.btnClass('js-send-callback-order');

        _this.ajaxRequest('callback_order', '',
            function () {
                _this.modal.bodyHtml('Загрузка...');
            },
            function (response) {
                _this.modal.bodyHtml(response);
            });
    },
    sendCallbackOrder: function () {
        var $self = $(this), $selfText = $self.text();
        _this.ajaxRequest('callback_order_handler', $('.js-callback-wrapper input').serialize(),
            function () {
                _this.modal.init();
                $self.html('Загрузка...');
            },
            function (response) {
                $self.text($selfText);
                if (response == 'error') {
                    _this.modal.bodyPrepend('<div class="alert alert-danger">Все поля обязательны к заполнению</div>');
                }

                if (response == 'ok') {
                    _this.modal.bodyHtml('<div class="alert alert-success">Спасибо! Скоро с вами свяжется наш менеджер!</div>');
                }
            });
    },
    emailSubscribe: function () {
        var $self = $(this), $data = $self.data(), $selfText = $self.text();
        _this.ajaxRequest($data.action, $($data.serialize).serialize(),
            function () {
                $($data.serialize).removeClass('is-invalid')
                $self.text('Загрузка...');
            },
            function (response) {
                $self.text($selfText);
                if (response == 'error') {
                    $($data.serialize).addClass('is-invalid')
                }

                if (response == 'ok') {
                    $('.subscribe .container').html('<div style="display: block; color: white; font-size: 18px; text-align: center;">Вы успешно подписались!</div>');
                }
            });
    },
    viewOffersList: function () {
        var $self = $(this), $data = $self.data(), $selfText = $self.text();
        _this.ajaxRequest($data.action, '&id=' + $data.id,
            function () {
                _this.modal.init();
                _this.modal.xl();
                _this.modal.titleText($data.title);
                _this.modal.bodyHtml('Загрузка...');

                commonModal.show();
            },
            function (response) {
                $self.text($selfText);
                _this.modal.bodyHtml(response);
            });
    },
    viewCatalogMenu: function () {
        var $self = $(this), $data = $self.data(), $selfText = $self.text();
        _this.ajaxRequest('get_categories', '',
            function () {
                _this.modal.init();
                _this.modal.xl();
                _this.modal.titleText($data.title);
                _this.modal.bodyHtml('Загрузка...');
                commonModal.show();
            },
            function (response) {
                _this.modal.bodyHtml(response);
            });
    },
    genProductTemplate: function () {
        var $self = $(this), $data = $self.data(), $selfText = $self.text();
        _this.modal.init();
        _this.modal.footerShow();
        _this.modal.xl();
        _this.modal.btnText('Сохранить');
        _this.modal.btnClass('js-add-all-products-from-cats');

        _this.ajaxRequest($data.action, '&id=' + $data.id,
            function () {
                _this.modal.titleText($data.title);
                _this.modal.bodyHtml('Загрузка...');
            },
            function (response) {
                _this.modal.bodyHtml(response);
            });
    },
    addToSupplierListSelectedProducts: function () {
        var $self = $(this), $data = $self.data(), $selfText = $self.text();
        _this.modal.init();
        _this.modal.xl();

        _this.ajaxRequest('add_to_supplier_list_selected_products', $('.cat-list-inputs-wrapper input:checked, [name="supplier_id"]').serialize(),
            function () {
                _this.modal.bodyHtml('Загрузка...');
            },
            function (response) {
                window.location.href = window.location.href + '&out=success';
            }
        );
    },
    getUpdateCsv: function () {
        var $self = $(this), $data = $self.data(), $selfText = $self.text();
        _this.modal.init();
        _this.ajaxRequest($data.action, 'id=' + $data.id + '&file=' + $data.file,
            function () {
                _this.modal.titleText($data.title);
                _this.modal.bodyHtml('Загрузка...');
            },
            function (response) {
                $self.text($selfText);
                _this.modal.bodyHtml(response);
            }
        );
    },
    uploadAndUpdateCsv: function () {
        var $self = $(this), $data = $self.data(), $selfText = $self.text();
        _this.modal.init();
        _this.modal.footerShow();
        _this.modal.btnText('Загрузить и обновить')
        _this.modal.btnClass('js-update-by-csv')
        _this.modal.btnAttr('data-action', 'ajax_common_handler')
        _this.modal.btnAttr('data-file', 'csv-upload-and-update')
        _this.modal.titleText($data.title)
        _this.ajaxRequest($data.action, 'id=' + $data.id + '&file=' + $data.file,
            function () {
                _this.modal.bodyHtml('Загрузка...')
            },
            function (response) {
                $self.text($selfText);
                _this.modal.bodyHtml(response);
            }
        );
    },
    uploadCsv: function () {
        var $self = $(this), $data = $self.data(), $selfText = $self.text();

        var formData = new FormData();
        formData.append('file', $('#file')[0].files[0]);
        formData.append('action', $data.action);
        formData.append('file', $data.file);

        $.ajax({
            url: front_ajaxurl,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                _this.modal.bodyHtml(response);
            }
        });

        // _this.ajaxRequest($data.action, 'file=' + $data.file + '&' + formData,
        //     function (){
        //         _this.modal.bodyHtml('Загрузка...');
        //     },
        //     function (response){
        //        console.log(response);
        //         $self.text($selfText);
        //         _this.modal.bodyHtml(response);
        //     }
        // );
    },
    viewOffersList: function () {
        var $self = $(this), $data = $self.data(), $selfText = $self.text();

        _this.modal.init();
        _this.modal.xl();
        _this.modal.titleText($data.title);

        _this.ajaxRequest($data.action, '&id=' + $data.id,
            function () {
                _this.modal.bodyHtml('Загрузка...');
                commonModal.show();
            },
            function (response) {
                $self.text($selfText);
                _this.modal.bodyHtml(response);
            }
        );
    },
    subscribeOnNotification: function () {
        var $self = $(this), $data = $self.data(), $selfText = $self.text();

        _this.modal.init();
        _this.modal.footerShow();
        _this.modal.btnText('Отправить')
        _this.modal.btnClass('js-send-subscribe-notification');
        _this.modal.titleText($data.title);

        _this.ajaxRequest($data.action, $($data.serialize).serialize(),
            function () {
                _this.modal.bodyHtml('Загрузка...');
                commonModal.show();
            },
            function (response) {
                $self.text($selfText);
                _this.modal.bodyHtml(response);
            });
    },
    sendSubscribeNotification: function () {
        var $self = $(this), $selfText = $self.text();

        _this.modal.init();

        _this.ajaxRequest('subscribe_notification_handler', $('.js-notification-wrapper input').serialize(),
            function () {
                $self.html('Загрузка...');
            },
            function (response) {
                $self.text($selfText);
                if (response == 'error') {
                    _this.modal.bodyPrepend('<div class="alert alert-danger">Все поля обязательны к заполнению</div>');
                }

                if (response == 'ok') {
                    _this.modal.bodyHtml('<div class="alert alert-success">Спасибо! Как только появится предложения к товару мы уведомим вас!</div>');
                }

            }
        );
    },
    sendRequestBePartner: function () {
        var $self = $(this), $data = $self.data(), $selfText = $self.text();
        _this.ajaxRequest('send_request_become_partner', $($data.serialize).serialize(),
            function () {
                $('input[name="phone"]').removeClass('is-invalid');
                $('input[name="name"]').removeClass('is-invalid');
                $self.html('Загрузка...');
            },
            function (response) {
                $self.text($selfText);
                if (response == 'name') {
                    $('input[name="' + response + '"]').addClass('is-invalid');
                }

                if (response == 'phone') {
                    $('input[name="' + response + '"]').addClass('is-invalid');
                }

                if (response == 'phonename') {
                    $('input[name="phone"]').addClass('is-invalid');
                    $('input[name="name"]').addClass('is-invalid');
                }

                if (response == 'ok') {
                    $('.callback-form').html('<h2 style="display: block; text-align: center; color: green">Спасибо, ваша заявка принята, мы скоро свяжемся с вами!</h2>');
                }
            });
    },
    addTo: function () {
        var $self = $(this), $data = $self.data(), $selfText = $self.text();

        if ($data.target == 'compare') var $name = 'cntools_compare';
        if ($data.target == 'favorite') var $name = 'cntools_favorite';

        $Data = _this.getCookie($name);
        var $result = $Data;
        if (undefined == $Data) {
            var $result = $data.id;
        } else {
            if (_this.inArray($data.id, $Data.split(',')) == false) {
                $result = $Data + ',' + $data.id;
            }
        }
        _this.setCookie($name, $result, 120);
        $self.addClass('opacity-low');
    },
    calcSelfService: function () {
        var $self = $(this),
            $data = $self.data(),
            $optionData = $self.find('option:selected').data(),
            $optionText = $self.find('option:selected').text();

        var $postData = 'value=' + $self.val() + '&get_pcode=' + $optionData.pcode + '&address=' + $optionText;
        _this.ajaxRequest('calc_self_service_price', $postData,
            function () {
                $('.date-picker').val('');
                $('.js-delivery-price-display-block').html('Загрузка...');
            },
            function (response) {
                var resp = JSON.parse(response);
                console.log(resp);
                $('.date-picker').val(resp.delivery_date);
                let block = `
                <input type="hidden" class="billing_address_1" data-required name="billing_address_1" value="СДЕК, ${$optionText}">
                <input type="hidden" class="billing_address_2" data-required name="billing_address_2" value="СДЕК, ${$optionText}">
                <input type="hidden" class="shipping_address_1" data-required name="shipping_address_1" value="СДЕК, ${$optionText}">
                <input type="hidden" class="shipping_address_2" data-required name="shipping_address_2" value="СДЕК, ${$optionText}">
                <input type="hidden" class="_order_shipping" data-required name="_order_shipping" value="${resp.total_price}">
                    <div class="properties" style="width: 100%;">
                        <ul class="options">
                            <li  class="width100prc"><span class="title" style="font-weight: 700;">Всего стоймость доставки:</span> <span class="value">${resp.total_price} ₽</span></li>
                            <li  class="width100prc"><span class="title"  style="font-weight: 700;">Ближайшая дата доставки:</span> <span class="value">${resp.delivery_date}</span></li> 
                        </ul>
                    </div>
                `;
                $('.js-delivery-price-display-block').html('');
                if (resp.delivery_info) {
                    $.each(resp.delivery_info, function (ix, val) {
                        let delInfoBlok = `
                            <div class="properties" style="width: 100%;">
                        <ul class="options">
                            <li class="noafter nobefore width100prc" style="font-weight: 600"><span class="title">${val.product_title}</span></li>
                            <li class="width100prc"><span class="title">Направление:</span> <span class="value">${val.start} &#8594; ${val.end} </span></li>
                            <li class="width100prc"><span class="title">Стоймость доставки:</span> <span class="value">${val.price} ₽</span></li>
                        </ul>
                        <br>
                    </div>
                       `;

                        $('.js-delivery-price-display-block').append(delInfoBlok);
                    });
                }


                $('.js-delivery-price-display-block').append(block);
            });
    },
    activeOffersTab: function () {
        $('.nav-link:first').trigger('click');
    },
    ajaxRequest: function ($action, $data, beforeSend, afterSuccess) {
        $.ajax({
            method: 'post',
            data: 'action=' + $action + '&' + $data,
            url: front_ajaxurl,
            beforeSend: function () {
                beforeSend()
            },
            success: function (response) {
                afterSuccess(response)
            }
        });
    },
    sendAction: function () {
        var $self = $(this), $data = $self.data(), $selfText = $self.text(), canRun = false;

        if ($data.confirmation !== undefined) {
            if (confirm($data.confirmation)) {
                canRun = true;
            }
        } else {
            if ($data.serialize !== undefined) {
                var postData = $($data.serialize).serialize();
                canRun = true;
            }
        }

        console.log(postData);

        if (canRun === true) {
            $.ajax({
                method: 'post',
                data: 'action=' + $data.action + '&post_type=' + $data.type + '&' + postData,
                url: ajaxurl,
                beforeSend: function () {
                    $self.text('Обработка...');
                },
                success: function (response) {
                    $self.text($selfText);

                    if (response == 'refresh') {
                        window.location.href = window.location.href;
                    }
                }
            });
        }
    },
    stepLocation: function () {
        var $self = $(this), $data = $self.data();
        $('.is-invalid').removeClass('is-invalid');
        $('*[data-required]').each(function (ix, val) {
            console.log($(val).val());
            if (!$(val).val()) {
                $(val).addClass('is-invalid');
            }
        });
        if ($('.is-invalid').length == 0) {
            var $tmpData = JSON.stringify(_this.toObject($('.cntools-checkout input, .cntools-checkout select').serializeArray()));

            if (_this.getCookie('cntools_checkout_steps_data')) {
                console.log(_this.getCookie('cntools_checkout_steps_data'));
                $tmpData = JSON.parse(_this.getCookie('cntools_checkout_steps_data'));
                var $serData = _this.toObject($('.cntools-checkout input, .cntools-checkout select').serializeArray());
                $.each($serData, function (ix, val) {
                    var elemVal = $tmpData[ix] !== $('[name="' + ix + '"]').val() ? $('[name="' + ix + '"]').val() : val;
                    $tmpData[ix] = elemVal;
                });

                $tmpData = JSON.stringify($tmpData);
            }

            _this.setCookie('cntools_checkout_steps_data', $tmpData, 3);

            window.location.href = $data.href;
        }
    },
    billingUserType: function () {
        var $self = $(this), $data = $self.data(), $selfText = $self.text();

        if ($self.attr('name') == 'billing_wooccm13') {
            $('.js-iur-fields .mb-3').css({display: 'block'})
        } else {
            $('.js-iur-fields .mb-3').css({display: 'none'})
        }
    }
}


/**
 *  Run JSCntoolsFront
 */
jQuery(document).ready(function ($) {
    JSCntoolsFront = new JSCntoolsFront();
});


//######################################################################################################################

$ = jQuery;
$(document).ready(function () {
    console.log('Start-app');

    window.spinnerDark = `<div class="spinner-border text-dark" role="status">
  <span class="visually-hidden">Loading...</span>
</div>`;

    /**
     *  .js-button-next
     */
    $(document).on('click touch', '.js-button-next', function () {
        var $self = $(this),
            $currentStep = $('.checkout-steps li button.active').data('target'),
            $nextStep = $currentStep + 1;

        $('[data-target="' + $nextStep + '"]').attr('disabled', false);
        $('[data-target="' + $nextStep + '"]').trigger('click')
        $('.checkout-steps li').removeClass('active');
        $('[data-target="' + $nextStep + '"]').parent('li').addClass('active')

        console.log($nextStep)


        $self.css({display: 'inline-block'});
        $('.js-send-block').css({display: 'none'});

        if ($nextStep > 1) {
            $('.js-button-back ').css({display: 'inline-block'});
        }
        //
        if ($nextStep == 2) {
            $('.select-get-method li.js-get-method:first-child').trigger('click');
        }
        //
        //
        if ($nextStep >= 3) {
            $self.hide();
            $('.js-send-block').css({display: 'inline-block'});
        }
        //
        //  console.log($nextStep);
        //  $('.checkout-steps li').removeClass('active');
        //  $('.checkout-steps-wrapper .step-block').removeClass('step-active');
        //
        //  $('li[data-target="'+ $nextStep +'"]').addClass('active');
        //  $('.step-' + $nextStep).addClass('step-active');

    });


    /**
     * .js-button-back
     */
    $(document).on('click touch', '.js-button-back', function () {
        var $self = $(this),
            $currentStep = $('.checkout-steps li button.active').data('target'),
            $backStep = $currentStep - 1;
        $('[data-target="' + $currentStep + '"]').attr('disabled', true);
        $('[data-target="' + $backStep + '"]').trigger('click')

        $('.checkout-steps li').removeClass('active');
        $('[data-target="' + $backStep + '"]').parent('li').addClass('active')

        $('.js-button-next').css({display: 'inline-block'});
        $('.js-send-block').css({display: 'none'});

    });


    /**
     *  checkbox as radio
     */
    $(document).on('click touch', '.checkbox-wrapper input[type="checkbox"]', function () {
        var group = '.checkbox-wrapper input[type="checkbox"]';
        $(group).prop("checked", false);
        $(this).prop("checked", true);

    });


    /**
     * select metods
     */
    $(document).on('click touch', '.js-get-method', function () {
        var $self = $(this), $data = $self.data(), $additionalData = '';

        $('.shipping_method').val($data.value);
        $('.billing_address_1').val($data.address);
        $('.billing_address_2').val($data.address);
        $('.shipping_address_1').val($data.address);
        $('.shipping_address_2').val($data.address);

        $('.js-get-method').removeClass('active');

        $self.addClass('active');

        jQuery.ajax({
            type: "post",
            url: front_ajaxurl,
            data: 'action=' + $data.action,
            beforeSend: function () {
                $('.selected-get-method-wrapper').html('<div class="loader-wrapper"><div class="loader"></div></div>')
            },
            success: function (res) {
                $('.selected-get-method-wrapper').html(res);
            }
        });

    })


    /**
     * js-change-quantity
     */
    $(document).on('click touch', '.js-change-quantity', function () {
        var $self = $(this),
            $data = $self.data(),
            $text = $self.find('span').text(),
            $parent = $self.parent().parent().parent(),
            $price = $parent.find('.offer-price span').data('price'),
            $quantity = $parent.find('[data-qty]'),
            $quantityVal = $quantity.val(),
            $quantityPlus = parseInt($quantityVal, 10) + 1,
            $quantityMinus = parseInt($quantityVal, 10) - 1;


//         if ($data.operator == 'plus') {
//             $quantityPlus = $quantityPlus > $data.quantity ? $data.quantity : $quantityPlus
//             $quantity.val($quantityPlus);
//             var $newPrice = $price * $quantityPlus;
//         }

//         if ($data.operator == 'minus') {
//             $quantityMinus = $quantityMinus < 1 ? 1 : $quantityMinus;
//             $quantity.val($quantityMinus)
//             var $newPrice = $price * $quantityMinus;
//         }

//         $parent.find('.offer-price span').text(parseFloat($newPrice));
//
//
        //my
        $cartParent = $self.parent().parent().parent(),
            $cartPrice = $cartParent.find('.span-cart-total').data('cartprice'),
            $cartQuantity = $cartParent.find('input.card-count'),
            $cartQuantityVal = $cartQuantity.val(),
            $cartQuantityPlus = parseInt($cartQuantityVal, 10) + 1,
            $cartQuantityMinus = parseInt($cartQuantityVal, 10) - 1;


        if ($data.operator == 'plus') {
            $quantityPlus = $quantityPlus > $data.quantity ? $data.quantity : $quantityPlus
            $cartQuantityPlus = $cartQuantityPlus > $data.cartQuantity ? $data.cartQuantity : $cartQuantityPlus
            $quantity.val($quantityPlus);
            $cartQuantity.val($cartQuantityPlus);
            var $newPrice = $price * $quantityPlus;
            var $cartNewPrice = $cartPrice * $cartQuantityPlus;
        }

        if ($data.operator == 'minus') {
            $quantityMinus = $quantityMinus < 1 ? 1 : $quantityMinus
            $cartQuantityMinus = $cartQuantityMinus < 1 ? 1 : $cartQuantityMinus
            $quantity.val($quantityMinus);
            $cartQuantity.val($cartQuantityMinus);
            var $newPrice = $price * $quantityMinus;
            var $cartNewPrice = $cartPrice * $cartQuantityMinus;
        }

        $parent.find('.offer-price span').text(parseFloat($newPrice));
        $cartParent.find('bdi').text($cartNewPrice + ',00 ₽');

// Кнопка Обновить корзину, появляется после изменения кол-ва
        if ($('[name="update_cart"]').length > 0) {
            $('[name="update_cart"]').attr('disabled', false)
            $('[name="update_cart"]').data('disabled', false)
            //$('[name="update_cart"]').show()
        }
    })


    /**
     *  .js-product-quantity
     */
    $(document).on('click keyup keydown', '.js-product-quantity', function () {
        var $self = $(this),
            $data = $self.data();

        if ($self.val() > $data.quantity) $self.val($data.quantity)
        if ($self.val() < 1) $self.val(1)
    })

    /**
     *  .js-toggle-menu
     */
    $(document).on('click touch', '.js-toggle-menu', function () {
        var $self = $(this),
            $data = $self.data();

        if ($($data.target).hasClass('show')) {
            $($data.target).removeClass('show');
        } else {
            $($data.target).addClass('show');
        }

    })


    /**
     *  js-add-to-cart
     */
    $(document).on('click touch', '.js-add-to-cart', function () {
        var $self = $(this),
            $data = $self.data(),
            $text = $self.text(),
            $parent = $self.parent().parent().parent(),
            $headerCartPrice = $('.header-cart-price'),
            $quantity = $parent.find('.js-product-quantity').val();

        $_self = $self;

        jQuery.ajax({
            type: "post",
            url: front_ajaxurl,
            data: 'action=add_in_cart&product_id=' + $data.product + '&supplier_id=' + $data.supplier + '&price=' + $data.price + '&quantity=' + $quantity,
            beforeSend: function () {
                $self.text('Добавление в корзину...');
                $('.addtocart .text-warning').show();
                $('.buy-block').hide();
                $self.parent('.buy-block').show();
                $self.parents('.addtocart').find('.text-warning').hide();
            },
            success: function (res) {
                var $result = JSON.parse(res);
                if ($result.status == 'success') {
                    let $block = `
                       <div class="row">
                            <div class="col-12"></div>
                           <div class="col-12">
                                <a href="/cart/" class="btn btn-primary go-to-cart btn-sm"><i class="fa fa-shopping-cart"></i> Перейти в корзину</a>
                           </div>
                            <div class="col-12">
                                <a href="#remove" data-id="${$result.cart_item_id}" class="text text-danger remove-from-cart js-remove-form-cart">Удалить из корзины</a>
                            </div>
                       </div> 
                    `;
                    $parent.hide();
                    $self.hide();
                    $parent.after($block);
                }
                $headerCartPrice.text($data.price * $quantity + ' ₽');

            }
        });

    });


    /**
     *  js-remove-form-cart
     */
    $(document).on('click touch', '.js-remove-form-cart', function () {
        var $self = $(this),
            $data = $self.data(),
            $text = $self.text(),
            $headerCartPrice = $('.header-cart-price'),
            $quantity = $self.parent().find('.js-product-quantity').val();

        $_self = $self;

        jQuery.ajax({
            type: "post",
            url: front_ajaxurl,
            data: 'action=remove_from_cart&cart_item_id=' + $data.id,
            beforeSend: function () {
                $self.text('Удаление из корзины...');
            },
            success: function (res) {
                var $result = JSON.parse(res);
                if ($result.status == 'success') {
                    $('.buy-block').show();
                    $('.addtocart .text-warning').hide();
                    $self.parents('.addtocart').find('a').remove();
                    $('.addtocart .input-group').show();
                    $('.addtocart .btn').show();

                }
                $headerCartPrice.text('0.00');
            }
        });

    })


    $(window).resize(function () {
        var $self = $(this);

        if (
            $self.width() < 500 &&
            $('.mob-search-block').find('.search-group').length == 0 &&
            $('.mob-search-block').find('.compare-and-favorite').length == 0 &&
            $('.mob-search-block').find('.checker-and-gate').length == 0
        ) {
            adaptiveSmallView();
        } else {
            removeAdaptiveView();
        }
    });


    if ($(window).width() < 500) adaptiveSmallView();


    function adaptiveSmallView() {
        var $searchBlock = $('.search-group').clone(),
            $cartBlock = $('.compare-and-favorite').clone(),
            $checkerBlock = $('.checker-and-gate').clone();

        $('.mob-search-block > .col-7').html($searchBlock);
        $('.mob-search-block > .col-5').html($cartBlock);
        $('.logo-wrapper').append($checkerBlock);

        $('.middle .blue-butter').css({display: 'block'})
        $('.mob-search-block').removeClass('hide');
    }


    function removeAdaptiveView() {

        $('.mob-search-block > .col-7').find('.search-group').remove();
        $('.mob-search-block > .col-5').find('.compare-and-favorite').remove();
        ;
        $('.logo-wrapper').find('.checker-and-gate').remove();

        $('.middle .blue-butter').css({display: 'none'})
    }


    /**
     *  scroll
     */
    $(window).scroll(function () {
        var $self = $(this);
        if ($self.scrollTop() >= 600) {
            $('header nav').addClass('fixed animate__animated animate__fadeInDown');
        } else {
            $('header nav').removeClass('fixed animate__animated animate__fadeInDown');
        }


        if ($self.scrollTop() >= 700) {
            if ($('.fixed-product-tabs').length == 0) {
                $('.product-tabs').append('<div class="fixed-product-tabs animate__animated animate__fadeInDown"><div class="container"></div></div>');
                var tabs = $('ul.product-tabs-control').html();
                // $('.fixed-product-tabs .container').html('<ul class="product-tabs-control">' + tabs + '</ul>');
            }
        } else {
            $('.fixed-product-tabs').remove();
        }
    });


    /**
     * Active tabs
     */
    $(document).on('click touch', '.product-tabs-control li', function () {
        var $self = $(this);
        $('.product-tabs-control li').removeClass('active');
        $self.addClass('active');
    });


    /**
     * strong.js-point-select
     */
    $(document).on('click touch', 'strong.js-point-select', function () {
        var $self = $(this), $id = '#' + $self.attr('id');
        $('input' + $id).attr('checked', true);
    });


    // const myCarousel = new Carousel(document.querySelector(".carousel"), {
    //     // 'friction' : 1,
    //     'slidesPerPage' : 1
    // });


    /**
     * Active tabs
     */
    $(document).on('click touch', '.js-select-offer li', function () {
        var $self = $(this), $data = $self.data();
        $('.offers').find('.active').removeClass('active');
        $self.addClass('active');
    });


    /**
     * .js-toggle-selector-filter
     */
    $(document).on('click touch', '.js-toggle-selector-filter', function () {
        var $self = $(this), $data = $self.data();

        if ($('.must-hide').length > 0) {
            $('.must-hide')
                .addClass('hide-selector-filter')
                .removeClass('must-hide');
            $self.find('span').text($data.show);
        } else {
            $('.hide-selector-filter')
                .addClass('must-hide')
                .removeClass('hide-selector-filter');
            $self.find('span').text($data.hide);
        }
    });
});

/**
 *
 */
jQuery(function ($) {
    $(document).on('click touch', '.js-change-quantity', function (e) {
        e.preventDefault()

        var thisBtn = $(this);
        var item_hash = $(this).parent().find($('input')).attr('name').replace(/cart\[([\w]+)\]\[qty\]/g, "$1");
        var item_quantity = $(this).parent().find($('input')).val();
        var currentVal = parseFloat(item_quantity);

        $.ajax({
            type: 'POST',
            url: cart_qty_ajax.ajax_url,
            data: {
                action: 'qty_cart',
                hash: item_hash,
                quantity: currentVal
            },
            beforeSend: function (req) {
                thisBtn.addClass('pointer-events-none')

            },
            success: function (data) {
                thisBtn.removeClass('pointer-events-none')
                $('.woocommerce').html(data);
            },
        });
    });
});