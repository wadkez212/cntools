<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Портал_Cntools
 */

get_header();
?>

	<main id="primary" class="site-main container">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<nav class="woocommerce-breadcrumb">','</nav>' );
        }
        ?>

		<?php if ( have_posts() ) : ?>
				<h1 class="cntools-entry-title">
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Результат по запросу: %s', 'cnts' ), '<span>' . get_search_query() . '</span>' );
					?>
				</h1>


			<?php
			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

	</main><!-- #main -->

<?php
get_footer();
