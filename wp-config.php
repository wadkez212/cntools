<?php

//Begin Really Simple SSL session cookie settings
@ini_set('session.cookie_httponly', true);
@ini_set('session.cookie_secure', true);
@ini_set('session.use_only_cookies', true);
//END Really Simple SSL cookie settings
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать файл в "wp-config.php"
 * и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки базы данных
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры базы данных: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'cntools_20230224' );

/** Имя пользователя базы данных */
define( 'DB_USER', 'root' );

/** Пароль к базе данных */
define( 'DB_PASSWORD', 'root' );

/** Имя сервера базы данных */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу. Можно сгенерировать их с помощью
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}.
 *
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными.
 * Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'L284Xt,pf-}_2E@U!n)x9 d;(^jKYA3|/]#Uq8Q#]qKZ?8O*sCu5uR?Gay6k4|~]' );
define( 'SECURE_AUTH_KEY',  '?E_XK#{1~m~e0=.;_R!z}w&mz[du!5v2f_D#3`=nY 6B%0(:387%RHOo!/1MEoBB' );
define( 'LOGGED_IN_KEY',    '(Z)@R3Ju{/U?oOA^@c[p9xr#}b>KMzD/+OsyA7`hX[d>8?0P6!*<6;Cx}t{xX=2w' );
define( 'NONCE_KEY',        '3xav?IU{5DdNaYrH{je{-zKY7@a!~ R-5 =I_mW@3y)^=aQr.O|ock9JIN5yK!Hp' );
define( 'AUTH_SALT',        'P?q+27Z:&S<ni?Q&Qq9I$Eu~@(/##Ibze+R_3uo7mPr5vz=nfL{0hT[ 5E/,PL;c' );
define( 'SECURE_AUTH_SALT', 'Viu1k_~ni9gX;UCf_X`B9odZdz88W*mV<8`J+3>`o**z!17%a~!X+BR(mL5H}wUh' );
define( 'LOGGED_IN_SALT',   '2yt!zDqH8TWmS<2mYD|tG-JQIb.C@zp:)Lpd7,36N,1i<@| V3H:#Yj1(bJ!z<vp' );
define( 'NONCE_SALT',       '1U,605h|5}ai!Pv!q=:!D0gZh7mhc&rWtx??arcGgw`.QfX#98!#xASqr;,%XPiK' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_cntoold';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Произвольные значения добавляйте между этой строкой и надписью "дальше не редактируем". */



/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
